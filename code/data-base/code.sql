DROP TABLE logs;

DROP TABLE permissions;

DROP TABLE familiar;

DROP TABLE acolhido;

DROP TABLE users;

DROP TABLE projeto_social;

DROP TABLE horario_disponivel;

DROP TABLE horario;

DROP TABLE orientador;

DROP TYPE OPERATION_TYPE;

DROP TYPE PERMISSION_GROUP;

DROP TYPE DIA_DA_SEMANA;

DROP TYPE PERIODO_PS;

DROP TYPE PERIODO_ESCOLAR;

DROP TYPE SEXO;

CREATE TYPE DIA_DA_SEMANA AS ENUM ('DOMINGO','SEGUNDA', 'TERCA', 'QUARTA', 'QUINTA', 'SEXTA', 'SABADO');

CREATE TYPE PERIODO_PS AS ENUM ('MANHA', 'TARDE'); 

CREATE TYPE PERIODO_ESCOLAR AS ENUM ('MANHA', 'TARDE');

CREATE TYPE SEXO AS ENUM ('MASCULINO', 'FEMININO');

CREATE TYPE PERMISSION_GROUP AS ENUM ();

CREATE TYPE OPERATION_TYPE AS ENUM ('SAVE', 'VIEW', 'MODIFY');

CREATE TABLE projeto_social (
  id SERIAL NOT NULL PRIMARY KEY,
  nome VARCHAR(30) NOT NULL
);

CREATE TABLE users (
  id SERIAL NOT NULL PRIMARY KEY,
  nickname VARCHAR(30) NOT NULL,
  user_name VARCHAR(30) NOT NULL,
  password VARCHAR(128) NOT NULL
);

CREATE TABLE acolhido (
  id SERIAL NOT NULL PRIMARY KEY,
  registry TIMESTAMP,
  nome_acolhido VARCHAR (60),
  foto_acolhido VARCHAR (58500),
  data_nascimento TIMESTAMP,
  sexo SEXO,
  nome_mae VARCHAR(60),
  RG_mae VARCHAR(20),
  CPF_mae VARCHAR(11),
  local_trabalho_mae VARCHAR(60),
  nome_pai VARCHAR(60),
  RG_pai VARCHAR(20),
  CPF_pai VARCHAR(11),
  local_trabalho_pai VARCHAR(60),
  endereco_rua VARCHAR(60),
  endereco_numero INTEGER,
  endereco_complemento VARCHAR(30),
  endereco_bairro VARCHAR(30),
  endereco_cidade VARCHAR(30),
  com_quem_volta_para_casa VARCHAR(50),
  telefone_residencial VARCHAR(16),
  telefone_celular VARCHAR(16),
  periodo_ps PERIODO_PS,
  quantas_vezes_no_projeto SMALLINT,
  atendimento_prioritario BOOLEAN,
  escola_nome VARCHAR(30),
  escola_periodo PERIODO_ESCOLAR,
  escola_ano SMALLINT,
  deficiencia VARCHAR(60),
  alergia VARCHAR(60),
  medicamento VARCHAR(60),
  bolsa_familia BOOLEAN,
  doenca_grave VARCHAR(200),
  tratamento_neurologista VARCHAR(60),
  tratamento_psiquiatra VARCHAR(60),
  tratamento_psicologo VARCHAR(60),
  tratamento_psicopedagogo VARCHAR(60),
  tratamento_fonoaudiologo VARCHAR(60),
  tratamento_outros VARCHAR(60),
  problema_na_familia VARCHAR(200),
  gosta_de_fazer VARCHAR(200),
  relacionamento_com_familia VARCHAR(200),
  desligamento TIMESTAMP,
  user_on_registry INTEGER REFERENCES users,
  first_registry_id INTEGER REFERENCES acolhido,
  first_registry_date TIMESTAMP,
  acolhido_reinserido BOOLEAN DEFAULT FALSE,
  projeto_social_id INTEGER REFERENCES projeto_social
);

CREATE TABLE familiar (
  id SERIAL NOT NULL PRIMARY KEY,
  acolhido_id INTEGER REFERENCES acolhido,
  nome VARCHAR(60),
  idade SMALLINT,
  parentesco VARCHAR(20),
  ocupacao VARCHAR(30)
);

CREATE TABLE orientador (
   id SERIAL NOT NULL PRIMARY KEY,
   registry TIMESTAMP,
   nome VARCHAR (60)
);

CREATE TABLE horario (
  id SERIAL NOT NULL PRIMARY KEY,
  hora_inicio INTEGER,
  minuto_inicio INTEGER,
  hora_termino INTEGER,
  minuto_termino INTEGER,
  dia_da_semana DIA_DA_SEMANA
);

CREATE TABLE horario_disponivel (
  id SERIAL NOT NULL PRIMARY KEY,
  orientador_id INTEGER REFERENCES orientador,
  horario_id INTEGER REFERENCES horario
);

CREATE TABLE permissions (
  user_id INTEGER REFERENCES users,
  group_name PERMISSION_GROUP,
  PRIMARY KEY (user_id, group_name)
);

CREATE TABLE logs (
  id SERIAL NOT NULL PRIMARY KEY,
  operation_date TIMESTAMP,
  user_id INTEGER REFERENCES users,
  entity_id INTEGER,
  operation_type OPERATION_TYPE,
  operation_description VARCHAR(200)
);

-- SELECT now();

INSERT INTO users (
              nickname,
              user_name,
              password) VALUES
  ('usuarioteste2',
   'Usuário de Teste B',
   '717391b84f80fa1105004b2eb8021b758cf791fd58412c7aee1f9856fbc0edeb2877bf0adc33919d91032ccc880499b4c497821b22879a3c60144260f461a65d'),
  ('usuarioteste',
   'Usuário de Teste',
   'a69f73cca23a9ac5c8b567dc185a756e97c982164fe25859e0d1dcc1475c80a615b2123af1f5f94c11e3e9402c3ac558f500199d95b6d3e301758586281dcd26');


INSERT INTO acolhido (
              id,
              registry,
              nome_acolhido,
              foto_acolhido,
              data_nascimento,
              sexo,
              nome_mae,
              CPF_mae,
              nome_pai,
              CPF_pai,
              telefone_residencial,
              telefone_celular,
              periodo_ps,
              atendimento_prioritario,
              escola_periodo,
              user_on_registry,
              first_registry_id,
              first_registry_date) VALUES


  (1,
   '2020-5-1',
   'Rita Bela Marques de Jesus',
   NULL,
   '2011-2-10',
   'FEMININO',
   'Maria Eduarda Bela Marques de Jesus',
   '11122233344',
   NULL,
   NULL,
   '4693-1000',
   '9 9988-1122',
   'MANHA',
   FALSE,
   'TARDE',
   1,
   1,
   '2020-5-1'),


  (2,
   '2022-5-1',
   'Fábio Marques de Jesus',
   NULL,
   '2014-7-31',
   'MASCULINO',
   'Maria Eduarda Bela Marques de Jesus',
   '11122233344',
   NULL,
   NULL,
   '4693-1000',
   '9 9988-1122',
   'MANHA',
   FALSE,
   'TARDE',
   1,
   1,
   '2022-5-1'),


  (3,
   '2021-12-1',
   'Adriana Jovem Moreira',
   NULL,
   '2013-11-1',
   'FEMININO',
   'Maria Jovem Moreira',
   NULL,
   NULL,
   '33322211155',
   '4693-1010',
   '9 9876-3333',
   'TARDE',
   FALSE,
   'MANHA',
   1,
   2,
   '2021-12-1'),


  (4,
   '2019-6-1',
   'Amanda Santos',
   NULL,
   '2011-4-18',
   'FEMININO',
   'Ana Santos',
   '55511122255',
   NULL,
   '56745656788',
   '4693-3030',
   '9 1010-9854',
   'MANHA',
   TRUE,
   'TARDE',
   1,
   3,
   '2019-6-1'),


  (5,
   '2022-6-5',
   'João do Pé de Laranja',
   NULL,
   '2016-7-31',
   'MASCULINO',
   'Ana Laranja',
   '55511122255',
   NULL,
   '56745656788',
   '4693-3030',
   '9 1010-9854',
   'MANHA',
   TRUE,
   'TARDE',
   1,
   4,
   '2022-6-5'),


  (6,
   '2020-8-4',
   'André Jovem Marques Moreira da Cruz',
   NULL,
   '2009-4-20',
   'MASCULINO',
   'Beatriz Jovem Marques Moreira da Cruz',
   NULL,
   NULL,
   '33322211155',
   '4695-7891',
   '9 9080-5432',
   'MANHA',
   FALSE,
   'TARDE',
   1,
   5,
   '2020-8-4'),


  (7,
   '2022-1-10',
   'Rafael Marques de Acerola',
   NULL,
   '2012-7-1',
   'MASCULINO',
   'Paula Pé de Acerola',
   NULL,
   NULL,
   '33322211155',
   '4695-7891',
   '9 9080-5432',
   'TARDE',
   FALSE,
   'MANHA',
   1,
   5,
   '2022-1-10'),


  (8,
   '2022-7-8',
   'Mariana da Abobora',
   NULL,
   '2010-8-1',
   'FEMININO',
   NULL,
   NULL,
   'Rafael da Abobora',
   '33322211155',
   '4695-7891',
   '9 9080-5432',
   'TARDE',
   FALSE,
   'MANHA',
   1,
   5,
   '2022-1-10'),


  (9,
   '2022-2-3',
   'Isabela Aparecida',
   NULL,
   '2010-7-31',
   'FEMININO',
   NULL,
   NULL,
   'Cláudio Jovem',
   '12312332122',
   '4695-7891',
   '9 9080-5432',
   'TARDE',
   FALSE,
   'MANHA',
   1,
   5,
   '2022-2-3'),


  (10,
   '2022-2-3',
   'Juliana Caqui',
   NULL,
   '2008-8-1',
   'FEMININO',
   NULL,
   NULL,
   'Marcos Caqui',
   '22211144433',
   '4695-7891',
   '9 9080-5432',
   'TARDE',
   FALSE,
   'MANHA',
   1,
   5,
   '2022-2-3'),

  (11,
   '2022-2-3',
   'Lucas Caqui',
   NULL,
   '2008-7-31',
   'MASCULINO',
   NULL,
   NULL,
   'Marcos Caqui',
   '22211144433',
   '4695-7891',
   '9 9080-5432',
   'TARDE',
   FALSE,
   'MANHA',
   1,
   5,
   '2022-2-3');


SELECT setval('acolhido_id_seq', 12, false);

/* DATA FOR AGE TESTING

INSERT INTO acolhido (
              registry,
              nome_acolhido,
	            data_nascimento) VALUES

                
  ('2019-2-18',
   'teste de idade A',
   '2012-11-10'),


  ('2020-2-10',
   'teste de idade B',
   '2012-11-1'),


  ('2020-2-20',
   'teste de idade C',
   '2012-10-31'),


  ('2020-2-20',
   'teste de idade D',
   '2012-9-30'),


  ('2018-2-6',
   'teste de idade E',
   '2010-10-31'),



  ('2018-2-6',
   'teste de idade F',
   '2010-9-30'),


  ('2018-2-6',
   'teste de idade G',
   '2010-11-1');
*/

SELECT * FROM acolhido;

SELECT * FROM users;
/* vi: set sts=2 ts=2 sw=2 et fenc=utf-8 ff=dos: */
