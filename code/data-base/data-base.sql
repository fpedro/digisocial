


CREATE TABLE atividade (
   id SERIAL NOT NULL PRIMARY KEY,
   registry TIMESTAMP,
   nome VARCHAR (60),
   descricao VARCHAR (400)
);

CREATE TABLE responsabilidade (
  id SERIAL NOT NULL PRIMARY KEY,
  atividade INTEGER REFERENCES atividade,
  orientador INTEGER REFERENCES orientador
);

CREATE TABLE atividade_agendada (
  id SERIAL NOT NULL PRIMARY KEY,
  registry TIMESTAMP,
  active BOOLEAN,
  inicio TIMESTAMP,
  termino TIMESTAMP,
  atividade INTEGER REFERENCES atividade,
  orientador INTEGER REFERENCES orientador
);

CREATE TABLE objetivo (
   id SERIAL NOT NULL PRIMARY KEY,
   active BOOLEAN,
   registry TIMESTAMP,
   nome VARCHAR (60),
   descricao VARCHAR (400),
   resultado_alcancado BOOLEAN
);

CREATE TABLE passo  (
  id SERIAL NOT NULL PRIMARY KEY,
  atividade_agendada INTEGER REFERENCES atividade_agendada,
  objetivo INTEGER REFERENCES objetivo
);


INSERT INTO orientador (registry, nome) VALUES ('2019-3-10', 'Pedro');
INSERT INTO orientador (registry, nome) VALUES ('2019-3-10', 'Maria Eduarda');
INSERT INTO orientador (registry, nome) VALUES ('2019-3-10', 'Ana');
INSERT INTO orientador (registry, nome) VALUES ('2019-3-10', 'Camila');
INSERT INTO atividade (registry, nome, descricao) VALUES ('2019-3-10', 'Oficina de Violão','Na oficina de violão as crianças aprendem como encontrar as notas no instrumento, tocar a música com sentimento e como tocar em harmonia com outras pessoas. A oficina é realizada com crianças com mais de 10 anos.');
INSERT INTO atividade (registry, nome, descricao) VALUES ('2019-3-10', 'Socio Educativa', 'Na atividade socio educativa as crianças partilham suas opniões sobre como a sociedade pode progredir. Elas fazem desenhos e cartazes.');
INSERT INTO atividade (registry, nome, descricao) VALUES ('2019-3-10', 'Artesanato com papel', 'São confeccionadas bolsas, pulseiras e itens para decoração de ambientes com papel. A atividade motiva a reciclagem e as crianças desenvolvem a criatividade.');
INSERT INTO atividade (registry, nome, descricao) VALUES ('2019-3-10', 'Futsal', 'São organizados os times em cada realização da atividade. Então, as crianças são levadas para a quadra da escola Marta. Deve ser tomado cuidado para que as crianças não fiquem expostas ao sol.');
INSERT INTO atividade_orientador (atividade, orientador) VALUES (1, 1);
INSERT INTO atividade_orientador (atividade, orientador) VALUES (1, 4);
INSERT INTO atividade_orientador (atividade, orientador) VALUES (2, 3);
INSERT INTO atividade_orientador (atividade, orientador) VALUES (2, 4);
INSERT INTO atividade_orientador (atividade, orientador) VALUES (3, 2);
INSERT INTO atividade_orientador (atividade, orientador) VALUES (4, 1);
SELECT atividade_orientador.atividade,
       atividade.nome,
       atividade_orientador.orientador,
       orientador.nome
FROM atividade_orientador JOIN atividade ON (atividade_orientador.atividade = atividade.id)
			  JOIN orientador ON (atividade_orientador.orientador = orientador.id);

INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (9, 0, 11, 0, 'SEGUNDA' );
INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (9, 0, 11, 0, 'QUINTA' );
INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (14, 0, 15, 0, 'QUINTA' );
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (1, 1);
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (1, 2);
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (1, 3);

INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (8, 0, 12, 0, 'SEGUNDA' );
INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (13, 0, 16, 0, 'SEGUNDA' );
INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (8, 0, 12, 0, 'TERCA' );
INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (13, 0, 16, 0, 'TERCA' );
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (4, 4);
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (4, 5);
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (4, 6);
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (4, 7);

INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (8, 0, 12, 0, 'SEXTA' );
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (3, 8);

INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (8, 0, 9, 0, 'TERCA' );
INSERT INTO horariodisponivel (horainicio, minutoinicio, horatermino, minutotermino, diadasemana)
	      VALUES (13, 0, 14, 0, 'TERCA' );
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (2, 9);
INSERT INTO orientador_horariodisponivel (orientador, horariodisponivel) VALUES (2, 10);

SELECT orientador.nome, 
       horariodisponivel.diadasemana
  FROM orientador_horariodisponivel 
    JOIN orientador 
    ON (orientador_horariodisponivel.orientador = orientador.id)
    JOIN horariodisponivel 
    ON (orientador_horariodisponivel.horariodisponivel = horariodisponivel.id)
  WHERE orientador.id = 1;

INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES ('2019-3-11 10:00', '2019-3-11 11:00', 1, 1);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES ('2019-3-11 10:00', '2019-3-11 11:00', 2, 4);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES ('2019-3-12 14:00', '2019-3-12 14:00', 3, 2);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES ('2019-3-14 9:00', '2019-3-14 11:00', 4, 1);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES ('2019-3-15 9:00', '2019-3-14 11:00', 2, 3);

INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES('2018-10-8 8:00', '2018-10-8 12:00', 2, 4);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES('2019-10-29 8:00', '2019-10-29 12:00', 2, 4);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES('2019-11-5 14:30', '2019-11-5 16:00', 2, 4);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES('2019-10-22 13:00', '2019-10-22 14:00', 1, 4);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES('2019-11-26 13:00', '2019-11-26 14:00', 1, 4);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES('2018-10-30 13:00', '2018-10-30 14:00', 3, 2);
INSERT INTO atividadeagendada (inicio, termino, atividade, orientador) VALUES('2019-10-3 14:00', '2019-10-3 15:00', 4, 1);

--

Rita Bela Marques de Jesus
Maria Eduarda Bela Marques de Jesus
4693-1000
9 9988-1122

Adriana Jovem Moreira
Maria Jovem Moreira
4693-1010
9 9876-3333

Amanda Santos
Ana Santos
4693-3030
9 1010-9854

Andr� Jovem Marques Moreira da Cruz
Beatriz Jovem Marques Moreira da Cruz
4695-7891
9 9080-5432


Cl�udio Jovem do P� de Abacate
Adriana Jovem do P� de Abacate
trabalha na fazenda do bairro Abacate cuidando do gado
amiga Laura

  NOMES
  -------------
  Adriana
  Amanda
  Andr�
  Ana
  Benedito
  Beatriz
  Cl�udio
  Daniela
  Emanuel
  Jo�o
  Jos�
  Laura
  Lucas
  Maria
  Mariana
  Mateus
  Marcos
  Pedro
  Paulo
  Rafael
  Rita
  Valentina

  SOBRENOMES
  -------------
  Aparecida
  Cruz
  Eduarda
  Santos
  Paz
  Henrique
  Marques
  Moreira
  Paulo
  Jesus
  Antoneli
  Flor
  Clara
  Belo
  Bela
  Jovem
  Vit�ria
