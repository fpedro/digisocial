package domain.implementation;

import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoQuery extends IdentifiableEntity {
  
   private Acolhido parameters;
   private List<Acolhido> queryResult;

   public Acolhido getParameters() {
      return parameters;
   }

   public void setParameters(Acolhido parameters) {
      this.parameters = parameters;
   }

   public List<Acolhido> getQueryResult() {
      return queryResult;
   }

   public void setQueryResult(List<Acolhido> queryResult) {
      this.queryResult = queryResult;
   }


  
}
