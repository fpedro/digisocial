package domain.implementation;

import java.util.List;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author Fábio Henrique
 */
public class SessaoPS extends IdentifiableEntity {

   private List<Usuario> registeredUsers;
   private HttpSession session;
   private Usuario userOnAuthentication;

   public List<Usuario> getRegisteredUsers() {
      return registeredUsers;
   }

   public void setRegisteredUsers(List<Usuario> registeredUsers) {
      this.registeredUsers = registeredUsers;
   }

   public HttpSession getSession() {
      return session;
   }

   public void setSession(HttpSession session) {
      this.session = session;
   }

   public Usuario getUserOnAuthentication() {
      return userOnAuthentication;
   }

   public void setUserOnAuthentication(Usuario userOnAuthentication) {
      this.userOnAuthentication = userOnAuthentication;
   }

}
