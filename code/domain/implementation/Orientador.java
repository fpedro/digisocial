package domain.implementation;

import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class Orientador extends IdentifiableEntity {
  
  private String nome;
  private List<HorarioDisponivel> horariosDisponiveis;

  public List<HorarioDisponivel> getHorariosDisponiveis() {
    return horariosDisponiveis;
  }

  public void setHorariosDisponiveis(List horariosDisponiveis) {
    this.horariosDisponiveis = horariosDisponiveis;
  }

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }
  
  
}
