package domain.implementation;

import domain.Entity;

import java.util.Date;

/**
 *
 * @author Fábio Henrique
 */
public class LogPS extends IdentifiableEntity {

   private Date data;
   private Usuario user;
   private Entity entity;
   private OperationType operationType;
   private String operationDescription;

   public Date getData() {
      return data;
   }

   public void setData(Date data) {
      this.data = data;
   }

   public Usuario getUser() {
      return user;
   }

   public void setUser(Usuario user) {
      this.user = user;
   }

   public Entity getEntity() {
      return entity;
   }

   public void setEntity(Entity entity) {
      this.entity = entity;
   }

   public OperationType getOperationType() {
      return operationType;
   }

   public void setOperationType(OperationType operationType) {
      this.operationType = operationType;
   }

   public String getOperationDescription() {
      return operationDescription;
   }

   public void setOperationDescription(String operationDescription) {
      this.operationDescription = operationDescription;
   }
   
}
