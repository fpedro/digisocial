package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public class HorarioDisponivel extends IdentifiableEntity {

  private Horario horarioInicio;
  private Horario horarioTermino;
  private DiaDaSemana dia;

  public Horario getHorarioInicio() {
    return horarioInicio;
  }

  public void setHorarioInicio(Horario horarioInicio) {
    this.horarioInicio = horarioInicio;
  }

  public Horario getHorarioTermino() {
    return horarioTermino;
  }

  public void setHorarioTermino(Horario horarioTermino) {
    this.horarioTermino = horarioTermino;
  }

  public DiaDaSemana getDia() {
    return dia;
  }

  public void setDia(DiaDaSemana dia) {
    this.dia = dia;
  }
  
}
