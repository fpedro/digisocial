package domain.implementation;

import domain.Entity;
import domain.Entity;
import domain.implementation.Usuario;

import java.util.Date;

/**
 *
 * @author Fábio Henrique
 */
public abstract class IdentifiableEntity implements Entity {

   private int id;
   private Date registry;
   private Usuario userOnAction;

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public Date getRegistry() {
      return registry;
   }

   public void setRegistry(Date registry) {
      this.registry = registry;
   }

   public Usuario getUserOnAction() {
      return userOnAction;
   }

   public void setUserOnAction(Usuario userOnAction) {
      this.userOnAction = userOnAction;
   }
  
}
