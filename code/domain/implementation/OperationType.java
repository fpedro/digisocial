package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public enum OperationType {
  SAVE,  
  VIEW,
  MODIFY
}
