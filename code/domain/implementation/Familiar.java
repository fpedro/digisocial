package domain.implementation;

/**
 *
 * @author Fabio Henrique
 */
public class Familiar {
  
   private String nome;
   private byte idade;
   private String parentesco;
   private String ocupacao;

   public String getNome() {
      return nome;
   }

   public void setNome(String nome) {
      this.nome = nome;
   }

   public byte getIdade() {
      return idade;
   }

   public void setIdade(byte idade) {
      this.idade = idade;
   }

   public String getParentesco() {
      return parentesco;
   }

   public void setParentesco(String parentesco) {
      this.parentesco = parentesco;
   }

   public String getOcupacao() {
      return ocupacao;
   }

   public void setOcupacao(String ocupacao) {
      this.ocupacao = ocupacao;
   }
   
}
