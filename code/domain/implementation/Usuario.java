package domain.implementation;

import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class Usuario extends IdentifiableEntity {

   private String nickName;
   private String nome;
   private String senha;
   private List<GrupoDePermissoes> permissoes;
   private boolean activeUser;

   public String getNickName() {
      return nickName;
   }

   public void setNickName(String nickName) {
      this.nickName = nickName;
   }

   public String getNome() {
      return nome;
   }

   public void setNome(String nome) {
      this.nome = nome;
   }

   public String getSenha() {
      return senha;
   }

   public void setSenha(String senha) {
      this.senha = senha;
   }

   public List<GrupoDePermissoes> getPermissoes() {
      return permissoes;
   }

   public void setPermissoes(List<GrupoDePermissoes> permissoes) {
      this.permissoes = permissoes;
   }

   public boolean isActiveUser() {
      return activeUser;
   }

   public void setActiveUser(boolean activeUser) {
      this.activeUser = activeUser;
   }
  
}
