package domain.implementation;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class Acolhido extends IdentifiableEntity {
   
   private String nomeAcolhido;
   private String foto;
   private Date dataNascimento;
   private Sexo sexo;
   private String nomeMae;
   private String CPFmae;
   private String RGmae;
   private String localTrabalhoMae;
   private String nomePai;
   private String CPFpai;
   private String RGpai;
   private String localTrabalhoPai;
   private Endereco endereco;
   private String comQuemVoltaParaCasa;
   private String telefoneResidencial;
   private String telefoneCelular;
   private PeriodoPS periodoPS;
   private byte quantasVezesNoProjeto;
   private boolean atendimentoPrioritario;
   private Escola escola;
   private String deficiencia;
   private String alergia;
   private String medicamento;
   private boolean bolsaFamilia;
   private List<Familiar> familiares;
   private String doencaGrave;
   private String tratamentoNeurologista;
   private String tratamentoPsiquiatra;
   private String tratamentoPsicologo;
   private String tratamentoPsicopedagogo;
   private String tratamentoFonoaudiologo;
   private String tratamentoOutros;
   private String problemaNaFamilia;
   private String gostaDeFazer;
   private String relacionamentoComFamilia;
   private Date desligamento;
   private Usuario userOnRegistry;
   private int firstRegistryId;
   private Date firstRegistryDate;
   private boolean acolhidoReinserido;


   public boolean estaSendoAtendido() {
      return desligamento == null;
   }

   public String getNomeAcolhido() {
      return nomeAcolhido;
   }

   public void setNomeAcolhido(String nomeAcolhido) {
      this.nomeAcolhido = nomeAcolhido;
   }

   public String getFoto() {
      return foto;
   }

   public void setFoto(String foto) {
      this.foto = foto;
   }

   public Date getDataNascimento() {
      return dataNascimento;
   }

   public void setDataNascimento(Date dataNascimento) {
      this.dataNascimento = dataNascimento;
   }

   public Sexo getSexo() {
      return sexo;
   }

   public void setSexo(Sexo sexo) {
      this.sexo = sexo;
   }

   public String getNomeMae() {
      return nomeMae;
   }

   public void setNomeMae(String nomeMae) {
      this.nomeMae = nomeMae;
   }

   public String getCPFmae() {
      return CPFmae;
   }

   public void setCPFmae(String CPFmae) {
      this.CPFmae = CPFmae;
   }

   public String getRGmae() {
      return RGmae;
   }

   public void setRGmae(String RGmae) {
      this.RGmae = RGmae;
   }

   public String getLocalTrabalhoMae() {
      return localTrabalhoMae;
   }

   public void setLocalTrabalhoMae(String localTrabalhoMae) {
      this.localTrabalhoMae = localTrabalhoMae;
   }

   public String getNomePai() {
      return nomePai;
   }

   public void setNomePai(String nomePai) {
      this.nomePai = nomePai;
   }

   public String getCPFpai() {
      return CPFpai;
   }

   public void setCPFpai(String CPFpai) {
      this.CPFpai = CPFpai;
   }

   public String getRGpai() {
      return RGpai;
   }

   public void setRGpai(String RGpai) {
      this.RGpai = RGpai;
   }

   public String getLocalTrabalhoPai() {
      return localTrabalhoPai;
   }

   public void setLocalTrabalhoPai(String localTrabalhoPai) {
      this.localTrabalhoPai = localTrabalhoPai;
   }

   public Endereco getEndereco() {
      return endereco;
   }

   public void setEndereco(Endereco endereco) {
      this.endereco = endereco;
   }

   public String getComQuemVoltaParaCasa() {
      return comQuemVoltaParaCasa;
   }

   public void setComQuemVoltaParaCasa(String comQuemVoltaParaCasa) {
      this.comQuemVoltaParaCasa = comQuemVoltaParaCasa;
   }

   public String getTelefoneResidencial() {
      return telefoneResidencial;
   }

   public void setTelefoneResidencial(String telefoneResidencial) {
      this.telefoneResidencial = telefoneResidencial;
   }

   public String getTelefoneCelular() {
      return telefoneCelular;
   }

   public void setTelefoneCelular(String telefoneCelular) {
      this.telefoneCelular = telefoneCelular;
   }

   public PeriodoPS getPeriodoPS() {
      return periodoPS;
   }

   public void setPeriodoPS(PeriodoPS periodoPS) {
      this.periodoPS = periodoPS;
   }

   public byte getQuantasVezesNoProjeto() {
      return quantasVezesNoProjeto;
   }

   public void setQuantasVezesNoProjeto(byte quantasVezesNoProjeto) {
      this.quantasVezesNoProjeto = quantasVezesNoProjeto;
   }

   public boolean isAtendimentoPrioritario() {
      return atendimentoPrioritario;
   }

   public void setAtendimentoPrioritario(boolean atendimentoPrioritario) {
      this.atendimentoPrioritario = atendimentoPrioritario;
   }

   public Escola getEscola() {
      return escola;
   }

   public void setEscola(Escola escola) {
      this.escola = escola;
   }

   public String getDeficiencia() {
      return deficiencia;
   }

   public void setDeficiencia(String deficiencia) {
      this.deficiencia = deficiencia;
   }

   public String getAlergia() {
      return alergia;
   }

   public void setAlergia(String alergia) {
      this.alergia = alergia;
   }

   public String getMedicamento() {
      return medicamento;
   }

   public void setMedicamento(String medicamento) {
      this.medicamento = medicamento;
   }

   public boolean isBolsaFamilia() {
      return bolsaFamilia;
   }

   public void setBolsaFamilia(boolean bolsaFamilia) {
      this.bolsaFamilia = bolsaFamilia;
   }

   public List<Familiar> getFamiliares() {
      return familiares;
   }

   public void setFamiliares(List<Familiar> familiares) {
      this.familiares = familiares;
   }

   public String getDoencaGrave() {
      return doencaGrave;
   }

   public void setDoencaGrave(String doencaGrave) {
      this.doencaGrave = doencaGrave;
   }

   public String getTratamentoNeurologista() {
      return tratamentoNeurologista;
   }

   public void setTratamentoNeurologista(String tratamentoNeurologista) {
      this.tratamentoNeurologista = tratamentoNeurologista;
   }

   public String getTratamentoPsiquiatra() {
      return tratamentoPsiquiatra;
   }

   public void setTratamentoPsiquiatra(String tratamentoPsiquiatra) {
      this.tratamentoPsiquiatra = tratamentoPsiquiatra;
   }

   public String getTratamentoPsicologo() {
      return tratamentoPsicologo;
   }

   public void setTratamentoPsicologo(String tratamentoPsicologo) {
      this.tratamentoPsicologo = tratamentoPsicologo;
   }

   public String getTratamentoPsicopedagogo() {
      return tratamentoPsicopedagogo;
   }

   public void setTratamentoPsicopedagogo(String tratamentoPsicopedagogo) {
      this.tratamentoPsicopedagogo = tratamentoPsicopedagogo;
   }

   public String getTratamentoFonoaudiologo() {
      return tratamentoFonoaudiologo;
   }

   public void setTratamentoFonoaudiologo(String tratamentoFonoaudiologo) {
      this.tratamentoFonoaudiologo = tratamentoFonoaudiologo;
   }

   public String getTratamentoOutros() {
      return tratamentoOutros;
   }

   public void setTratamentoOutros(String tratamentoOutros) {
      this.tratamentoOutros = tratamentoOutros;
   }

   public String getProblemaNaFamilia() {
      return problemaNaFamilia;
   }

   public void setProblemaNaFamilia(String problemaNaFamilia) {
      this.problemaNaFamilia = problemaNaFamilia;
   }

   public String getGostaDeFazer() {
      return gostaDeFazer;
   }

   public void setGostaDeFazer(String gostaDeFazer) {
      this.gostaDeFazer = gostaDeFazer;
   }

   public String getRelacionamentoComFamilia() {
      return relacionamentoComFamilia;
   }

   public void setRelacionamentoComFamilia(String relacionamentoComFamilia) {
      this.relacionamentoComFamilia = relacionamentoComFamilia;
   }

   public Date getDesligamento() {
      return desligamento;
   }

   public void setDesligamento(Date desligamento) {
      this.desligamento = desligamento;
   }

   public Usuario getUserOnRegistry() {
      return userOnRegistry;
   }

   public void setUserOnRegistry(Usuario userOnRegistry) {
      this.userOnRegistry = userOnRegistry;
   }

   public int getFirstRegistryId() {
      return firstRegistryId;
   }

   public void setFirstRegistryId(int firstRegistryId) {
      this.firstRegistryId = firstRegistryId;
   }

   public Date getFirstRegistryDate() {
      return firstRegistryDate;
   }

   public void setFirstRegistryDate(Date firstRegistryDate) {
      this.firstRegistryDate = firstRegistryDate;
   }

   public boolean isAcolhidoReinserido() {
      return acolhidoReinserido;
   }

   public void setAcolhidoReinserido(boolean acolhidoReinserido) {
      this.acolhidoReinserido = acolhidoReinserido;
   }

}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
