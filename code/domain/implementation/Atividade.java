package domain.implementation;

import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class Atividade extends IdentifiableEntity {
  
  private String nome;
  private String descricao;
  private List<Orientador> orientadores;

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public List<Orientador> getOrientadores() {
    return orientadores;
  }

  public void setOrientadores(List<Orientador> orientadores) {
    this.orientadores = orientadores;
  }

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }
  
  
  
}
