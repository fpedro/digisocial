package domain.implementation;

import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Fábio Henrique
 */
public class DadosDoMes extends IdentifiableEntity {

   private Month mes;
   private Year ano;
   private List<Acolhido> acolhidos;
   private List<Acolhido> acolhidosDesligados;
   private List<AtividadeAgendada> atividades;

   public DadosDoMes(Month mes, Year ano) {
      this.mes = mes;
      this.ano = ano;
   }
   
   public int contTotalAcolhidos() {
      return acolhidos.size();
   }

   public int contDesligamentos() {
      return acolhidosDesligados.size();
   }

   public int contDesligamentosMenosDe1mes() {
      Calendar c = new GregorianCalendar();
      Date d;
      int cont = 0;
      for (Acolhido i : acolhidosDesligados) {
         c.setTime(i.getFirstRegistryDate());
         c.add(Calendar.MONTH, 1);
         d = i.getDesligamento();
         if (c.getTimeInMillis() > d.getTime())
            cont++;
      }
      return cont;
   }

   public int contAcolhidosComMenosDe8Anos() {
      int cont = 0;
      Calendar c = new GregorianCalendar();
      c.setTime(getFinalDoMes());
      c.add(Calendar.YEAR, -8);
      Calendar dataNascimento = new GregorianCalendar();
      for (Acolhido i : acolhidos) {
         dataNascimento.setTime(i.getDataNascimento());
         if (c.compareTo(dataNascimento) < 0)
            cont++;
      }
      return cont;
   }

   public int contAcolhidosCom8AnosAteMenosDe10Anos() {
      int cont = 0;
      Calendar c1 = new GregorianCalendar();
      c1.setTime(getFinalDoMes());
      c1.add(Calendar.YEAR, -8);
      Calendar c2 = new GregorianCalendar();
      c2.setTime(getFinalDoMes());
      c2.add(Calendar.YEAR, -10);
      Calendar dataNascimento = new GregorianCalendar();
      for (Acolhido i : acolhidos) {
         dataNascimento.setTime(i.getDataNascimento());
         if (c1.compareTo(dataNascimento) >= 0 &&
             c2.compareTo(dataNascimento) < 0)
            cont++;
      }
      return cont;
   }

   public int contAcolhidosCom10AnosAteMenosDe12Anos() {
      int cont = 0;
      Calendar c1 = new GregorianCalendar();
      c1.setTime(getFinalDoMes());
      c1.add(Calendar.YEAR, -10);
      Calendar c2 = new GregorianCalendar();
      c2.setTime(getFinalDoMes());
      c2.add(Calendar.YEAR, -12);
      Calendar dataNascimento = new GregorianCalendar();
      for (Acolhido i : acolhidos) {
         dataNascimento.setTime(i.getDataNascimento());
         if (c1.compareTo(dataNascimento) >= 0 &&
             c2.compareTo(dataNascimento) < 0)
            cont++;
      }
      return cont;
   }
   
   public int contAcolhidosCom12AnosAteMenosDe14Anos() {
      int cont = 0;
      Calendar c1 = new GregorianCalendar();
      c1.setTime(getFinalDoMes());
      c1.add(Calendar.YEAR, -12);
      Calendar c2 = new GregorianCalendar();
      c2.setTime(getFinalDoMes());
      c2.add(Calendar.YEAR, -14);
      Calendar dataNascimento = new GregorianCalendar();
      for (Acolhido i : acolhidos) {
         dataNascimento.setTime(i.getDataNascimento());
         if (c1.compareTo(dataNascimento) >= 0 &&
             c2.compareTo(dataNascimento) < 0)
            cont++;
      }
      return cont;
   }

   public int contAcolhidosCom14AnosOuMais() {
      int cont = 0;
      Calendar c = new GregorianCalendar();
      c.setTime(getFinalDoMes());
      c.add(Calendar.YEAR, -14);
      Calendar dataNascimento = new GregorianCalendar();
      for (Acolhido i : acolhidos) {
         dataNascimento.setTime(i.getDataNascimento());
         if (c.compareTo(dataNascimento) >= 0)
            cont++;
      }
      return cont;
   }
   
   public int contFamiliasAtendidas() {
      int cont = 0;
      List<String> a = new ArrayList<>();
      String s;
      String c = "";
      for (Acolhido i : acolhidos) {
         if (i.getCPFmae() == null)
            i.setCPFmae("0");
         if (i.getCPFpai() == null)
            i.setCPFpai("0");
         s = i.getCPFmae();
         s = s.concat(i.getCPFpai());
         a.add(s);
      }
      a.sort(null);
      for (String i: a) {
         if (!c.equals(i))
            cont++;
         c = i;
      }
      return cont;
   }
   
   public int contMeninas() {
      int cont = 0;
      for (Acolhido c : acolhidos)
         if (c.getSexo() == Sexo.FEMININO)
            cont++;
      return cont;
   }

   public int contPeriodoPSManha() {
      int cont = 0;
      for (Acolhido c : acolhidos)
         if (c.getPeriodoPS() == PeriodoPS.MANHA)
            cont++;
      return cont;
   }

   public Map<String, Integer> contBairros() {
      Map<String, Integer> bairros = new HashMap<>();
      int cont = 0;
      for (Acolhido c : acolhidos)
         if ("Centro".equals(c.getEndereco().getBairro()))
            cont++;
      bairros.put("Centro", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("Ipiranga".equals(c.getEndereco().getBairro()))
            cont++;
      bairros.put("Ipiranga", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("Nogueira".equals(c.getEndereco().getBairro()))
            cont++;
      bairros.put("Nogueira", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("Itapema".equals(c.getEndereco().getBairro()))
            cont++;
      bairros.put("Itapema", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("Freguesia".equals(c.getEndereco().getBairro()))
            cont++;
      bairros.put("Freguesia", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("Maracatu".equals(c.getEndereco().getBairro()))
            cont++;
      bairros.put("Maracatu", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("Itaoca".equals(c.getEndereco().getBairro()))
            cont++;
      bairros.put("Itaoca", cont);
      return bairros;
   }

   public Map<String, Integer> contEscolas() {
      Map escolas = new HashMap<String, Integer>();
      int cont = 0;
      for (Acolhido c : acolhidos)
         if ("IB".equals(c.getEscola().getNomeAbreviado()))
            cont++;
      escolas.put("IB", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("GV".equals(c.getEscola().getNomeAbreviado()))
            cont++;
      escolas.put("GV", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("CL".equals(c.getEscola().getNomeAbreviado()))
            cont++;
      escolas.put("CL", cont);
      cont = 0;
      for (Acolhido c : acolhidos)
         if ("RF".equals(c.getEscola().getNomeAbreviado()))
            cont++;
      escolas.put("RF", cont);
      return escolas;
   }

   public Map<String, Integer> contPermanencia() {
      Map<String, Integer> permanencia = new HashMap<>();
      int cont = 0;
      Calendar c = new GregorianCalendar();
      Date d = getInicioDoMes();
      List<Acolhido> selecaoPermanencia = new ArrayList<>();
      List<Acolhido> selecaoPermanencia_aux = new ArrayList<>();
      selecaoPermanencia.addAll(acolhidos);
      selecaoPermanencia_aux.addAll(selecaoPermanencia);
      for (Acolhido i : selecaoPermanencia_aux) {
         c.setTime(i.getFirstRegistryDate());
         c.add(Calendar.YEAR, 3);
         if (c.getTimeInMillis() <= d.getTime()) {
            selecaoPermanencia.remove(i);
            cont++;
         }
      }
      permanencia.put("3anos", cont);
      selecaoPermanencia_aux.clear();
      selecaoPermanencia_aux.addAll(selecaoPermanencia);
      cont = 0;
      for (Acolhido i : selecaoPermanencia_aux) {
         c.setTime(i.getFirstRegistryDate());
         c.add(Calendar.YEAR, 2);
         if (c.getTimeInMillis() <= d.getTime()) {
            selecaoPermanencia.remove(i);
            cont++;
         }
      }
      permanencia.put("2anos", cont);
      selecaoPermanencia_aux.clear();
      selecaoPermanencia_aux.addAll(selecaoPermanencia);
      cont = 0;
      for (Acolhido i : selecaoPermanencia_aux) {
         c.setTime(i.getFirstRegistryDate());
         c.add(Calendar.YEAR, 1);
         if (c.getTimeInMillis() <= d.getTime()) {
            selecaoPermanencia.remove(i);
            cont++;
         }
      }
      permanencia.put("1ano", cont);
      selecaoPermanencia_aux.clear();
      selecaoPermanencia_aux.addAll(selecaoPermanencia);
      cont = 0;
      for (Acolhido i : selecaoPermanencia_aux) {
         c.setTime(i.getFirstRegistryDate());
         c.add(Calendar.MONTH, 6);
         if (c.getTimeInMillis() <= d.getTime()) {
            selecaoPermanencia.remove(i);
            cont++;
         }
      }
      permanencia.put("1semestre", cont);
      selecaoPermanencia_aux.clear();
      selecaoPermanencia_aux.addAll(selecaoPermanencia);
      cont = 0;
      for (Acolhido i : selecaoPermanencia_aux) {
         c.setTime(i.getFirstRegistryDate());
         c.add(Calendar.MONTH, 1);
         if (c.getTimeInMillis() <= d.getTime()) {
            selecaoPermanencia.remove(i);
            cont++;
         }
      }
      permanencia.put("1mes", cont);
      permanencia.put("menosDe1mes", selecaoPermanencia.size());
      return permanencia;
   }
   
   public Date getFinalDoMes() {
      int lastDay = mes.length(ano.isLeap());
      return new GregorianCalendar(ano.getValue(), mes.ordinal(), lastDay)
                  .getTime();
   }
   
   
   public Date getInicioDoProximoMes() {
      Calendar c = new GregorianCalendar(ano.getValue(), mes.ordinal(), 1);
      c.add(Calendar.MONTH, 1);
      return c.getTime();
   }

   public Date getInicioDoMes() {
      return new GregorianCalendar(ano.getValue(), mes.ordinal(), 1).getTime();
   }

   public Month getMes() {
      return mes;
   }

   public void setMes(Month mes) {
      this.mes = mes;
   }

   public Year getAno() {
      return ano;
   }

   public void setAno(Year ano) {
      this.ano = ano;
   }

   public void setacolhidos(List<Acolhido> acolhidos) {
      this.acolhidos = acolhidos;
      filtraracolhidosDesligadas();
   }

   public List<AtividadeAgendada> getAtividades() {
      return atividades;
   }

   public void setAtividades(List<AtividadeAgendada> atividades) {
      this.atividades = atividades;
   }

   private void filtraracolhidosDesligadas() {
      acolhidosDesligados = new ArrayList<>();
      Calendar c = new GregorianCalendar();
      for (Acolhido i : acolhidos) {
         if (!i.estaSendoAtendido()) {
            c.setTime(i.getDesligamento());
            if (Month.of(c.get(Calendar.MONTH) + 1) == mes &&
                ano.getValue() == c.get(Calendar.YEAR)) {
               acolhidosDesligados.add(i);
            }
         }
      }
   }
 
}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
