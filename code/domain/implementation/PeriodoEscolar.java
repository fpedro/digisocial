package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public enum PeriodoEscolar {
  MANHA,  
  TARDE
}
