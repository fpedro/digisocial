package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public class ObjetivoResultado {
 
  private String nome;
  private String descricao;
  private String resultadoEsperado;
  private boolean resultadoAlcancado;
  private String resultadoDescricao;

  public String getNome() {
    return nome;
  }

  public void setNome(String nome) {
    this.nome = nome;
  }

  public String getDescricao() {
    return descricao;
  }

  public void setDescricao(String descricao) {
    this.descricao = descricao;
  }

  public String getResultadoEsperado() {
    return resultadoEsperado;
  }

  public void setResultadoEsperado(String resultadoEsperado) {
    this.resultadoEsperado = resultadoEsperado;
  }

  public boolean isResultadoAlcancado() {
    return resultadoAlcancado;
  }

  public void setResultadoAlcancado(boolean resultadoAlcancado) {
    this.resultadoAlcancado = resultadoAlcancado;
  }

  public String getResultadoDescricao() {
    return resultadoDescricao;
  }

  public void setResultadoDescricao(String resultadoDescricao) {
    this.resultadoDescricao = resultadoDescricao;
  }
  
}
