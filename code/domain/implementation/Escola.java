package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public class Escola {
  
  private String nome;
  private PeriodoEscolar periodo;
  private byte ano;  
  
  public String getNome() {
    switch (nome) {
      case "GV": return "E. E. Getúlio Vargas";
      case "RF": return "E. E. Roberto Feijó";
      case "IB": return "E. E. Ivan Brasil";
      case "CL": return "E. M. Célia Leonor";      
    }
    return nome;
  }  

   public void setNome(String nome) {
      this.nome = nome;
   }
  
   public String getNomeAbreviado() {
     return nome;
   }

   public PeriodoEscolar getPeriodo() {
      return periodo;
   }

   public void setPeriodo(PeriodoEscolar periodo) {
      this.periodo = periodo;
   }

   public byte getAno() {
      return ano;
   }

   public void setAno(byte ano) {
      this.ano = ano;
   }
  
}
