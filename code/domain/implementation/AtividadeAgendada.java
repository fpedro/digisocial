package domain.implementation;

import java.util.Date;
import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class AtividadeAgendada extends IdentifiableEntity {
  
  private Date inicio;
  private Date termino;
  private Orientador orientador;
  private Atividade atividade;
  private List<ObjetivoResultado> objetivosresultados;

  public List<ObjetivoResultado> getObjetivosResultados() {
    return objetivosresultados;
  }

  public void setObjetivosResultados
                (List<ObjetivoResultado> objetivosresultados) {
    this.objetivosresultados = objetivosresultados;
  }

  public Orientador getOrientador() {
    return orientador;
  }

  public void setOrientador(Orientador orientador) {
    this.orientador = orientador;
  }

  public Atividade getAtividade() {
    return atividade;
  }

  public void setAtividade(Atividade atividade) {
    this.atividade = atividade;
  }
  
  public Date getInicio() {
    return inicio;
  }

  public void setInicio(Date inicio) {
    this.inicio = inicio;
  }

  public Date getTermino() {
    return termino;
  }

  public void setTermino(Date termino) {
    this.termino = termino;
  }
  
}
