package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public enum Sexo {
  FEMININO,
  MASCULINO  
}
