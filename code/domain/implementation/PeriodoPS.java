package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public enum PeriodoPS {
  MANHA,  
  TARDE
}
