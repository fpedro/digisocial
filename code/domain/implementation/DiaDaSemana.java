package domain.implementation;

/**
 *
 * @author Fábio Henrique
 */
public enum DiaDaSemana {
  DOMINGO,
  SEGUNDA,
  TERCA,
  QUARTA,
  QUINTA,
  SEXTA,
  SABADO
}
