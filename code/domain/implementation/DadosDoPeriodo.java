package domain.implementation;

import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class DadosDoPeriodo extends IdentifiableEntity {
  
  private List<DadosDoMes> dadosDosMeses = new ArrayList<>();  
  
  public DadosDoPeriodo(byte mesInicio, int anoInicio,
                         byte mesTermino, int anoTermino) {
      while (anoInicio < anoTermino ||
              anoInicio == anoTermino && mesInicio <= mesTermino) {
        DadosDoMes n;
        n = new DadosDoMes(Month.of(mesInicio), Year.of(anoInicio));
        mesInicio++;
        if (mesInicio == 12) {
          anoInicio++;
          mesInicio = 1;
        }
        dadosDosMeses.add(n);
      }
   }

   public List<DadosDoMes> getDadosDosMeses() {
      return dadosDosMeses;
   }

   public void setDadosDosMeses(List<DadosDoMes> dadosDosMeses) {
      this.dadosDosMeses = dadosDosMeses;
   }

}
