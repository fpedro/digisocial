package web.controller;

import domain.Entity;
import core.result.PSResult;
import domain.implementation.IdentifiableEntity;
import domain.implementation.Usuario;
import web.command.Command;
import web.command.implementation.ModifyCommand;
import web.command.implementation.SaveCommand;
import web.command.implementation.ViewCommand;
import web.viewhelper.ViewHelper;
import web.viewhelper.implementation.AcolhidoQueryViewHelper;
import web.viewhelper.implementation.AcolhidoViewHelper;
import web.viewhelper.implementation.DadosDoMesViewHelper;
import web.viewhelper.implementation.DadosDoPeriodoViewHelper;
import web.viewhelper.implementation.LogPSViewHelper;
import web.viewhelper.implementation.OrientadorViewHelper;
import web.viewhelper.implementation.UsuarioViewHelper;
import web.viewhelper.implementation.SessaoViewHelper;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;

/**
 *
 * @author Fábio Henrique
 */
@MultipartConfig
public class Controller extends HttpServlet {

  private static Map<String, Command> commands;
  private static Command defaultCommand;
  private static Map<String, ViewHelper> viewHelpers;

  
  public Controller() {
    
    commands = new HashMap<>();
    commands.put("SAVE", new SaveCommand());
    commands.put("MODIFY", new ModifyCommand());
    commands.put("VIEW", new ViewCommand());

    defaultCommand = new ViewCommand();
    
    viewHelpers = new HashMap<>();
    viewHelpers.put("/app/acolhido.do", new AcolhidoViewHelper());
    viewHelpers.put("/app/orientador.do", new OrientadorViewHelper());
    viewHelpers.put("/app/dados-mes.do", new DadosDoMesViewHelper());
    viewHelpers.put("/app/dados-periodo.do", new DadosDoPeriodoViewHelper());
    viewHelpers.put("/app/usuario.do", new UsuarioViewHelper());
    viewHelpers.put("/app/acolhido-query.do", new AcolhidoQueryViewHelper());
    viewHelpers.put("/app/logs.do", new LogPSViewHelper());
    viewHelpers.put("/app/sessao.do", new SessaoViewHelper());
    
    viewHelpers.put("/autenticacao.do", new SessaoViewHelper());
    
  }

  /**
   * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
   * methods.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */  
  public void processRequest(HttpServletRequest request,
                               HttpServletResponse response)
                throws ServletException, IOException {
    Entity entity;
    Usuario u = (Usuario)request.getSession().getAttribute("activeUser");
    String requestPath = request.getRequestURI().substring(
                           request.getContextPath().length());
    String operation = request.getParameter("operation");
    PSResult result;
    ViewHelper viewHelper;
    viewHelper = viewHelpers.get(requestPath);
    entity = viewHelper.getEntity(request);
    ((IdentifiableEntity)entity).setUserOnAction(u);
    result = commands.getOrDefault(operation, defaultCommand)
               .execute(entity);
    viewHelper.sendResponse(result, response);
  }

  // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code." >
  /**
   * Handles the HTTP <code>GET</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Handles the HTTP <code>POST</code> method.
   *
   * @param request servlet request
   * @param response servlet response
   * @throws ServletException if a servlet-specific error occurs
   * @throws IOException if an I/O error occurs
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response)
          throws ServletException, IOException {
    processRequest(request, response);
  }

  /**
   * Returns a short description of the servlet.
   *
   * @return a String containing servlet description
   */
  @Override
  public String getServletInfo() {
    return "servlet description";
  }// </editor-fold>

  
}
