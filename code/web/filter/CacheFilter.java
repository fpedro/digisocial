package web.filter;

import java.io.IOException;
import jakarta.servlet.Filter;
import jakarta.servlet.FilterChain;
import jakarta.servlet.FilterConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.ServletRequest;
import jakarta.servlet.ServletResponse;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Fábio Henrique
 */
public class CacheFilter implements Filter {

   private FilterConfig filterConfig = null;

   public CacheFilter() {
   }

   /**
    *
    * @param request The servlet request we are processing
    * @param response The servlet response we are creating
    * @param chain The filter chain we are processing
    *
    * @exception IOException if an input/output error occurs
    * @exception ServletException if a servlet error occurs
    */
   @Override
   public void doFilter(ServletRequest request, ServletResponse response,
         FilterChain chain)
         throws IOException, ServletException {
      HttpServletResponse httpResponse = (HttpServletResponse) response;
      httpResponse.setHeader("Cache-Control",
                             "no-store");
      chain.doFilter(request, response);
   }

   @Override
   public void destroy() {
   }

   @Override
   public void init(FilterConfig filterConfig) {
      this.filterConfig = filterConfig;
   }

}
