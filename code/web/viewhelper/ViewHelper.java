
package web.viewhelper;

import java.io.IOException;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import core.result.PSResult;
import domain.Entity;

/**
 *
 * @author Fábio Henrique
 */
public interface ViewHelper {

  public Entity getEntity(HttpServletRequest request);

  public void sendResponse(PSResult resultado,
                           HttpServletResponse response)
                throws IOException, ServletException;	
}
