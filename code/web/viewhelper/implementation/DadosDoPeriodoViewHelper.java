package web.viewhelper.implementation;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.DadosDoMes;
import domain.implementation.DadosDoPeriodo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import web.viewhelper.ViewHelper;

/**
 *
 * @author Fábio Henrique
 */
public class DadosDoPeriodoViewHelper implements ViewHelper {

  @Override
  public Entity getEntity(HttpServletRequest request) {
    byte mesInicio = 1;
    int anoInicio = 1;
    byte mesTermino = 1;
    int anoTermino = 1;
    
    DadosDoPeriodo n;
    String s;
    
    s = request.getParameter("mesInicio");
    if (s != null)
        mesInicio = Byte.parseByte(s);
    s = request.getParameter("anoInicio");
    if (s != null)
        anoInicio = Integer.parseInt(s);
    s = request.getParameter("mesTermino");
    if (s != null)
      mesTermino = Byte.parseByte(s);
    s = request.getParameter("anoTermino");
    if (s != null)
      anoTermino = Integer.parseInt(s);
    n = new DadosDoPeriodo(mesInicio, anoInicio, mesTermino, anoTermino);
    return n;
  }

  @Override
  public void sendResponse(PSResult resultado, HttpServletResponse response)
          throws IOException, ServletException {
    List entities = resultado.getEntities();
    PrintWriter out;
    out = response.getWriter();
    String contentJSON;
    if (resultado.getMessage() == null) {
      DadosDoPeriodo d = (DadosDoPeriodo) entities.get(0);
      contentJSON = "{\"dadosDoPeriodo\": [ \n";
      if (!d.getDadosDosMeses().isEmpty()) {
        for (DadosDoMes i : d.getDadosDosMeses()) {
          contentJSON += "{\"mes\": " + i.getMes().getValue() + ", ";
          contentJSON += "\"ano\": " + i.getAno().getValue() + ", ";
          contentJSON += "\"acolhidosAtendidas\": " + 
                          i.contTotalAcolhidos() + ", ";
          contentJSON += "\"desligamentos\": " + 
                          i.contDesligamentos() + ", ";
          contentJSON += "\"acolhidosComMenosDe8Anos\" : " + 
                          i.contAcolhidosComMenosDe8Anos()+ ", ";
          contentJSON += "\"acolhidosCom8AnosAteMenosDe10Anos\": " + 
                          i.contAcolhidosCom8AnosAteMenosDe10Anos()+ ", ";
          contentJSON += "\"acolhidosCom14AnosOuMais\": " + 
                          i.contAcolhidosCom14AnosOuMais()+ "},\n";
        }
        contentJSON = contentJSON.substring(0, contentJSON.length() - 2);
      }
      contentJSON += "]\n}";
    } else
      contentJSON = "{\"erro\": \"" + resultado.getMessage() + "\"\n}";
    out.println(contentJSON);
  }
}