package web.viewhelper.implementation;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.DadosDoMes;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.Month;
import java.time.Year;
import java.util.List;
import java.util.Map;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import web.viewhelper.ViewHelper;

/**
 *
 * @author Fábio Henrique
 */
public class DadosDoMesViewHelper implements ViewHelper {

  @Override
  public Entity getEntity(HttpServletRequest request) {
    byte mes = 1;
    int ano = 1;
    DadosDoMes n;
    String s;
    s = request.getParameter("mes");
    if (s != null)
      mes = Byte.parseByte(s);
    s = request.getParameter("ano");
    if (s != null)
      ano = Integer.parseInt(s);
    n = new DadosDoMes(Month.of(mes), Year.of(ano));
    return n;
  }

  @Override
  public void sendResponse(PSResult resultado, HttpServletResponse response)
          throws IOException, ServletException {
    List entities = resultado.getEntities();
    
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");

    String contentJSON;
    PrintWriter out = response.getWriter();
    
    if (resultado.getMessage() == null) {
      DadosDoMes d = (DadosDoMes) entities.get(0);
      contentJSON = "{\"dadosDoMes\": \"" + d.getInicioDoMes() + "\", ";
      contentJSON += "\"totalAcolhidos\": " +
                     d.contTotalAcolhidos() + ", ";
      contentJSON += "\"desligamentos\": " + d.contDesligamentos() + ", ";
      contentJSON += "\"desligamentosMenosDe1mes\": " +
                     d.contDesligamentosMenosDe1mes() + ", ";
      contentJSON += "\"acolhidosComMenosDe8Anos\": " +
                     d.contAcolhidosComMenosDe8Anos() + ", ";
      contentJSON += "\"acolhidosCom8AnosAteMenosDe10Anos\": " +
                     d.contAcolhidosCom8AnosAteMenosDe10Anos() + ", ";
      contentJSON += "\"acolhidosCom10AnosAteMenosDe12Anos\": " +
                     d.contAcolhidosCom10AnosAteMenosDe12Anos() + ", ";
      contentJSON += "\"acolhidosCom12AnosAteMenosDe14Anos\": " +
                     d.contAcolhidosCom12AnosAteMenosDe14Anos() + ", ";
      contentJSON += "\"acolhidosCom14AnosOuMais\": " +
                     d.contAcolhidosCom14AnosOuMais() + ", ";
      contentJSON += "\"familiasAtendidas\": " +
                     d.contFamiliasAtendidas() + ", ";
      contentJSON += "\"meninas\": \"" + d.contMeninas() + "\", ";
      contentJSON += "\"periodoPSManha\": \"" + d.contPeriodoPSManha()+ "\", ";
      Map permanencia = d.contPermanencia();
      contentJSON += "\"permanencia\": {\n";
      contentJSON += "\"com3anos\": " + permanencia.get("3anos") + ", ";
      contentJSON += "\"com2anos\": " + permanencia.get("2anos") + ", ";
      contentJSON += "\"com1ano\": " + permanencia.get("1ano") + ", ";
      contentJSON += "\"com1semestre\": " +
                     permanencia.get("1semestre") + ", ";
      contentJSON += "\"com1mes\": " + permanencia.get("1mes") + ", ";
      contentJSON += "\"menosDe1mes\": " +
                     permanencia.get("menosDe1mes") + " },\n";
      Map bairros = d.contBairros();
      contentJSON += "\"bairros\": {\n";
      contentJSON += "\"centro\": " + bairros.get("Centro") + ", ";
      contentJSON += "\"ipiranga\": " + bairros.get("Ipiranga") + ", ";
      contentJSON += "\"nogueira\": " + bairros.get("Nogueira") + ", ";
      contentJSON += "\"itapema\": " + bairros.get("Itapema") + ", ";
      contentJSON += "\"freguesia\": " + bairros.get("Freguesia") + ", ";
      contentJSON += "\"maracatu\": " + bairros.get("Maracatu") + ", ";
      contentJSON += "\"itaoca\": " + bairros.get("Itaoca") + " },\n";
      Map escolas = d.contEscolas();
      contentJSON += "\"escolas\": {\n";
      contentJSON += "\"IB\": " + escolas.get("IB") + ", ";
      contentJSON += "\"GV\": " + escolas.get("GV") + ", ";
      contentJSON += "\"CL\": " + escolas.get("CL") + ", ";
      contentJSON += "\"RF\": " + escolas.get("RF") + " }\n";
      contentJSON += "}";
    } else
      contentJSON = "{\"erro\": \"" + resultado.getMessage() + "\"\n}";
    out.println(contentJSON);
  }
}
/* vi: set sts=2 ts=2 sw=2 et fenc=utf-8 ff=dos: */ 
