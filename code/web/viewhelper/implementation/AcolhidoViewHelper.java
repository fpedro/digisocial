package web.viewhelper.implementation;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.Acolhido;
import domain.implementation.Endereco;
import domain.implementation.Escola;
import domain.implementation.Familiar;
import domain.implementation.PeriodoEscolar;
import domain.implementation.PeriodoPS;
import domain.implementation.Sexo;
import web.viewhelper.ViewHelper;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.ArrayList;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoViewHelper implements ViewHelper {

   @Override
   public Entity getEntity(HttpServletRequest request) {
      Acolhido acolhido = new Acolhido();
      String s;
      s = request.getParameter("id");
      if (s != null)
         acolhido.setId(Integer.parseInt(s));
      acolhido.setNomeAcolhido(request.getParameter("nomeAcolhido"));
      acolhido.setFoto(request.getParameter("foto"));
      s = request.getParameter("dataNascimento");
      if (s != null)
         acolhido.setDataNascimento(new Date(Long.parseLong(s)));
      s = request.getParameter("sexo");
      if ("FEMININO".equals(s))
         acolhido.setSexo(Sexo.FEMININO);
      else if ("MASCULINO".equals(s))
         acolhido.setSexo(Sexo.MASCULINO);
      acolhido.setNomeMae(request.getParameter("nomeMae"));
      acolhido.setRGmae(request.getParameter("RGmae"));
      acolhido.setCPFmae(request.getParameter("CPFmae"));
      acolhido.setLocalTrabalhoMae(request.getParameter("localTrabalhoMae"));
      acolhido.setNomePai(request.getParameter("nomePai"));
      acolhido.setRGpai(request.getParameter("RGpai"));
      acolhido.setCPFpai(request.getParameter("CPFpai"));
      acolhido.setLocalTrabalhoPai(request.getParameter("localTrabalhoPai"));    
      acolhido.setEndereco(new Endereco());
      acolhido.getEndereco().setRua(request.getParameter("endereco.rua"));
      s = request.getParameter("endereco.numero");
      if (s != null)
         acolhido.getEndereco().setNumero(Integer.parseInt(s));
      acolhido.getEndereco().setComplemento(request
                                .getParameter("endereco.complemento"));
      acolhido.getEndereco().setBairro(request.getParameter("endereco.bairro"));
      acolhido.getEndereco().setCidade(request.getParameter("endereco.cidade"));
      acolhido.setComQuemVoltaParaCasa(
                   request.getParameter("comQuemVoltaParaCasa"));
      acolhido.setTelefoneResidencial(request.getParameter("telefoneResidencial"));
      acolhido.setTelefoneCelular(request.getParameter("telefoneCelular"));
      s = request.getParameter("periodoPS");
      if ("MANHA".equals(s))
         acolhido.setPeriodoPS(PeriodoPS.MANHA);
      else if ("TARDE".equals(s))
         acolhido.setPeriodoPS(PeriodoPS.TARDE);
      s = request.getParameter("quantasVezesNoProjeto");
      if (s != null)
         acolhido.setQuantasVezesNoProjeto(Byte.parseByte(s));
      s = request.getParameter("atendimentoPrioritario");
      if ("SIM".equals(s))
         acolhido.setAtendimentoPrioritario(true);
      acolhido.setEscola(new Escola());
      acolhido.getEscola().setNome(request.getParameter("escola.nome"));
      s = request.getParameter("escola.periodo");
      if ("MANHA".equals(s))
         acolhido.getEscola().setPeriodo(PeriodoEscolar.MANHA);
      else if ("TARDE".equals(s))
         acolhido.getEscola().setPeriodo(PeriodoEscolar.TARDE);
      s = request.getParameter("escola.ano");
      if (s != null)
         acolhido.getEscola().setAno(Byte.parseByte(s));
      acolhido.setDeficiencia(request.getParameter("deficiencia"));
      acolhido.setAlergia(request.getParameter("alergia"));
      acolhido.setMedicamento(request.getParameter("medicamento"));
      s = request.getParameter("bolsaFamilia");
      if ("SIM".equals(s))
         acolhido.setBolsaFamilia(true);
      acolhido.setFamiliares(new ArrayList<>());
      String[] a = request.getParameterValues("familiares.nome");
      if (a != null) {
         for (String i : a) {
            Familiar f = new Familiar();
            f.setNome(i);
            acolhido.getFamiliares().add(f);
         }
         a = request.getParameterValues("familiares.idade");
         for (int i = 0; i < a.length; i++) {
            if (!a[i].isBlank())
               acolhido.getFamiliares().get(i).setIdade(Byte.parseByte(a[i]));
         }
         a = request.getParameterValues("familiares.parentesco");
         for (int i = 0; i < a.length; i++)
            acolhido.getFamiliares().get(i).setParentesco(a[i]);
         a = request.getParameterValues("familiares.ocupacao");
         for (int i = 0; i < a.length; i++)
            acolhido.getFamiliares().get(i).setOcupacao(a[i]);
      }
      acolhido.setDoencaGrave(request.getParameter("doencaGrave"));
      acolhido.setTratamentoNeurologista(
                   request.getParameter("tratamentoNeurologista"));
      acolhido.setTratamentoPsiquiatra(
                   request.getParameter("tratamentoPsiquiatra"));
      acolhido.setTratamentoPsicologo(
                   request.getParameter("tratamentoPsicologo"));
      acolhido.setTratamentoPsicopedagogo(
                   request.getParameter("tratamentoPsicopedagogo"));
      acolhido.setTratamentoFonoaudiologo(
                   request.getParameter("tratamentoFonoaudiologo"));
      acolhido.setTratamentoOutros(
                   request.getParameter("tratamentoOutros"));
      acolhido.setProblemaNaFamilia(request.getParameter("problemaNaFamilia"));
      acolhido.setGostaDeFazer(request.getParameter("gostaDeFazer"));
      acolhido.setRelacionamentoComFamilia(
                request.getParameter("relacionamentoComFamilia"));
      s = request.getParameter("desligamento");
      if (s != null)
         acolhido.setDesligamento(new Date());
      return acolhido;
   }

   @Override
   public void sendResponse(PSResult result, HttpServletResponse response)
               throws IOException, ServletException {
      Acolhido a;
      
      response.setContentType("application/json");
      response.setCharacterEncoding("UTF-8");

      String contentJSON;
      PrintWriter out = response.getWriter();

      if (result.isValid()) {

         a = (Acolhido)result.getEntities().get(0);
         contentJSON = "{ \"id\": " + a.getId() + ", ";
         contentJSON += "\"dataCadastro\": " + a.getRegistry().getTime() + ", ";
         contentJSON += "\"nomeAcolhido\": \"" + a.getNomeAcolhido() + "\", ";
         if (a.getFoto() != null)
            contentJSON += "\"foto\": \"" + a.getFoto() + "\", ";
         if (a.getDataNascimento() != null)
            contentJSON += "\"dataNascimento\": " +
                            a.getDataNascimento().getTime() + ", ";
         contentJSON += "\"sexo\": \"" +
                          a.getSexo() + "\", ";
         if (a.getNomeMae() != null)
            contentJSON += "\"nomeMae\": \"" + a.getNomeMae() + "\", ";
         contentJSON += "\"RGmae\": \"" + a.getRGmae()+ "\", ";
         if (a.getCPFmae() != null)
            contentJSON += "\"CPFmae\": " + a.getCPFmae()+ ", ";
         contentJSON += "\"localTrabalhoMae\": \"" +
                          a.getLocalTrabalhoMae() + "\", ";
         if (a.getNomePai() != null)
            contentJSON += "\"nomePai\": \"" + a.getNomePai() + "\", ";
         contentJSON += "\"RGpai\": \"" + a.getRGpai()+ "\", ";
         if (a.getCPFpai() != null)
            contentJSON += "\"CPFpai\": " + a.getCPFpai()+ ", ";
         contentJSON += "\"localTrabalhoPai\": \"" +
                          a.getLocalTrabalhoPai() + "\", ";
         contentJSON += "\"ruaEndereco\": \"" +
                          a.getEndereco().getRua() + "\", ";
         contentJSON += "\"numeroEndereco\": " +
                          a.getEndereco().getNumero() + ", ";
         contentJSON += "\"complementoEndereco\": \"" +
                          a.getEndereco().getComplemento() + "\", ";
         contentJSON += "\"bairroEndereco\": \"" +
                          a.getEndereco().getBairro() + "\", ";
         contentJSON += "\"cidadeEndereco\": \"" +
                          a.getEndereco().getCidade() + "\", ";
         if (a.getComQuemVoltaParaCasa() != null)
            contentJSON += "\"comQuemVoltaParaCasa\": \"" +
                            a.getComQuemVoltaParaCasa() + "\", ";
         contentJSON += "\"telefoneResidencial\": \"" +
                          a.getTelefoneResidencial() + "\", ";
         contentJSON += "\"telefoneCelular\": \"" +
                          a.getTelefoneCelular() + "\", ";
         contentJSON += "\"periodoPS\": \"" + a.getPeriodoPS() + "\", ";
         contentJSON += "\"quantasVezesNoProjeto\": " +
                          a.getQuantasVezesNoProjeto() + ", ";
         contentJSON += "\"atendimentoPrioritario\": " +
                          a.isAtendimentoPrioritario() + ", ";
         contentJSON += "\"nomeEscola\": \"" +
                          a.getEscola().getNomeAbreviado()+ "\", ";
         contentJSON += "\"periodoEscola\": \"" +
                          a.getEscola().getPeriodo() + "\", ";
         contentJSON += "\"anoEscola\": " +
                          a.getEscola().getAno() + ", ";
         if (a.getDeficiencia() != null)
            contentJSON += "\"deficiencia\": \"" + a.getDeficiencia()+ "\", ";
         if (a.getAlergia() != null)
            contentJSON += "\"alergia\": \"" + a.getAlergia()+ "\", ";
         if (a.getMedicamento() != null)
            contentJSON += "\"medicamento\": \"" + a.getMedicamento()+ "\", ";
         contentJSON += "\"bolsaFamilia\": " + a.isBolsaFamilia()+ ", ";
         contentJSON += "\"familiares\": [\n";
         if (!a.getFamiliares().isEmpty()) {
            for (Familiar f: a.getFamiliares()) {
               contentJSON += "{ \"nome\": \"" + f.getNome() + "\", ";
               contentJSON += "\"idade\": " + f.getIdade() + ", ";
               contentJSON += "\"parentesco\": \"" + f.getParentesco() + "\", ";
               contentJSON += "\"ocupacao\": \"" + f.getOcupacao() + "\" },\n";
            }
            contentJSON = contentJSON.substring(0, contentJSON.length() - 2);
         } 
         contentJSON += "],\n";
         contentJSON += "\"doencaGrave\": \"" + a.getDoencaGrave() + "\", ";
         if (a.getTratamentoNeurologista() != null)
            contentJSON += "\"tratamentoNeurologista\": \"" +
                            a.getTratamentoNeurologista() + "\", ";
         if (a.getTratamentoPsiquiatra() != null)
            contentJSON += "\"tratamentoPsiquiatra\": \"" +
                            a.getTratamentoPsiquiatra()+ "\", ";
         if (a.getTratamentoPsicologo() != null)
            contentJSON += "\"tratamentoPsicologo\": \"" +
                            a.getTratamentoPsicologo() + "\", ";
         if (a.getTratamentoPsicopedagogo() != null)
            contentJSON += "\"tratamentoPsicopedagogo\": \"" +
                            a.getTratamentoPsicopedagogo() + "\", ";
         if (a.getTratamentoFonoaudiologo() != null)
            contentJSON += "\"tratamentoFonoaudiologo\": \"" +
                            a.getTratamentoFonoaudiologo() + "\", ";
         if (a.getTratamentoOutros() != null)
            contentJSON += "\"tratamentoOutros\": \"" +
                            a.getTratamentoOutros() + "\", ";
         contentJSON += "\"problemaNaFamilia\": \"" +
                          a.getProblemaNaFamilia() + "\", ";
         contentJSON += "\"gostaDeFazer\": \"" + a.getGostaDeFazer()+ "\", ";
         contentJSON += "\"relacionamentoComFamilia\": \"" +
                          a.getRelacionamentoComFamilia() + "\", ";
         contentJSON += "\"userOnRegistry\": {";
         contentJSON += "\"id\": " + a.getUserOnRegistry().getId() + ", ";
         contentJSON += "\"nome\": \"" +
                          a.getUserOnRegistry().getNickName() + "\" } \n";
         if (a.getDesligamento() != null)
            contentJSON += ", \"desligamento\": " +
                            a.getDesligamento().getTime();
         contentJSON += " }";
      } else {
         contentJSON = "{ \"exception\": \"" + result.getMessage() + "\" }";
      }
      out.println(contentJSON);
   }

}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
