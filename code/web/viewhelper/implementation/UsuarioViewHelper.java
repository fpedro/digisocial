package web.viewhelper.implementation;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.Usuario;
import java.util.List;
import web.viewhelper.ViewHelper;

/**
 *
 * @author Fábio Henrique
 */
public class UsuarioViewHelper implements ViewHelper {

  @Override
  public Entity getEntity(HttpServletRequest request) {
    Usuario u = new Usuario();
    
    return u;
  }

  @Override
  public void sendResponse(PSResult result, HttpServletResponse response)
          throws IOException, ServletException {
    
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");

    String contentJSON;
    PrintWriter out = response.getWriter();

    if (result.isValid()) {
        contentJSON = "{ \"usuarios\": [\n";
        List<Entity> usuarios = (List)result.getEntities();
        if (!usuarios.isEmpty()) {
          for (Entity e : usuarios) {
            Usuario u = (Usuario)e;
            contentJSON += "{ \"id\": " + u.getId();
            contentJSON += " },\n";
          }
          contentJSON = contentJSON.substring(0, contentJSON.length() - 2);
        }
        contentJSON += "]\n}";
      } else {
      contentJSON = "{\"exception\": \"" + result.getMessage() + "\"}";
    }
    out.println(contentJSON);
  }

}
