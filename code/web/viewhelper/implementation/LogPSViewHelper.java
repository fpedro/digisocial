package web.viewhelper.implementation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.LogPS;
import web.viewhelper.ViewHelper;

/**
 *
 * @author Fábio Henrique
 */
public class LogPSViewHelper implements ViewHelper {

  @Override
  public Entity getEntity(HttpServletRequest request) {
    LogPS log = new LogPS();
    return log;
  }

  @Override
  public void sendResponse(PSResult result, HttpServletResponse response)
          throws IOException, ServletException {
    
    response.setContentType("application/json");
    response.setCharacterEncoding("UTF-8");

    String contentJSON;
    PrintWriter out = response.getWriter();

    if (result.isValid()) {
      contentJSON = "{ \"logs\": [\n";
      List<LogPS> logs = (List)result.getEntities();
      if (!logs.isEmpty()) {
        for (LogPS l : logs) {
          contentJSON += "{ \"id\": " + l.getId() + ", ";
          contentJSON += "\"operationDate\": " +
                           l.getData().getTime() + ", ";
          contentJSON += "\"userId\": " + l.getUser().getId() + ", ";
          contentJSON += "\"userName\":  \"" + l.getUser().getNome() + "\", ";
          contentJSON += "\"operationDescription\": \"" +
                           l.getOperationDescription() + "\"";
          contentJSON += " },\n";
        }
        contentJSON = contentJSON.substring(0, contentJSON.length() - 2);
      }
      contentJSON += "]\n}";
    } else {
      contentJSON = "{ \"exception\": \"" + result.getMessage() + "\" }";
    }
    out.println(contentJSON);
  }

}
