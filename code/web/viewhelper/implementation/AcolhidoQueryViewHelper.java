package web.viewhelper.implementation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.Acolhido;
import domain.implementation.AcolhidoQuery;
import domain.implementation.Endereco;
import domain.implementation.Escola;
import domain.implementation.PeriodoEscolar;
import domain.implementation.PeriodoPS;
import domain.implementation.Sexo;

import web.viewhelper.ViewHelper;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoQueryViewHelper implements ViewHelper {

   @Override
   public Entity getEntity(HttpServletRequest request) {
      AcolhidoQuery acolhidoQuery = new AcolhidoQuery();
      Acolhido parameters = new Acolhido();
      String s;
      s = request.getParameter("id");
      if (s != null) {
         parameters.setId(Integer.parseInt(s));
      }
      parameters.setNomeAcolhido(request.getParameter("nomeAcolhido"));
      s = request.getParameter("dataNascimento");
      if (s != null) {
         parameters.setDataNascimento(new Date(Long.parseLong(s)));
      }
      s = request.getParameter("sexo");
      if ("FEMININO".equals(s)) {
         parameters.setSexo(Sexo.FEMININO);
      } else if ("MASCULINO".equals(s)) {
         parameters.setSexo(Sexo.MASCULINO);
      }
      parameters.setNomeMae(request.getParameter("nomeMae"));
      parameters.setRGmae(request.getParameter("RGmae"));
      parameters.setCPFmae(request.getParameter("CPFmae"));
      parameters.setLocalTrabalhoMae(request.getParameter("localTrabalhoMae"));
      parameters.setNomePai(request.getParameter("nomePai"));
      parameters.setRGpai(request.getParameter("RGpai"));
      parameters.setCPFpai(request.getParameter("CPFpai"));
      parameters.setLocalTrabalhoPai(request.getParameter("localTrabalhoPai"));
      parameters.setEndereco(new Endereco());
      parameters.getEndereco().setRua(request.getParameter("endereco.rua"));
      s = request.getParameter("endereco.numero");
      if (s != null) {
         parameters.getEndereco().setNumero(Integer.parseInt(s));
      }
      parameters.getEndereco().setComplemento(request
            .getParameter("endereco.complemento"));
      parameters.getEndereco().setBairro(request.getParameter("endereco.bairro"));
      parameters.getEndereco().setCidade(request.getParameter("endereco.cidade"));
      parameters.setComQuemVoltaParaCasa(
            request.getParameter("comQuemVoltaParaCasa"));
      parameters.setTelefoneResidencial(
            request.getParameter("telefoneResidencial"));
      parameters.setTelefoneCelular(request.getParameter("telefoneCelular"));
      s = request.getParameter("periodoPS");
      if ("MANHA".equals(s)) {
         parameters.setPeriodoPS(PeriodoPS.MANHA);
      } else if ("TARDE".equals(s)) {
         parameters.setPeriodoPS(PeriodoPS.TARDE);
      }
      s = request.getParameter("quantasVezesNoProjeto");
      if (s != null) {
         parameters.setQuantasVezesNoProjeto(Byte.parseByte(s));
      }
      s = request.getParameter("atendimentoPrioritario");
      if ("SIM".equals(s)) {
         parameters.setAtendimentoPrioritario(true);
      }
      parameters.setEscola(new Escola());
      parameters.getEscola().setNome(request.getParameter("escola.nome"));
      s = request.getParameter("escola.periodo");
      if ("MANHA".equals(s)) {
         parameters.getEscola().setPeriodo(PeriodoEscolar.MANHA);
      } else if ("TARDE".equals(s)) {
         parameters.getEscola().setPeriodo(PeriodoEscolar.TARDE);
      }
      s = request.getParameter("escola.ano");
      if (s != null) {
         parameters.getEscola().setAno(Byte.parseByte(s));
      }
      parameters.setDeficiencia(request.getParameter("deficiencia"));
      parameters.setAlergia(request.getParameter("alergia"));
      parameters.setMedicamento(request.getParameter("medicamento"));
      s = request.getParameter("bolsaFamilia");
      if ("SIM".equals(s)) {
         parameters.setBolsaFamilia(true);
      }
      parameters.setDoencaGrave(request.getParameter("doencaGrave"));
      parameters.setTratamentoNeurologista(
            request.getParameter("tratamentoNeurologista"));
      parameters.setTratamentoPsiquiatra(
            request.getParameter("tratamentoPsiquiatra"));
      parameters.setTratamentoPsicologo(
            request.getParameter("tratamentoPsicologo"));
      parameters.setTratamentoPsicopedagogo(
            request.getParameter("tratamentoPsicopedagogo"));
      parameters.setTratamentoFonoaudiologo(
            request.getParameter("tratamentoFonoaudiologo"));
      parameters.setTratamentoOutros(
            request.getParameter("tratamentoOutros"));
      s = request.getParameter("desligamento");
      if (s != null) {
         parameters.setDesligamento(new Date(Long.parseLong(s)));
      }
      s = request.getParameter("reinsercao");
      if (s != null) {
         parameters.setRegistry(new Date(-1));
      }
      acolhidoQuery.setParameters(parameters);
      return acolhidoQuery;
   }

   @Override
   public void sendResponse(PSResult result, HttpServletResponse response)
         throws IOException, ServletException {
      Acolhido a;

      response.setContentType("application/json");
      response.setCharacterEncoding("UTF-8");

      String contentJSON;
      PrintWriter out = response.getWriter();

      if (result.isValid()) {
         contentJSON = "{ \"acolhidos\": [\n";
         List<Entity> acolhidos = (List) result.getEntities();
         if (!acolhidos.isEmpty()) {
            for (Entity e : acolhidos) {
               a = (Acolhido) e;
               contentJSON += "{ \"id\": " + a.getId() + ", ";
               contentJSON += "\"dataCadastro\": "
                     + a.getRegistry().getTime() + ", ";
               contentJSON += "\"nomeAcolhido\": \"" +
                              a.getNomeAcolhido() + "\", ";
               if (a.getFoto() != null) {
                  contentJSON += "\"foto\": \"" + a.getFoto() + "\", ";
               }
               contentJSON += "\"dataNascimento\": "
                     + a.getDataNascimento().getTime() + ", ";
               contentJSON += "\"sexo\": \""
                     + a.getSexo() + "\", ";
               if (a.getNomeMae() != null)
                  contentJSON += "\"nomeMae\": \"" + a.getNomeMae() + "\", ";
               if (a.getNomePai() != null)
                  contentJSON += "\"nomePai\": \"" + a.getNomePai() + "\", ";
               contentJSON += "\"periodoPS\": \"" + a.getPeriodoPS() + "\", ";
               contentJSON += "\"telefoneResidencial\": \""
                     + a.getTelefoneResidencial() + "\", ";
               contentJSON += "\"telefoneCelular\": \""
                     + a.getTelefoneCelular() + "\", ";
               contentJSON += "\"atendimentoPrioritario\": "
                     + a.isAtendimentoPrioritario();
               if (a.getDesligamento() != null) {
                  contentJSON += ", \"desligamento\": "
                        + a.getDesligamento().getTime();
               }
               contentJSON += " },\n";
            }
            contentJSON = contentJSON.substring(0, contentJSON.length() - 2);
         }
         contentJSON += "],\n";
         contentJSON += "\"currentTime\": "
               + System.currentTimeMillis() + "\n }";

      } else {
         contentJSON = "{ \"exception\": \"" + result.getMessage() + "\" }";
      }
      out.println(contentJSON);
   }

}
