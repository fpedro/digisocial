package web.viewhelper.implementation;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.HorarioDisponivel;
import domain.implementation.Orientador;
import web.viewhelper.ViewHelper;

/**
 *
 * @author Fábio Henrique
 */
public class OrientadorViewHelper implements ViewHelper {

  @Override
  public Entity getEntity(HttpServletRequest request) {
    int id;
    String idString;
    Orientador o = new Orientador();
    idString = request.getParameter("id");
    if (idString != null) {
      try {
        id = Integer.parseInt(idString);
//        o.setId(id);
      } catch (NumberFormatException e) {
        System.out.println("number format exception");
      }
    }
    return o;
  }

  @Override
  public void sendResponse(PSResult resultado, HttpServletResponse response) throws IOException, ServletException {
    PrintWriter out;
    String contentJSON;
    out = response.getWriter();
    contentJSON = "{ \"orientadores\" : [ \n";
    if (resultado.getMessage() == null) {
      for (Entity e : (List<Entity>)resultado.getEntities()) {
        Orientador o = (Orientador) e;
        contentJSON += "{ ";
//        contentJSON += "\"id\": " + o.getId() + ", ";
        contentJSON += "\"nome\": \"" + o.getNome() + "\", ";
        contentJSON += "\"horariosdisponiveis\": [ \n"; 
        for (HorarioDisponivel h: o.getHorariosDisponiveis()) {
          contentJSON += "{ ";
          contentJSON += "\"diadasemana\": \"" + h.getDia() + "\", ";
          contentJSON += "\"horainicio\": " + 
                           h.getHorarioInicio().getHora() + ", ";
          contentJSON += "\"minutoinicio\": " + 
                           h.getHorarioInicio().getMinuto() + ", ";
          contentJSON += "\"horatermino\": " + 
                           h.getHorarioTermino().getHora() + ", ";
          contentJSON += "\"minutotermino\": " + 
                           h.getHorarioTermino().getMinuto() + "";
          contentJSON += "},\n";
        }
        contentJSON = contentJSON.substring(0, contentJSON.length() - 2);
        contentJSON += "\n] },\n";
      }
      contentJSON = contentJSON.substring(0, contentJSON.length() - 2);
    }
    contentJSON += "  ]\n}";
    out.println(contentJSON);
  }
  
}
