package web.viewhelper.implementation;

import core.result.PSResult;
import domain.Entity;
import domain.implementation.Usuario;
import domain.implementation.SessaoPS;
import web.viewhelper.ViewHelper;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Fábio Henrique
 */
public class SessaoViewHelper implements ViewHelper {

   @Override
   public Entity getEntity(HttpServletRequest request) {
      SessaoPS sessao = new SessaoPS();
      Usuario u = (Usuario)request.getSession().getAttribute("activeUser");
      Usuario a = new Usuario();
      String s;
      s = request.getParameter("id");
      if (s != null)
         a.setId(Integer.parseInt(s));
      a.setNickName(request.getParameter("usuario"));
      s = request.getParameter("senha");    
      a.setSenha(s);
      if (u != null && request.getParameter("logout") != null)
         u.setActiveUser(false);
      sessao.setUserOnAction(u);
      sessao.setUserOnAuthentication(a);
      sessao.setSession(request.getSession());
      return sessao;
   }

   @Override
   public void sendResponse(PSResult result, HttpServletResponse response)
               throws IOException, ServletException {
      
      response.setContentType("application/json");
      response.setCharacterEncoding("UTF-8");

      String contentJSON;
      PrintWriter out = response.getWriter();

      if (result.isValid()) {
         SessaoPS s = (SessaoPS)result.getEntities().get(0);
         if (s.getUserOnAction() != null) {
            contentJSON = "{ \"userId\": " + s.getUserOnAction().getId() + ", ";
            contentJSON += "\"userName\": \"" + 
                             s.getUserOnAction().getNome() + "\", ";
            contentJSON += "\"nickName\": \"" +
                             s.getUserOnAction().getNickName()+ "\" }";
        }
        else contentJSON = "{ \"userId\": 0 }";
      } else {
         contentJSON = "{ \"exception\": \"" + result.getMessage() + "\" }";
      }
      out.println(contentJSON);
   }
}
/* vi: set sts=3 ts=3 sw=3 et: */
