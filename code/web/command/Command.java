package web.command;

import core.result.PSResult;
import domain.Entity;

/**
 *
 * @author Fábio Henrique
 */
public interface Command {
	public PSResult execute(Entity entidade);
}
