package web.command.implementation;

import core.facade.implementation.PSFacade;
import core.result.PSResult;
import domain.Entity;
import web.command.Command;


/**
 *
 * @author Fábio Henrique
 */
public class ModifyCommand implements Command {
   
  @Override
  public PSResult execute(Entity entity) {
    return PSFacade.getInstance().modify(entity);
  }
  
}
