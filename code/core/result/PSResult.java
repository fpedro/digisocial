package core.result;

import domain.Entity;

import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class PSResult {
    
   private List<Entity> entities;
   private String message;  

   public boolean isValid() {
      return getMessage() == null;
   }

   public List<Entity> getEntities() {
      return entities;
   }

   public void setEntities(List<Entity> entities) {
      this.entities = entities;
   }

   public String getMessage() {
      return message;
   }

   public void setMessage(String message) {
      this.message = message;
   }

}
