package core.facade;

import core.result.PSResult;
import domain.Entity;

/**
 *
 * @author Fabio Henrique
 */
public interface Facade {
    
	public PSResult save(Entity entity);
  public PSResult modify(Entity entity);
  public PSResult view(Entity entity);

}
