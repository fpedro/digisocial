package core.facade.implementation;

import core.result.PSResult;
import core.rules.Strategy;
import core.dao.DAO;
import core.dao.implementation.AcolhidoDAO;
import core.dao.implementation.AcolhidoQueryDAO;
import core.dao.implementation.DadosDoMesDAO;
import core.dao.implementation.DadosDoPeriodoDAO;
import core.dao.implementation.LogDAO;
import core.dao.implementation.SessaoDAO;
import core.dao.implementation.UsuarioDAO;
import core.rules.implementation.AuthenticationRule;
import core.rules.implementation.AcolhidoFilledRule;
import core.rules.implementation.AcolhidoFillForModifyRule;
import core.rules.implementation.AcolhidoModifyRule;
import core.rules.implementation.AcolhidoReinsercaoRule;
import core.rules.implementation.AcolhidoRegistryUser;
import core.rules.implementation.LogoutRule;
import core.rules.implementation.PasswordDigestRule;
//import core.rules.CPFRule;
//import core.rules.HorarioDefinidoRule;
//import core.rules.HorarioDisponivelRule;
import domain.Entity;
import domain.implementation.AtividadeAgendada;
import domain.implementation.Acolhido;
import domain.implementation.AcolhidoQuery;
import domain.implementation.DadosDoMes;
import domain.implementation.DadosDoPeriodo;
import domain.implementation.LogPS;
import domain.implementation.SessaoPS;
import domain.implementation.Usuario;
//import domain.implementation.Orientador;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.sql.SQLException;

/**
 *
 * @author Fábio Henrique
 */
public class PSFacade {

   private final Map<Class, Map<String, List<Strategy>>> psRules;
   private final Map<Class, DAO> psDAOs;
   private static final PSFacade INSTANCE = new PSFacade();
   
   private PSFacade() {

      psRules = new HashMap<>();
      psDAOs = new HashMap<>();

      psDAOs.put(SessaoPS.class, new SessaoDAO());
      psDAOs.put(Usuario.class, new UsuarioDAO());
      psDAOs.put(Acolhido.class, new AcolhidoDAO());
      psDAOs.put(DadosDoMes.class, new DadosDoMesDAO());
      psDAOs.put(DadosDoPeriodo.class, new DadosDoPeriodoDAO());
      psDAOs.put(AcolhidoQuery.class, new AcolhidoQueryDAO());
      psDAOs.put(LogPS.class, new LogDAO());
      

//      psDAOs.put(Atividade.class, new AtividadeDAO());
//      psDAOs.put(Orientador.class, new OrientadorDAO());
//      psDAOs.put(AtividadeAgendada.class, new AtividadeAgendadaDAO());

      List acolhidoSaveRules;
      List acolhidoModifyRules;
      List sessaoModifyRules;

//      List usuarioRules;

      psRules.put(Usuario.class, new HashMap<>());
      psRules.put(SessaoPS.class, new HashMap<>());
      psRules.put(Acolhido.class, new HashMap<>());

      psRules.get(SessaoPS.class).put("MODIFY", new ArrayList<>());
      psRules.get(Acolhido.class).put("SAVE", new ArrayList<>());
      psRules.get(Acolhido.class).put("MODIFY", new ArrayList<>());
      
      
      acolhidoSaveRules = psRules.get(Acolhido.class).get("SAVE");
      
      acolhidoSaveRules.add(new AcolhidoFilledRule());
      acolhidoSaveRules.add(new AcolhidoReinsercaoRule());
      acolhidoSaveRules.add(new AcolhidoRegistryUser());

      
//      acolhidoRules.add(new CPFRule());
      
      psRules.put(AtividadeAgendada.class, new HashMap<>());

      psRules.get(AtividadeAgendada.class).put("SAVE", new ArrayList<>());

//      List atividadeAgendadaRules;
//      atividadeAgendadaRules = psRules.get(AtividadeAgendada.class)
//                                  .get("SAVE");
         //    atividadeAgendadaRules.add(new HorarioDisponivelRule());
//   atividadeAgendadaRules.add(new HorarioDefinidoRule());

      
      sessaoModifyRules = psRules.get(SessaoPS.class).get("MODIFY");

      sessaoModifyRules.add(new PasswordDigestRule());    
      sessaoModifyRules.add(new AuthenticationRule());
      sessaoModifyRules.add(new LogoutRule());
      
      
      acolhidoModifyRules = psRules.get(Acolhido.class).get("MODIFY");
      
      acolhidoModifyRules.add(new AcolhidoFillForModifyRule());
      acolhidoModifyRules.add(new AcolhidoModifyRule());

   }
   
   public static PSFacade getInstance() {
      return INSTANCE;
   }

   public PSResult save(Entity entity) {
      PSResult result = new PSResult();
      result.setMessage(processRules(entity, "SAVE"));
      if (result.isValid()) {
         try {
           DAO dao = psDAOs.get(entity.getClass());
           dao.save(entity);
   		  List<Entity> entities = new ArrayList<>();
      	  entities.add(entity);
           result.setEntities(entities);
         } catch (SQLException e) {
           result.setMessage(e.getMessage());
         }
      }
      return result;
   }

   public PSResult view(Entity entity) {
      PSResult result = new PSResult();
      result.setMessage(processRules(entity, "VIEW"));
      if (result.isValid()) {
	      DAO dao = psDAOs.get(entity.getClass());
         try {
           result.setEntities(dao.get(entity));
         } catch (SQLException e) {
           result.setMessage(e.getMessage());
         }
      }
      return result;
   }

   public PSResult modify(Entity entity) {
      PSResult result = new PSResult();
      result.setMessage(processRules(entity, "MODIFY"));
      if (result.isValid()) {
   		DAO dao = psDAOs.get(entity.getClass());
      	try {
      		dao.update(entity);
      		List<Entity> entities = new ArrayList<>();
      		entities.add(entity);
      		result.setEntities(entities);
         } catch (SQLException e) {
           result.setMessage(e.getMessage());
         }
		}
      return result;
   }

   private String processRules(Entity entity, String operation) {
      Map<String, List<Strategy>> operationRules = psRules
                                                      .get(entity.getClass());
      String message;
      if (operationRules != null) {
         List<Strategy> rules = operationRules.get(operation);
         if (rules != null) {
           for (Strategy s : rules) {
             message = s.process(entity);
             if (message != null) 
               return message;
           }
         }
      }
      return null;
   }
   
}
