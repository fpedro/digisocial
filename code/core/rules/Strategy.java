package core.rules;

import domain.Entity;

/**
 *
 * @author Fábio Henrique
 */
public interface Strategy {
	public String process(Entity entity);    
}
