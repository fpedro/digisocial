package core.rules.implementation;

import java.sql.Timestamp;

import core.facade.implementation.PSFacade;
import domain.Entity;
import domain.implementation.Acolhido;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoReinsercaoRule implements core.rules.Strategy {

   @Override
   public String process(Entity entity) {
      Acolhido c = (Acolhido)entity;
      if (c.getId() != 0) {
         Acolhido auxiliar = (Acolhido)PSFacade.getInstance()
                              .view(c).getEntities().get(0);
         if (!auxiliar.estaSendoAtendido())
            return "Acolhido não está sendo atendida";
         c.setDesligamento(new Timestamp(System.currentTimeMillis()));
         c.setFirstRegistryId(auxiliar.getId());
         c.setFirstRegistryDate(auxiliar.getRegistry());
         c.setAcolhidoReinserido(true);
         PSFacade.getInstance().modify(c);
      }
      return null;
	}
	
}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
