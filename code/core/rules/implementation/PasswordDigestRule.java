package core.rules.implementation;

import domain.Entity;
import domain.implementation.SessaoPS;

import org.apache.commons.codec.digest.DigestUtils;

/**
 *
 * @author Fábio Henrique
 */
public class PasswordDigestRule implements core.rules.Strategy {

  @Override
  public String process(Entity entity) {
    SessaoPS s = (SessaoPS)entity;
    String senha = s.getUserOnAuthentication().getSenha();
    if (senha != null)
      s.getUserOnAuthentication().setSenha(DigestUtils.sha3_512Hex(senha));
    return null;
  }
  
}
