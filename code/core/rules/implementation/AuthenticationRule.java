package core.rules.implementation;

import core.facade.implementation.PSFacade;
import core.rules.Strategy;
import domain.Entity;
import domain.implementation.SessaoPS;
import domain.implementation.Usuario;

/**
 *
 * @author Fábio Henrique
 */
public class AuthenticationRule implements Strategy {

   @Override
   public String process(Entity entity) {
      SessaoPS s = (SessaoPS) entity;
      Usuario u = s.getUserOnAuthentication();
      if (u.getSenha() != null || u.getNickName() != null) {
         if (u.getSenha() == null) {
            return "Falta informar a senha";
         }
         if (PSFacade.getInstance().view(u).getEntities().isEmpty()) {
            return "Nome de usuário ou senha inválida";
         }
         u.setSenha(null);
         s.getSession().setAttribute("activeUser", u);
      }
      return null;
   }

}
