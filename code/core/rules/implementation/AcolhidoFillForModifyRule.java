package core.rules.implementation;

import core.facade.implementation.PSFacade;
import domain.Entity;
import domain.implementation.Acolhido;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoFillForModifyRule implements core.rules.Strategy {

   @Override
	public String process(Entity entity) {
      Acolhido a = (Acolhido)entity;
      Acolhido auxiliar = (Acolhido)PSFacade.getInstance().
                           view(a).getEntities().get(0);
      a.setRegistry(auxiliar.getRegistry());
      a.setNomeAcolhido(auxiliar.getNomeAcolhido());
      a.setCPFmae(auxiliar.getCPFmae());
      a.setCPFpai(auxiliar.getCPFpai());
      a.setUserOnRegistry(auxiliar.getUserOnRegistry());
      return null;
   }

}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8: */
