package core.rules.implementation;

import domain.Entity;
import domain.implementation.Acolhido;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoRegistryUser implements core.rules.Strategy {

	@Override
	public String process(Entity entity) {
	   Acolhido c = (Acolhido)entity;
      c.setUserOnRegistry(c.getUserOnAction());
      return null;
}

}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8: */
