package core.rules.implementation;

import domain.Entity;
import domain.implementation.Acolhido;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoFilledRule implements core.rules.Strategy {

	@Override
	public String process(Entity entity) {
		Acolhido a = (Acolhido)entity;
      if (a.getNomeAcolhido().isBlank() ||
          a.getDataNascimento() == null ||
          a.getSexo() == null ||
          a.getEndereco().getRua().isBlank() ||
          a.getEndereco().getNumero() == 0 ||
          a.getEndereco().getBairro().isBlank() ||
          a.getEndereco().getCidade().isBlank() ||
          a.getPeriodoPS() == null ||
          a.getEscola().getNome().isBlank() ||
          a.getEscola().getPeriodo() == null ||
          a.getEscola().getAno() == 0 ||
          a.getGostaDeFazer().isBlank() ||
          a.getRelacionamentoComFamilia().isBlank())
         return "Falta preencher campo";
      if (a.getNomeMae().isBlank()) a.setNomeMae(null);
      if (a.getNomePai().isBlank()) a.setNomePai(null);
      if (a.getNomeMae() == null && a.getNomePai() == null)
         return "Falta preencher dados do responsável";
      if (a.getNomeMae() != null) {
         if (a.getRGmae().isBlank() || a.getCPFmae() == null ||
             a.getLocalTrabalhoMae().isBlank())
           return "Falta preencher dados da mãe";
      }
      else {
         if (a.getNomePai() == null || a.getRGpai().isBlank() ||
             a.getCPFpai() == null || a.getLocalTrabalhoPai().isBlank())
           return "Falta preencher dados do pai";
      }
      if (a.getComQuemVoltaParaCasa() != null) {
         if (a.getComQuemVoltaParaCasa().isBlank())
           return "Falta preencher com quem volta para casa";
      }
      if (a.getTelefoneResidencial().isBlank() &&
             a.getTelefoneCelular().isBlank())
         return "Falta preencher o telefone de contato";
      if (a.getDeficiencia() != null) {
         if (a.getDeficiencia().isBlank())
           return "Falta descrever a deficiência";
      }
      if (a.getAlergia() != null) {
         if (a.getAlergia().isBlank())
           return "Falta descrever a alergia";
      }
      if (a.getMedicamento() != null) {
         if (a.getMedicamento().isBlank())
           return "Falta descrever o medicamento";
      }
      return null;
	}
	
}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
