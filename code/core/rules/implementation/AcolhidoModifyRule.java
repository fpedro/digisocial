package core.rules.implementation;

import core.facade.implementation.PSFacade;
import domain.Entity;
import domain.implementation.Acolhido;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoModifyRule implements core.rules.Strategy {

   @Override
   public String process(Entity entity) {
      Acolhido a = (Acolhido)entity;
      Acolhido auxiliar = (Acolhido)PSFacade.getInstance()
                           .view(a).getEntities().get(0);
      if (a.estaSendoAtendido()) {
         if (a.getEndereco().getRua().isBlank() ||
             a.getEndereco().getNumero() == 0 ||
             a.getEndereco().getBairro().isBlank() ||
             a.getEndereco().getCidade().isBlank() ||
             a.getPeriodoPS() == null ||
             a.getEscola().getNome().isBlank() ||
             a.getEscola().getPeriodo() == null ||
             a.getEscola().getAno() == 0)
            return "Falta preencher campo";
         if (auxiliar.getCPFmae() != null && a.getLocalTrabalhoMae().isBlank() ||
             auxiliar.getCPFpai() != null && a.getLocalTrabalhoPai().isBlank()) 
            return "Falta preencher local de trabalho do responsável";
         if (a.getComQuemVoltaParaCasa() != null) {
            if (a.getComQuemVoltaParaCasa().isBlank())
               return "Falta preencher com quem volta para casa";
         }
         if (a.getTelefoneResidencial().isBlank() &&
             a.getTelefoneCelular().isBlank())
            return "Falta preencher o telefone de contato";
         if (a.getDeficiencia() != null) {
            if (a.getDeficiencia().isBlank())
               return "Falta descrever a deficiência";
         }
         if (a.getAlergia() != null) {
            if (a.getAlergia().isBlank())
               return "Falta descrever a alergia";
         }
         if (a.getMedicamento() != null) {
            if (a.getMedicamento().isBlank())
               return "Falta descrever o medicamento";
         }
      }
      return null;
   }
	
}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
