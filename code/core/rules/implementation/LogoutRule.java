package core.rules.implementation;

import core.rules.Strategy;
import domain.Entity;
import domain.implementation.SessaoPS;
import domain.implementation.Usuario;

/**
 *
 * @author Fábio Henrique
 */
public class LogoutRule implements Strategy {

   @Override
   public String process(Entity entity) {
      SessaoPS s = (SessaoPS) entity;
      Usuario u = s.getUserOnAction();
      if (u != null && !u.isActiveUser())
         s.getSession().setAttribute("activeUser", null);
      return null;
   }

}
