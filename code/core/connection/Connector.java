package core.connection;

import java.sql.Connection;
import java.sql.SQLException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

/**
 *
 * @author Fábio Henrique
 */
public class Connector {

  public static Connection getConnection() throws SQLException {
    Connection connection = null;
    try {
      Context ctx = new InitialContext();
      if (ctx == null) {
        throw new SQLException("No Context");
      }
      DataSource ds = (DataSource) ctx.lookup("java:comp/env/jdbc/postgres");
      if (ds != null)
        connection = ds.getConnection();
    } catch (SQLException | NamingException e) {
      throw new SQLException("No connection with database");
    }
    return connection;
  }

    
}
