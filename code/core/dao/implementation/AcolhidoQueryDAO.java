package core.dao.implementation;

import domain.Entity;
import domain.implementation.Acolhido;
import domain.implementation.PeriodoPS;
import domain.implementation.Sexo;
import core.connection.Connector;
import domain.implementation.AcolhidoQuery;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.sql.PreparedStatement;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoQueryDAO implements core.dao.DAO {

   @Override
   public void save(Entity entity) throws SQLException {
   }

   @Override
   public List get(Entity entity) throws SQLException {
      Acolhido c = ((AcolhidoQuery)entity).getParameters();
      List entities = new ArrayList<Acolhido>();
      Connection connection = Connector.getConnection();
      try {
         if (connection != null) {
            connection.beginRequest();
            PreparedStatement preparedStatement;
            ResultSet rst;
            if (c.getNomeAcolhido() != null) {
              preparedStatement = connection.prepareStatement(
                "SELECT id, registry, nome_acolhido, foto_acolhido, " +
                  "data_nascimento, sexo, nome_mae, nome_pai, " +
                  "telefone_residencial, telefone_celular, " +
                  "periodo_ps, atendimento_prioritario, " +
                  "desligamento FROM acolhido " +
                    "WHERE nome_acolhido ILIKE ? " +
                    "ORDER BY nome_acolhido " +
                    "LIMIT 110");
              preparedStatement.setString(1, c.getNomeAcolhido() + '%');
              rst = preparedStatement.executeQuery();
              while (rst.next()) {
                Acolhido a = new Acolhido();
                a.setId(rst.getInt(1));
                a.setRegistry(rst.getTimestamp(2));
                a.setNomeAcolhido(rst.getString(3));
                a.setFoto(rst.getString(4));
                a.setDataNascimento(rst.getTimestamp(5));
                a.setSexo(Sexo.valueOf(rst.getString(6)));
                a.setNomeMae(rst.getString(7));
                a.setNomePai(rst.getString(8));
                a.setTelefoneResidencial(rst.getString(9));
                a.setTelefoneCelular(rst.getString(10));
                a.setPeriodoPS(PeriodoPS.valueOf(rst.getString(11)));
                a.setAtendimentoPrioritario(rst.getBoolean(12));
                a.setDesligamento(rst.getTimestamp(13));
                entities.add(a);
              }
            }
            else if (c.getRGmae() != null || c.getRGpai() != null) {
              preparedStatement = connection.prepareStatement(
                "SELECT id, registry, nome_acolhido, foto_acolhido, " +
                  "data_nascimento, sexo, " +
                  "nome_mae, nome_pai, telefone_residencial, telefone_celular, " +
                  "periodo_ps, atendimento_prioritario, " +
                  "desligamento FROM acolhido " +
                    "WHERE RG_mae LIKE ? OR RG_pai LIKE ? " +
                    "ORDER BY nome_acolhido " +
                    "LIMIT 110");
              preparedStatement.setString(1, c.getRGmae() + '%');
              preparedStatement.setString(2, c.getRGpai() + '%');
              rst = preparedStatement.executeQuery();
              while (rst.next()) {
                Acolhido a = new Acolhido();
                a.setId(rst.getInt(1));
                a.setRegistry(rst.getTimestamp(2));
                a.setNomeAcolhido(rst.getString(3));
                a.setFoto(rst.getString(4));
                a.setDataNascimento(rst.getTimestamp(5));
                a.setSexo(Sexo.valueOf(rst.getString(6)));
                a.setNomeMae(rst.getString(7));
                a.setNomePai(rst.getString(8));
                a.setTelefoneResidencial(rst.getString(9));
                a.setTelefoneCelular(rst.getString(10));
                a.setPeriodoPS(PeriodoPS.valueOf(rst.getString(11)));
                a.setAtendimentoPrioritario(rst.getBoolean(12));
                a.setDesligamento(rst.getTimestamp(13));
                entities.add(a);
              }
            }
            else if (c.getRegistry() != null &&
                     c.getRegistry().getTime() == -1) {
              GregorianCalendar calendarAuxiliar = new GregorianCalendar();
              int anoAtual = calendarAuxiliar.get(Calendar.YEAR);
              calendarAuxiliar.clear();
              calendarAuxiliar.set(Calendar.YEAR, anoAtual);
              Date inicioDoAno = calendarAuxiliar.getTime();
              preparedStatement = connection.prepareStatement(
                "SELECT id, registry, nome_acolhido, foto_acolhido, " +
                  "data_nascimento, sexo, " +
                  "nome_mae, nome_pai, telefone_residencial, telefone_celular, " +
                  "periodo_ps, atendimento_prioritario, " +
                  "desligamento FROM acolhido " +
                    "WHERE registry < ? " +
		                "AND desligamento IS NULL " +
                    "ORDER BY nome_acolhido " +
                    "LIMIT 110");
              preparedStatement.
                setTimestamp(1, new Timestamp(
                                  inicioDoAno.getTime()));
              rst = preparedStatement.executeQuery();
              while (rst.next()) {
                Acolhido a = new Acolhido();
                a.setId(rst.getInt(1));
                a.setRegistry(rst.getTimestamp(2));
                a.setNomeAcolhido(rst.getString(3));
                a.setFoto(rst.getString(4));
                a.setDataNascimento(rst.getTimestamp(5));
                a.setSexo(Sexo.valueOf(rst.getString(6)));
                a.setNomeMae(rst.getString(7));
                a.setNomePai(rst.getString(8));
                a.setTelefoneResidencial(rst.getString(9));
                a.setTelefoneCelular(rst.getString(10));
                a.setPeriodoPS(PeriodoPS.valueOf(rst.getString(11)));
                a.setAtendimentoPrioritario(rst.getBoolean(12));
                a.setDesligamento(rst.getTimestamp(13));
                entities.add(a);
              }
            }
            else {
              preparedStatement = connection.prepareStatement(
                "SELECT id, registry, nome_acolhido, foto_acolhido, " +
                  "data_nascimento, sexo, " +
                  "nome_mae, nome_pai, telefone_residencial, telefone_celular, " +
                  "periodo_ps, atendimento_prioritario FROM acolhido " +
                    "WHERE desligamento IS NULL " +
                    "ORDER BY nome_acolhido " +
                    "LIMIT 110");
              rst = preparedStatement.executeQuery();
              while (rst.next()) {
                Acolhido a = new Acolhido();
                a.setId(rst.getInt(1));
                a.setRegistry(rst.getTimestamp(2));
                a.setNomeAcolhido(rst.getString(3));
                a.setFoto(rst.getString(4));
                a.setDataNascimento(rst.getTimestamp(5));
                a.setSexo(Sexo.valueOf(rst.getString(6)));
                a.setNomeMae(rst.getString(7));
                a.setNomePai(rst.getString(8));
                a.setTelefoneResidencial(rst.getString(9));
                a.setTelefoneCelular(rst.getString(10));
                a.setPeriodoPS(PeriodoPS.valueOf(rst.getString(11)));
                a.setAtendimentoPrioritario(rst.getBoolean(12));
                entities.add(a);
              }
            }
            connection.close();
         }
      } catch (SQLException e) {
           throw new SQLException("Não foi possível realizar consulta");
      } finally {
           if (connection != null)
              connection.close();
      }
      return entities;
   }

   @Override
   public void update(Entity entity) throws SQLException {
   }

}
/* vi: set sts=3 ts=3 sw=3 et: */
