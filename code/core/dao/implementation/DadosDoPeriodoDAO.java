package core.dao.implementation;

import domain.Entity;
import domain.implementation.DadosDoMes;
import domain.implementation.DadosDoPeriodo;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Fábio Henrique
 */
public class DadosDoPeriodoDAO implements core.dao.DAO {

  @Override
  public void save(Entity entity) throws SQLException {
  }

  @Override
  public List get(Entity entity) throws SQLException {
    List entities = new ArrayList();
    DadosDoMesDAO dao = new DadosDoMesDAO();
    DadosDoPeriodo d = (DadosDoPeriodo) entity;
    for (DadosDoMes i: d.getDadosDosMeses()) 
      dao.get(i);
    entities.add(d);
    return entities;
  }

  @Override
  public void update(Entity entity) throws SQLException {
  }

}
