package core.dao.implementation;

import core.connection.Connector;
import domain.Entity;
import domain.implementation.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;

/**
 *
 * @author Fábio Henrique
 */
public class UsuarioDAO implements core.dao.DAO {

  @Override
  public void save(Entity entity) throws SQLException {
    Usuario u = (Usuario)entity;
    Connection connection = Connector.getConnection();
    try {
      if (connection != null) {
        connection.beginRequest();
        connection.commit();
        connection.close();
      }
    } catch (SQLException e) {
        throw new SQLException("Não foi possível salvar novo usuário");
    } finally {
        if (connection != null) {
          if (!connection.isClosed())
            connection.rollback();
          connection.close();    
        }
    }
  }

  @Override
  public List get(Entity entity) throws SQLException {
    Usuario u = (Usuario) entity;
    List entities = new ArrayList<Usuario>();
    Connection connection = Connector.getConnection();
    try {
      if (connection != null) {
        connection.beginRequest();
        PreparedStatement preparedStatement;
        ResultSet rst;
        if (u.getSenha() != null) {
          preparedStatement = connection.prepareStatement(
            "SELECT id, user_name, nickname FROM users " +
              "WHERE password = ? AND nickname = ?");
          preparedStatement.setString(1, u.getSenha());
          preparedStatement.setString(2, u.getNickName());
          rst = preparedStatement.executeQuery();
          if (rst.next()) {
            u.setId(rst.getInt(1));
            u.setNome(rst.getString(2));
            u.setNickName(rst.getString(3));
            entities.add(u);
          }
        }
        else if (u.getId() != 0) {
          preparedStatement = connection.prepareStatement(
            "SELECT id, user_name, nickName FROM users " +
              "WHERE id = ?");
          preparedStatement.setInt(1, u.getId());
          rst = preparedStatement.executeQuery();
          if (rst.next()) {
            u.setNome(rst.getString(2));
            u.setNickName(rst.getString(3));
            entities.add(u);
          }
        }
        else {
          preparedStatement = connection.prepareStatement(
            "SELECT id, user_name FROM users");
          rst = preparedStatement.executeQuery();
          while (rst.next()) {
            u = new Usuario();
            u.setId(rst.getInt(1));
            u.setNome(rst.getString(2));
            entities.add(u);
          }
        }
        connection.close();
      }
    } catch (SQLException e) {
        throw new SQLException("Não foi possível realizar consulta");
    } finally {
        if (connection != null)
          connection.close();
    }
    return entities;
  }

  @Override
  public void update(Entity entity) throws SQLException {
    Usuario u = (Usuario) entity;
    Connection connection = Connector.getConnection();
    try {
      if (connection != null) {
        connection.beginRequest();
        connection.commit();
        connection.close();
      }
    } catch (SQLException e) {
        throw new SQLException("Não foi possível concluir a alteração");
    } finally {
        if (connection != null) {
          if (!connection.isClosed())
            connection.rollback();
          connection.close();
        }
    }
  }

}
