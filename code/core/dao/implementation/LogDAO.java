package core.dao.implementation;

import core.connection.Connector;
import domain.Entity;
import domain.implementation.LogPS;
import domain.implementation.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import java.util.List;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Fábio Henrique
 */
public class LogDAO implements core.dao.DAO {

  @Override
  public void save(Entity entity) throws SQLException {
  }

  @Override
  public List get(Entity entity) throws SQLException {
    LogPS l = (LogPS)entity;
    List entities = new ArrayList<LogPS>();
    Connection connection = Connector.getConnection();
    try {
      if (connection != null) {
        connection.beginRequest();
        PreparedStatement preparedStatement;
        ResultSet rst;
        preparedStatement = connection.prepareStatement(
          "SELECT logs.id, logs.operation_date, logs.user_id, " +
                    "users.user_name, logs.operation_description " +
            "FROM logs JOIN users ON (logs.user_id = users.id) " +
            "LIMIT 110");
        rst = preparedStatement.executeQuery();
        while (rst.next()) {
          LogPS a = new LogPS();
          Usuario u = new Usuario();
          a.setId(rst.getInt(1));
          a.setData(rst.getTimestamp(2));
          u.setId(rst.getInt(3));
          u.setNome(rst.getString(4));
          a.setUser(u);
          a.setOperationDescription(rst.getString(5));
          entities.add(a);
        }
      }
    } catch (SQLException e) {
        throw new SQLException("Não foi possível realizar consulta");
    } finally {
        if (connection != null)
          connection.close();
    }
    return entities;
  }

  @Override
  public void update(Entity entity) throws SQLException {
  }

  public static void logOperation(LogPS log, Connection c) throws SQLException {
    ResultSet rst = c.createStatement()
                        .executeQuery("SELECT nextval('logs_id_seq')");
    rst.next();
    log.setId(rst.getInt(1));
    PreparedStatement preparedStatement = c.prepareStatement(
      "INSERT INTO logs (id, operation_date, user_id, operation_description) " +
                          "VALUES (?, ?, ?, ?)");
    preparedStatement.setInt(1, log.getId());
    preparedStatement.setTimestamp(2,
                        new Timestamp(System.currentTimeMillis()));
    preparedStatement.setInt(3, log.getUser().getId());
    preparedStatement.setString(4, log.getOperationDescription());
    preparedStatement.execute();
  }
  
}
