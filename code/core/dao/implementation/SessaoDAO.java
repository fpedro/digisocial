package core.dao.implementation;

import core.connection.Connector;
import domain.Entity;
import domain.implementation.SessaoPS;
import domain.implementation.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

/**
 *
 * @author Fábio Henrique
 */
public class SessaoDAO implements core.dao.DAO {

  @Override
  public void save(Entity entity) throws SQLException {
    SessaoPS s = (SessaoPS) entity;
    Connection connection = Connector.getConnection();
    try {
      if (connection != null) {
        connection.beginRequest();
        ResultSet rst = connection.createStatement()
          .executeQuery("SELECT nextval('sessao_id_seq')");
        rst.next();
        s.setId(rst.getInt(1));
        PreparedStatement preparedStatement = connection.prepareStatement(
          "INSERT INTO sessao (" +
          "id, registry)" +
          "VALUES (?, ?)");
        preparedStatement.setInt(1, s.getId());
        preparedStatement.setTimestamp(2,
                            new Timestamp(System.currentTimeMillis()));
//        preparedStatement.execute();
//        connection.commit();
        connection.close();
      }
    } catch (SQLException e) {
        throw new SQLException("Não foi possível salvar a sessão");
    } finally {
        if (connection != null) {
          if (!connection.isClosed())
            connection.rollback();
          connection.close();
        }
    }
  }

  @Override
  public List get(Entity entity) throws SQLException {
    List entities = new ArrayList<SessaoPS>();
    entities.add(entity);
    return entities;
  }

  @Override
  public void update(Entity entity) throws SQLException {
  }

}
