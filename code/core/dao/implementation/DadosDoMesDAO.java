package core.dao.implementation;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import core.connection.Connector;
import domain.Entity;
import domain.implementation.Acolhido;
import domain.implementation.DadosDoMes;
import domain.implementation.Endereco;
import domain.implementation.Escola;
import domain.implementation.PeriodoPS;
import domain.implementation.Sexo;

/**
 *
 * @author Fábio Henrique
 */
public class DadosDoMesDAO implements core.dao.DAO {

   @Override
   public void save(Entity entity) throws SQLException {
   }

   @Override
   public List get(Entity entity) throws SQLException {
      DadosDoMes d = (DadosDoMes) entity;
      List entities = new ArrayList();
      List<Acolhido> acolhidos = new ArrayList<>();
      Connection connection = Connector.getConnection();
      try {
         if (connection != null) {
            connection.beginRequest();
            PreparedStatement stmt = connection.prepareStatement(
               "SELECT id, registry, nome_acolhido, data_nascimento, " +
                         "sexo, CPF_mae, CPF_pai, " +
                         "endereco_bairro, " +
                         "periodo_ps, escola_nome, desligamento, " +
                         "first_registry_id, first_registry_date " +
                  "FROM acolhido " +
                  "WHERE now() >= ? AND registry < ? " +
                     "AND (desligamento >= ? OR desligamento IS NULL) " +
                        "AND acolhido_reinserido = FALSE");
            stmt.setTimestamp(1, new Timestamp(
                                       d.getInicioDoMes().getTime()));
            stmt.setTimestamp(2, new Timestamp(
                                       d.getInicioDoProximoMes().
                                         getTime()));
            stmt.setTimestamp(3, new Timestamp(
                                       d.getInicioDoMes().getTime()));
            ResultSet rst = stmt.executeQuery();
            d.setacolhidos(new ArrayList<>());
            while (rst.next()) {
               Acolhido n = new Acolhido();
               n.setId(rst.getInt(1));
               n.setRegistry(rst.getTimestamp(2));
               n.setNomeAcolhido(rst.getString(3));
               n.setDataNascimento(rst.getDate(4));
               n.setSexo(Sexo.valueOf(rst.getString(5)));
               n.setCPFmae(rst.getString(6));
               n.setCPFpai(rst.getString(7));
               n.setEndereco(new Endereco());
               n.getEndereco().setBairro(rst.getString(8));
               n.setPeriodoPS(PeriodoPS.valueOf(rst.getString(9)));
               n.setEscola(new Escola());
               n.getEscola().setNome(rst.getString(10));
               n.setDesligamento(rst.getTimestamp(11));
               n.setFirstRegistryId(rst.getInt(12));
               n.setFirstRegistryDate(rst.getTimestamp(13));
               acolhidos.add(n);
            }
            d.setacolhidos(acolhidos);
            entities.add(d);
            connection.close();
         }
      } catch (SQLException e) {
            throw new SQLException("Não foi realizar consulta");
      } finally {
            if (connection != null)
               connection.close();
      }
      return entities;
   }

   @Override
   public void update(Entity entity) throws SQLException {
   }

}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
