package core.dao.implementation;

import core.connection.Connector;
import domain.Entity;
import domain.implementation.Acolhido;
import domain.implementation.Endereco;
import domain.implementation.Familiar;
import domain.implementation.PeriodoPS;
import domain.implementation.Sexo;
import domain.implementation.Escola;
import domain.implementation.LogPS;
import domain.implementation.PeriodoEscolar;
import domain.implementation.Usuario;

import java.util.ArrayList;
import java.util.List;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.Timestamp;

/**
 *
 * @author Fábio Henrique
 */
public class AcolhidoDAO implements core.dao.DAO {

   @Override
   public void save(Entity entity) throws SQLException {
      Acolhido a = (Acolhido) entity;
      Connection connection = Connector.getConnection();
      try {
         if (connection != null) {
            connection.beginRequest();
            ResultSet rst = connection.createStatement()
                  .executeQuery("SELECT nextval('acolhido_id_seq')");
            rst.next();
            a.setId(rst.getInt(1));
            a.setRegistry(new Timestamp(System.currentTimeMillis()));
            if (a.getFirstRegistryId() == 0) {
               a.setFirstRegistryId(a.getId());
               a.setFirstRegistryDate(a.getRegistry());
            }
            PreparedStatement preparedStatement = connection.prepareStatement(
              "INSERT INTO acolhido (" +
                "id, registry, nome_acolhido, " +
                "foto_acolhido, data_nascimento, sexo, " +
                "nome_mae, RG_mae, CPF_mae, local_trabalho_mae, " +
                "nome_pai, RG_pai, CPF_pai, local_trabalho_pai, " +
                "endereco_rua, endereco_numero, endereco_complemento, " +
                "endereco_bairro, endereco_cidade, " +
                "com_quem_volta_para_casa, " +
                "telefone_residencial, telefone_celular, " +
                "periodo_PS, quantas_vezes_no_projeto, " +
                "atendimento_prioritario, " +
                "escola_nome, escola_periodo, escola_ano, " +
                "deficiencia, alergia, medicamento, " +
                "bolsa_familia, doenca_grave, " +
                "tratamento_neurologista, tratamento_psiquiatra, " +
                "tratamento_psicologo, tratamento_psicopedagogo, " +
                "tratamento_fonoaudiologo, tratamento_outros, " +
                "problema_na_familia, gosta_de_fazer, " +
                "relacionamento_com_familia, " +
                "user_on_registry, " +
                "first_registry_id, first_registry_date) " +
                  "VALUES (?, ?, ?, " +
                    "?, ?, SEXO(?), " +
                    "?, ?, ?, ?, " +
                    "?, ?, ?, ?, " +
                    "?, ?, ?, " +
                    "?, ?, ?,  " +
                    "?, ?, " +
                    "PERIODO_PS(?), ?, ?, " +
                    "?, PERIODO_ESCOLAR(?), ?, " +
                    "?, ?, ?, " +
                    "?, ?, " +
                    "?, ?, " +
                    "?, ?, " +
                    "?, ?, " +
                    "?, ?, ?, " +
                    "?, " +
                    "?, ?)");
            preparedStatement.setInt(1, a.getId());
            preparedStatement.setTimestamp(2, (Timestamp) a.getRegistry());
            preparedStatement.setString(3, a.getNomeAcolhido());
            preparedStatement.setString(4, a.getFoto());
            preparedStatement.setTimestamp(5,
                  new Timestamp(a.getDataNascimento().getTime()));
            preparedStatement.setString(6, a.getSexo().name());
            preparedStatement.setString(7, a.getNomeMae());
            preparedStatement.setString(8, a.getRGmae());
            preparedStatement.setString(9, a.getCPFmae());
            preparedStatement.setString(10, a.getLocalTrabalhoMae());
            preparedStatement.setString(11, a.getNomePai());
            preparedStatement.setString(12, a.getRGpai());
            preparedStatement.setString(13, a.getCPFpai());
            preparedStatement.setString(14, a.getLocalTrabalhoPai());
            preparedStatement.setString(15, a.getEndereco().getRua());
            preparedStatement.setInt(16, a.getEndereco().getNumero());
            preparedStatement.setString(17, a.getEndereco().getComplemento());
            preparedStatement.setString(18, a.getEndereco().getBairro());
            preparedStatement.setString(19, a.getEndereco().getCidade());
            preparedStatement.setString(20, a.getComQuemVoltaParaCasa());
            preparedStatement.setString(21, a.getTelefoneResidencial());
            preparedStatement.setString(22, a.getTelefoneCelular());
            preparedStatement.setString(23, a.getPeriodoPS().name());
            preparedStatement.setInt(24, a.getQuantasVezesNoProjeto());
            preparedStatement.setBoolean(25, a.isAtendimentoPrioritario());
            preparedStatement.setString(26, a.getEscola().getNomeAbreviado());
            preparedStatement.setString(27, a.getEscola().getPeriodo().name());
            preparedStatement.setInt(28, a.getEscola().getAno());
            preparedStatement.setString(29, a.getDeficiencia());
            preparedStatement.setString(30, a.getAlergia());
            preparedStatement.setString(31, a.getMedicamento());
            preparedStatement.setBoolean(32, a.isBolsaFamilia());
            preparedStatement.setString(33, a.getDoencaGrave());
            preparedStatement.setString(34, a.getTratamentoNeurologista());
            preparedStatement.setString(35, a.getTratamentoPsiquiatra());
            preparedStatement.setString(36, a.getTratamentoPsicologo());
            preparedStatement.setString(37, a.getTratamentoPsicopedagogo());
            preparedStatement.setString(38, a.getTratamentoFonoaudiologo());
            preparedStatement.setString(39, a.getTratamentoOutros());
            preparedStatement.setString(40, a.getProblemaNaFamilia());
            preparedStatement.setString(41, a.getGostaDeFazer());
            preparedStatement.setString(42, a.getRelacionamentoComFamilia());
            preparedStatement.setInt(43, a.getUserOnRegistry().getId());
            preparedStatement.setInt(44, a.getFirstRegistryId());
            preparedStatement.setTimestamp(45, (Timestamp)a.getFirstRegistryDate());
            preparedStatement.execute();
            for (Familiar f : a.getFamiliares()) {
               preparedStatement = connection.prepareStatement(
                     "INSERT INTO familiar (acolhido_id, "
                     + "nome, idade, parentesco, ocupacao) "
                     + "VALUES (?, ?, ?, ?, ?)");
               preparedStatement.setInt(1, a.getId());
               preparedStatement.setString(2, f.getNome());
               preparedStatement.setInt(3, f.getIdade());
               preparedStatement.setString(4, f.getParentesco());
               preparedStatement.setString(5, f.getOcupacao());
               preparedStatement.execute();
            }
            LogPS log = new LogPS();
            log.setUser(a.getUserOnAction());
            if (a.isAcolhidoReinserido())
               log.setOperationDescription("Reinseriu o acolhido "
                    + a.getNomeAcolhido() + ".");
            else
               log.setOperationDescription("Cadastrou o acolhido "
                    + a.getNomeAcolhido() + ".");
            LogDAO.logOperation(log, connection);
            connection.commit();
            connection.close();
         }
      } catch (SQLException e) {
         throw new SQLException("Não foi possível concluir o cadastro");
      } finally {
         if (connection != null) {
            if (!connection.isClosed()) {
               connection.rollback();
            }
            connection.close();
         }
      }
   }

   @Override
   public List get(Entity entity) throws SQLException {
      Acolhido c = (Acolhido) entity;
      List<Acolhido> entities = new ArrayList<>();
      Connection connection = Connector.getConnection();
      try {
         if (connection != null) {
            connection.beginRequest();
            PreparedStatement preparedStatement;
            ResultSet rst;
            if (c.getId() != 0) {
               preparedStatement = connection.prepareStatement(
                     "SELECT acolhido.id, registry, nome_acolhido, foto_acolhido, "
                     + "data_nascimento, sexo, "
                     + "nome_mae, RG_mae, CPF_mae, local_trabalho_mae, "
                     + "nome_pai, RG_pai, CPF_pai, local_trabalho_pai, "
                     + "endereco_rua, endereco_numero, endereco_complemento, "
                     + "endereco_bairro, endereco_cidade, com_quem_volta_para_casa, "
                     + "telefone_residencial, telefone_celular, "
                     + "periodo_ps, quantas_vezes_no_projeto, "
                     + "atendimento_prioritario, "
                     + "escola_nome, escola_periodo, escola_ano, "
                     + "deficiencia, alergia, medicamento, bolsa_familia, "
                     + "doenca_grave, tratamento_neurologista, "
                     + "tratamento_psiquiatra, "
                     + "tratamento_psicologo, tratamento_psicopedagogo, "
                     + "tratamento_fonoaudiologo, tratamento_outros, "
                     + "problema_na_familia, gosta_de_fazer, "
                     + "relacionamento_com_familia, desligamento, "
                     + "user_on_registry, users.nickname "
                     + "FROM acolhido "
                     + "INNER JOIN users ON acolhido.user_on_registry = users.id "
                     + "WHERE acolhido.id = ?");
               preparedStatement.setInt(1, c.getId());
               rst = preparedStatement.executeQuery();
               if (rst.next()) {
                  Acolhido a = new Acolhido();
                  a.setId(rst.getInt(1));
                  a.setRegistry(rst.getTimestamp(2));
                  a.setNomeAcolhido(rst.getString(3));
                  a.setFoto(rst.getString(4));
                  a.setDataNascimento(rst.getTimestamp(5));
                  a.setSexo(Sexo.valueOf(rst.getString(6)));
                  a.setNomeMae(rst.getString(7));
                  a.setRGmae(rst.getString(8));
                  a.setCPFmae(rst.getString(9));
                  a.setLocalTrabalhoMae(rst.getString(10));
                  a.setNomePai(rst.getString(11));
                  a.setRGpai(rst.getString(12));
                  a.setCPFpai(rst.getString(13));
                  a.setLocalTrabalhoPai(rst.getString(14));
                  a.setEndereco(new Endereco());
                  a.getEndereco().setRua(rst.getString(15));
                  a.getEndereco().setNumero(rst.getInt(16));
                  a.getEndereco().setComplemento(rst.getString(17));
                  a.getEndereco().setBairro(rst.getString(18));
                  a.getEndereco().setCidade(rst.getString(19));
                  a.setComQuemVoltaParaCasa(rst.getString(20));
                  a.setTelefoneResidencial(rst.getString(21));
                  a.setTelefoneCelular(rst.getString(22));
                  a.setPeriodoPS(PeriodoPS.valueOf(rst.getString(23)));
                  a.setQuantasVezesNoProjeto(rst.getByte(24));
                  a.setAtendimentoPrioritario(rst.getBoolean(25));
                  a.setEscola(new Escola());
                  a.getEscola().setNome(rst.getString(26));
                  a.getEscola().setPeriodo(PeriodoEscolar.valueOf(rst.getString(27)));
                  a.getEscola().setAno(rst.getByte(28));
                  a.setDeficiencia(rst.getString(29));
                  a.setAlergia(rst.getString(30));
                  a.setMedicamento(rst.getString(31));
                  a.setBolsaFamilia(rst.getBoolean(32));
                  a.setDoencaGrave(rst.getString(33));
                  a.setTratamentoNeurologista(rst.getString(34));
                  a.setTratamentoPsiquiatra(rst.getString(35));
                  a.setTratamentoPsicologo(rst.getString(36));
                  a.setTratamentoPsicopedagogo(rst.getString(37));
                  a.setTratamentoFonoaudiologo(rst.getString(38));
                  a.setTratamentoOutros(rst.getString(39));
                  a.setProblemaNaFamilia(rst.getString(40));
                  a.setGostaDeFazer(rst.getString(41));
                  a.setRelacionamentoComFamilia(rst.getString(42));
                  a.setDesligamento(rst.getTimestamp(43));
                  Usuario u = new Usuario();
                  u.setId(rst.getInt(44));
                  u.setNickName(rst.getString(45));
                  a.setUserOnRegistry(u);
                  a.setFamiliares(new ArrayList());
                  preparedStatement = connection.prepareStatement(
                        "SELECT nome, idade, parentesco, ocupacao "
                        + "FROM familiar "
                        + "WHERE acolhido_id = ?");
                  preparedStatement.setInt(1, c.getId());
                  rst = preparedStatement.executeQuery();
                  while (rst.next()) {
                     Familiar f = new Familiar();
                     f.setNome(rst.getString(1));
                     f.setIdade(rst.getByte(2));
                     f.setParentesco(rst.getString(3));
                     f.setOcupacao(rst.getString(4));
                     a.getFamiliares().add(f);
                  }
                  entities.add(a);
               }
            }
         }
      } catch (SQLException e) {
         throw new SQLException("Não foi possível realizar consulta");
      } finally {
         if (connection != null) {
            connection.close();
         }
      }
      return entities;
   }

   @Override
   public void update(Entity entity) throws SQLException {
      Acolhido a = (Acolhido) entity;
      Connection connection = Connector.getConnection();
      try {
         if (connection != null) {
            connection.beginRequest();
            if (a.isAcolhidoReinserido()) {
               PreparedStatement preparedStatement = connection.
                 prepareStatement(
                   "UPDATE acolhido " +
                     "SET desligamento = ? ," +
                     "acolhido_reinserido = TRUE " +
                     "WHERE id = ?");
               preparedStatement.setTimestamp(1, (Timestamp) a.getDesligamento());
               preparedStatement.setInt(2, a.getId());
               preparedStatement.executeUpdate();
            }
            else if (!a.estaSendoAtendido()) {
               PreparedStatement preparedStatement = connection.prepareStatement(
                     "UPDATE acolhido SET desligamento = ? "
                     + "WHERE id = ?");
               preparedStatement.setTimestamp(1, new Timestamp(a.getDesligamento().getTime()));
               preparedStatement.setInt(2, a.getId());
               preparedStatement.executeUpdate();
               LogPS log = new LogPS();
               log.setUser(a.getUserOnAction());
               log.setOperationDescription("Desligou "
                     + a.getNomeAcolhido() + ".");
               LogDAO.logOperation(log, connection);
            }
            else {
               PreparedStatement preparedStatement = connection.prepareStatement(
                  "UPDATE acolhido "
                  + "SET foto_acolhido = ?, "
                  + "local_trabalho_mae = ?, local_trabalho_pai = ? ,"
                  + "endereco_rua = ?, endereco_numero = ? ,"
                  + "endereco_complemento = ?, endereco_bairro = ? ,"
                  + "endereco_cidade = ?, com_quem_volta_para_casa = ? ,"
                  + "telefone_residencial = ?, telefone_celular = ? ,"
                  + "periodo_ps = PERIODO_PS(?), quantas_vezes_no_projeto = ?,"
                  + "atendimento_prioritario = ?, escola_nome = ?,"
                  + "escola_periodo = PERIODO_ESCOLAR(?), escola_ano = ?,"
                  + "deficiencia = ?, alergia = ?,"
                  + "medicamento = ?, bolsa_familia = ? "
                  + "WHERE id = ?");
            preparedStatement.setString(1, a.getFoto());
            preparedStatement.setString(2, a.getLocalTrabalhoMae());
            preparedStatement.setString(3, a.getLocalTrabalhoPai());
            preparedStatement.setString(4, a.getEndereco().getRua());
            preparedStatement.setInt(5, a.getEndereco().getNumero());
            preparedStatement.setString(6, a.getEndereco().getComplemento());
            preparedStatement.setString(7, a.getEndereco().getBairro());
            preparedStatement.setString(8, a.getEndereco().getCidade());
            preparedStatement.setString(9, a.getComQuemVoltaParaCasa());
            preparedStatement.setString(10, a.getTelefoneResidencial());
            preparedStatement.setString(11, a.getTelefoneCelular());
            preparedStatement.setString(12, a.getPeriodoPS().name());
            preparedStatement.setInt(13, a.getQuantasVezesNoProjeto());
            preparedStatement.setBoolean(14, a.isAtendimentoPrioritario());
            preparedStatement.setString(15, a.getEscola().getNomeAbreviado());
            preparedStatement.setString(16, a.getEscola().getPeriodo().name());
            preparedStatement.setInt(17, a.getEscola().getAno());
            preparedStatement.setString(18, a.getDeficiencia());
            preparedStatement.setString(19, a.getAlergia());
            preparedStatement.setString(20, a.getMedicamento());
            preparedStatement.setBoolean(21, a.isBolsaFamilia());
            preparedStatement.setInt(22, a.getId());
            preparedStatement.executeUpdate();
            LogPS log = new LogPS();
            log.setUser(a.getUserOnAction());
            log.setOperationDescription("Modificou dados de "
                  + a.getNomeAcolhido() + ".");
            LogDAO.logOperation(log, connection);
            }
            connection.commit();
            connection.close();
         }
      } catch (SQLException e) {
         throw new SQLException("Não foi possível concluir a alteração");
      } finally {
         if (connection != null) {
            if (!connection.isClosed()) {
               connection.rollback();
            }
            connection.close();
         }
      }
   }

}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8 ff=dos: */
