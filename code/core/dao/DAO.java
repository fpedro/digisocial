package core.dao;

import java.util.List;
import java.sql.SQLException;

import domain.Entity;

/**
 *
 * @author Fábio Henrique
 */
public interface DAO {
	
  public void save(Entity entity) throws SQLException;
	public List get(Entity entity) throws SQLException;
  public void update(Entity entity) throws SQLException;
	
}
