<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Autenticação</title>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css" />
    <script></script>
  </head>
  <body>
    <div id="flexContainer" style="height: 600px;" >
      <div style="margin: auto;" >
        <div style="display: inline-block; float: left;
                      width: 300px; height: 330px; padding: 0;" >
          <img src="logo-min.png" alt="Luminosa" />
          <div style="margin: 10px 0; padding: 1px 10px" >
            <p style="font-size: 16pt" >Bem vindo(a)!</p></div>
        </div>
        <div style="display: inline-block; background-color: white;
                      width: 300px; height: 330px;" >
          <form id="authentication_form" >
            <div id="message_div" style="margin: 0 0 0 8px; padding: 0" >
            </div>
            <div style="margin: 8px" >
              <input id="nomeUsuario_input" type="text" name="usuario"
                     style="margin: 0; width: 250px"/>
            </div>
            <div style="margin: 8px" >
              <input type="password" name="senha"
                     style="margin: 0; width: 250px"/>
            </div>
            <div style="float: right;" >
                <span id="info_span" class="infoMessage" ></span>
            </div>
            <div style="margin: 0; padding: 0; clear: both" ></div>
            <div style="margin: 0 8px 0 0; padding: 0; float: right;" >
              <input id="entrar_button" type="button" class="operationControl"
                     value="Entrar" onclick="entrar()"/>
            </div>
            <input type="hidden" name="operation" value="MODIFY" />
          </form>
          <div style="clear: both" ></div>
          <div style="margin: 0 8px 8px;" >
            <span class="optionsButton" >Esqueci a senha</span>
          </div>
          <div style="margin: 8px" >
            <span class="optionsButton" >Criar novo usuário</span>
          </div>
        </div>
      </div>
    </div>  
    <script>

  nomeUsuario_input.focus();

  document.onkeydown = getEnter;

  function getEnter(e) {
     if (e.code === 'Enter') {
       e.preventDefault();
       entrar();
     }
  }

  function entrar() {
     
     entrar_button.disabled = true;
     info_span.textContent = 'processando...';

     formData = new FormData(authentication_form);
    
     fetch('autenticacao.do', {
             method: 'POST',
             body: formData
     }).then(function (response) {
         if (!response.ok) {
            throw new Error("HTTP error, status = " + response.status);
         }
         return response.json();
      })
      .then(function (json) {
        if (json.exception === undefined)
           window.location = 'app';
        else {
           while (message_div.hasChildNodes()) {
               message_div.removeChild(message_div.firstChild);
            }
           message_span = document.createElement('span');
           message_span.className = 'exceptionMessage';
           message_span.textContent = json.exception;
           message_div.appendChild(message_span);
           setTimeout(clearMessage, 3000, message_span);
           setTimeout(removeMessage, 5000, message_span);
           info_span.textContent = '';
           entrar_button.disabled = false;          
        }
      })
      .catch(function (error) {
        console.log(error.message);
        console.log(error);
      });
  }

  function clearMessage(e) {
    window.requestAnimationFrame(function (time) {
      window.requestAnimationFrame(function (time) {
        e.style.opacity = 0; });
    });
  }

  function removeMessage(e) {
    e.style.display = 'none';
  }

    </script>  
  </body>
</html>
<!-- vi: set sts=3 ts=3 sw=3 et: -->
