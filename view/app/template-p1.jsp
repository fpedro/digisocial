<%@page contentType="text/html" pageEncoding="UTF-8"%>
<% String contextPath = request.getContextPath(); %>
    <script>
      const app_path = '<%=contextPath%>' + '/app';
    </script>
    <div id="flexContainer" >
      <div style="padding: 0; margin: 0" >
        <a id="logo_link" href="" >
          <img id="logo" alt="Luminosa"
               src="<%=contextPath%>/logo-min.png"/></a>
        <div id="menu_div" style="padding: 0; margin: 0; margin-top: 15px;" >
          <a style="text-decoration: none"
             href="<%=contextPath%>/app/acolhidos.jsp" >
            <div class="menuOption" ><span>ACOLHIDOS</span></div></a>
          <a style="display: none; text-decoration: none"
             href="<%=contextPath%>/app/orientadores.jsp" >
            <div class="menuOption" ><span>ORIENTADORES</span></div></a>
          <a style="display: none; text-decoration: none"
             href="<%=contextPath%>/app/atividades.jsp" >
            <div class="menuOption" ><span>ATIVIDADES</span></div></a>
          <a style="display: none; text-decoration: none"
             href="<%=contextPath%>/app/agenda.jsp" >
            <div class="menuOption" ><span>AGENDA</span></div></a>
          <a style="display: none; text-decoration: none"
             href="<%=contextPath%>/app/objetivos-resultados.jsp" >
            <div class="menuOption" style="height: 50px;" >
              <span>OBJETIVOS E<br />
                &nbsp;&nbsp;RESULTADOS
              </span></div></a>
          <a style="text-decoration: none"
             href="<%=contextPath%>/app/relatorios.jsp" >
            <div class="menuOption" ><span>RELATÓRIOS</span></div></a>
          <a style="display: none; text-decoration: none"
             href="<%=contextPath%>/app/analise.jsp" >
            <div class="menuOption" style="height: 50px;" >
              <span>ANÁLISE<br />
                &nbsp;&nbsp;DE DADOS
              </span></div></a>
        </div>
      </div>
      <div style="padding: 0; width: 100%" >
        <div style="margin: 0 0 10px;" >
          <input id="query_input" type="text" style="width: 565px"    
                 placeholder="Nome do acolhido ou documento do responsável"
                 oninput="query()"/>
          <a style="text-decoration: none"
             href="<%=contextPath%>/app/usuario.jsp" >
            <span id="userName_span" class="optionsButton"
                  style="margin-right: 5px; float: right" ></span></a>
          <span style="margin-right: 5px; float: right;
		       clear: both; font-size: 10pt" >
	    NOME DO PROJETO SOCIAL</span>
        </div>
        <div style="margin: 0; background-color: white;
                    padding: 0px; width: 100%; min-height: 800px" >
          <div id="content_pane" style="padding: 10px" >
