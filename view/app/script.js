logo_link.href = app_path;

fetch(app_path + '/sessao.do', {
        method: 'POST'
}).then(function (response) {
     if (!response.ok) {
        throw new Error("HTTP error, status = " + response.status);
     }
     return response.json();
   })
   .then(function (json) {
      userName_span.textContent = json.userName;
   })
   .catch(function (error) {
      console.log(error.message);
});

query_input.value = '';
query_input.focus();

function query() {
   clearResultPane();
   query_type = 'nome';
   if (query_input.value.length === 0) {
      content_pane.style.display = 'block';
      result_pane.style.display = 'none';
   }       
   else {
      result_pane.style.display = 'block';
      content_pane.style.display = 'none';
      for (i = 0; i < query_input.value.length; i++)
         if (Number.isInteger(parseInt(query_input.value[i]))) {
            query_type = 'documento';
            break;
         }
      switch (query_type) {
         case 'documento':
            fetch(app_path + '/acolhido-query.do?RGmae=' + query_input.value +
                         '&RGpai=' + query_input.value, {
                         method: 'POST'
             }).then(function (response) {
                   if (!response.ok) {
                         throw new Error("HTTP error, status = " + response.status);
                   }
                   return response.json();
                })
                .then(function (json) {
                   fillResultPane(json, 'Pesquisa por documento');
                })
                .catch(function (error) {
                   console.log(error.message);
                });
            break
         case 'menino_menina':
            // title "Apresentando meninos" ...
            break;
         case 'bairro':
            // title "Apresentando acolhidos que moram no bairro Freguesia" ...
            break;
         case 'idade':
            break;
         default:
            // pesquisa por nome
            fetch(app_path + '/acolhido-query.do?nomeAcolhido='
                        + query_input.value, {
                        method: 'POST'
             }).then(function (response) {
                   if (!response.ok) {
                         throw new Error("HTTP error, status = " + response.status);
                   }
                   return response.json();
                })
                .then(function (json) {
                   fillResultPane(json, 'Pesquisa por nome');
                })
                .catch(function (error) {
                   console.log(error.message);
                });
      }
   }
}

function validateNumber(input) {
   numericValue = "";
   for (i = 0; i < input.value.length; i++)
      if (Number.isInteger(parseInt(input.value[i])))
         numericValue += input.value[i];
   input.value = numericValue;
}

function acolhido_builder(id,
                       	 dataCadastro,
                         nomeAcolhido,
                         foto,
                         dataNascimento,
                         sexo,
                         nomeMae,
                         nomePai,                                     
                         telefoneResidencial,
                         telefoneCelular,
                         atendimentoPrioritario,
                         currentTime,
                         desligamento) {
   n = document.createElement('div');
   n.className = 'acolhidoFrame';
   nomeAcolhido_div = document.createElement('div');
   nomeAcolhido_div.className = 'acolhidoField';
   nomeAcolhido_div.textContent = nomeAcolhido;
   nomeResponsavel_div = document.createElement('div');
   nomeResponsavel_div.className = 'acolhidoField';
   if (nomeMae !== undefined)
      nomeResponsavel_div.textContent = '\u2192 ' + nomeMae;
   else
      nomeResponsavel_div.textContent = '\u2192 ' + nomePai;
   telefoneResidencial_div = document.createElement('div');
   telefoneResidencial_div.className = 'acolhidoField';
   telefoneResidencial_div.textContent = telefoneResidencial;
   telefoneCelular_div = document.createElement('div');
   telefoneCelular_div.className = 'acolhidoField';
   telefoneCelular_div.textContent = telefoneCelular;
   photo_img = document.createElement('img');
   if (foto !== undefined) photo_img.src = foto;
   else {
      if (sexo === 'FEMININO')
         photo_img.src = 'foto-menina-a.png';
      else if (sexo === 'MASCULINO')
         photo_img.src = 'foto-menino-a.png';
   }   
   d = new Date(currentTime.getTime());
   d.setFullYear(d.getFullYear() - 11);
   d = new Date(d.getFullYear(), d.getMonth(), d.getDate());
   if (desligamento !== undefined)
      n.style.borderColor = 'gainsboro';
   else if (new Date(dataCadastro)
              .getFullYear() < currentTime.getFullYear()) {
      n.style.borderStyle = 'dashed';
      n.style.borderColor = 'black';
   }
   else if (atendimentoPrioritario)
      n.style.borderColor = 'yellow';
   else if (dataNascimento <= d.getTime())
      n.style.borderColor = 'blue';
   n.appendChild(photo_img);
   n.appendChild(nomeAcolhido_div);
   n.appendChild(nomeResponsavel_div);
   n.appendChild(telefoneResidencial_div);
   n.appendChild(telefoneCelular_div);
   return n;
}

function acolhido_highLight(acolhido_div) {
   acolhido_div.style.borderStyle = 'double';
   fields = acolhido_div.querySelectorAll('.acolhidoField');
   for (k = 0; k < fields.length; k++)
      fields[k].style.color = 'blue';
}

function clearResultPane() {
   while (result_pane.hasChildNodes()) {
      result_pane.removeChild(result_pane.firstChild);
   }
}

function fillResultPane(json, title) {
   query_title = document.createElement('span');
   query_title.textContent = title;
   line_break = document.createElement('br');
   result_pane.appendChild(query_title);
   result_pane.appendChild(line_break);
   query_title = document.createElement('span');
   query_title.textContent = json.acolhidos.length;
   if (json.acolhidos.length >= 2)
      query_title.textContent += ' resultados';
   else if (json.acolhidos.length === 1)
      query_title.textContent += ' resultado';
   else
      query_title.textContent = 'nenhum resultado';
   line_break = document.createElement('br');
   result_pane.appendChild(query_title);
   result_pane.appendChild(line_break);
   currentTime = new Date(json.currentTime);
   for (i = 0; i < json.acolhidos.length; i++) {
      acolhido_div = acolhido_builder(json.acolhidos[i]
                                                   .id,
                                                   json.acolhidos[i]
                                                   .dataCadastro,
                                                   json.acolhidos[i]
                                                   .nomeAcolhido,
                                                   json.acolhidos[i]
                                                   .foto,
                                                   json.acolhidos[i]
                                                   .dataNascimento,
                                                   json.acolhidos[i]
                                                   .sexo,
                                                   json.acolhidos[i]
                                                   .nomeMae,
                                                   json.acolhidos[i]
                                                   .nomePai,
                                                   json.acolhidos[i]
                                                   .telefoneResidencial,
                                                   json.acolhidos[i]
                                                   .telefoneCelular,
                                                   json.acolhidos[i]
                                                   .atendimentoPrioritario,
                                                   currentTime,
                                                   json.acolhidos[i]
                                                   .desligamento);
      view_link = document.createElement('a');
      view_link.href = app_path + '/visualizar-dados-acolhido.jsp?id=' +
                                  json.acolhidos[i].id;
      view_link.style.color = 'black';
      view_link.style.textDecoration = 'none';
      view_link.appendChild(acolhido_div);
      result_pane.appendChild(view_link);
   }
}
/* vi: set sts=3 ts=3 sw=3 et fenc=utf-8: */
