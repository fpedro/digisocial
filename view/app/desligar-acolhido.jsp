<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Desligar acolhido</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <link rel="stylesheet" href="../style.css" />
    <script>
        let params = (new URL(document.location)).searchParams;
        let id = params.get('id');
    </script>
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<script src="script.js" ></script>
<form id="acolhido_form" style="width: 790px" >
  <h1>Desligar acolhido</h1>
  <div id="message_div" ></div>
  <div>
    <canvas id="photo_canvas" width="100" height="115"
            style="float: left; margin: 5px;
                   border: 3px solid skyblue;" ></canvas>
    <div style="margin-top: 10px; float: left" >
      <span>Nome: </span>
      <span id="nomeAcolhido_span" ></span>
    </div>
  </div>
  <div style="float: left" >
    <span>Data de nascimento: </span>
    <span id="diaNascimento_span" ></span> / 
    <span id="mesNascimento_span" ></span> /
    <span id="anoNascimento_span" ></span>&nbsp;&nbsp;
    <span>Sexo: </span><span id="sexo_span" ></span>
  </div>
  <div style="clear: both" ></div>
  <div style="margin-top: 10px" >
    DADOS DA MÃE
  </div>
  <div>
    <span>Nome: </span>
    <span id="nomeMae_span" ></span>
  </div>  
  <div>
    <span>RG: </span>
    <span id="RGmae_span" ></span>&nbsp;&nbsp;
    <span>CPF: </span>
    <span id="CPFmae_p1_span" ></span> . 
    <span id="CPFmae_p2_span" ></span> . 
    <span id="CPFmae_p3_span" ></span> - 
    <span id="CPFmae_p4_span" ></span>
  </div>
  <div style="margin-top: 10px" >
    DADOS DO PAI
  </div>
  <div>
    <span>Nome: </span>
    <span id="nomePai_span" ></span>
  </div>  
  <div>
    <span>RG: </span>
    <span id="RGpai_span" ></span>&nbsp;&nbsp;
    <span>CPF: </span>
    <span id="CPFpai_p1_span" ></span> . 
    <span id="CPFpai_p2_span" ></span> . 
    <span id="CPFpai_p3_span" ></span> - 
    <span id="CPFpai_p4_span" ></span>
  </div>
  <div style="min-height: 10px" ></div>
  <div>
    <span>
      Cadastro realizado por: </span>
    <span id="usuarioNoCadastro_span"
          style="width: 600px; height: 80px;" ></span>
  </div>
  <div>
    <span>
      Data e hora do Cadastro: </span>
    <span id="dataCadastro_span"
          style="width: 600px; height: 80px;" ></span>
  </div>
  <div>
    <span>
      Id do usuário que realizou o cadastro: </span>
    <span id="idUsuarioNoCadastro_span"
          style="width: 600px; height: 80px; clear: both" ></span>
  </div>
  <div style="float: right" >
    <div><span id="info_span" class="infoMessage" ></span></div>
    <input id="desligamento_button" type="button"
           class="operationControl" style="align-self: flex-end"
           value="Desligar acolhido"
           onclick="desligamento_executer()"/>
  </div>
  <div style="clear: both" ></div>
  <input type="hidden" name="operation" value="MODIFY" />
</form>
<script>

  acolhido_form.reset();

  var foto_img = document.createElement('img');

  var ctx = photo_canvas.getContext('2d');

  foto_img.src = 'sem-foto.png';

  foto_img.addEventListener('load', e => {
     ctx.drawImage(foto_img, 0, 0, 100, 115);
  });

  fetch('acolhido.do?id=' + id, {
          method: 'GET'
  }).then(function (response) {
      if (!response.ok)
         throw new Error("HTTP error, status = " + response.status);
      return response.json();
   })
   .then(function (json) {
     nomeAcolhido_span.textContent = json.nomeAcolhido;
     if (json.foto  !== undefined)
        foto_img.src = json.foto;
     dataNascimento = new Date(json.dataNascimento);
     diaNascimento_span.textContent = dataNascimento.getDate();
     mesNascimento_span.textContent = dataNascimento.getMonth() + 1;
     anoNascimento_span.textContent = dataNascimento.getFullYear();
     sexo_span.textContent = json.sexo;
     if (json.nomeMae !== undefined)
        nomeMae_span.textContent = json.nomeMae;
     RGmae_span.textContent = json.RGmae;
     if (json.CPFmae !== undefined) {
        CPF_p1 = parseInt(json.CPFmae / 100000000);
        CPFmae_p1_span.textContent = CPF_p1;
        CPF_p2 = parseInt((json.CPFmae - CPF_p1 * 100000000) / 100000);
        CPFmae_p2_span.textContent = CPF_p2;
        CPF_p3 = parseInt((json.CPFmae - CPF_p1 * 100000000 -
                             CPF_p2 * 100000) / 100);
        CPFmae_p3_span.textContent = CPF_p3;
        CPFmae_p4_span.textContent = json.CPFmae - CPF_p1 * 100000000 -
                                     CPF_p2 * 100000 - CPF_p3 * 100;
     }
     if (json.nomePai !== undefined)
        nomePai_span.textContent = json.nomePai;
     RGpai_span.textContent = json.RGpai;
     if (json.CPFpai !== undefined) {
        CPF_p1 = parseInt(json.CPFpai / 100000000);
        CPFpai_p1_span.textContent = CPF_p1;
        CPF_p2 = parseInt((json.CPFpai - CPF_p1 * 100000000) / 100000);
        CPFpai_p2_span.textContent = CPF_p2;
        CPF_p3 = parseInt((json.CPFpai - CPF_p1 * 100000000 -
                             CPF_p2 * 100000) / 100);
        CPFpai_p3_span.textContent = CPF_p3;
        CPFpai_p4_span.textContent = json.CPFpai - CPF_p1 * 100000000 -
                                     CPF_p2 * 100000 - CPF_p3 * 100;
     }
     usuarioNoCadastro_span
       .textContent = json.userOnRegistry.nome;
     idUsuarioNoCadastro_span
       .textContent = json.userOnRegistry.id;
     d = new Date(json.dataCadastro);
     h = d.getHours();
     if (h < 10)
        h = '0' + h;
     m = d.getMinutes();
     if (m < 10)
        m = '0' + m;
     dataCadastro_span
       .textContent = '' + d.getDate() +
                      '-' + (d.getMonth() + 1) +
                      '-' + d.getFullYear() + '  ' +
                      h + ':' +  m;
   })      
   .catch(function (error) {
     console.log(error.message);
 });


 function desligamento_executer() {

   desligamento_button.disabled = true;
   info_span.textContent = 'processando...';

   formData = new FormData(acolhido_form);

   formData.set('id', id);
   formData.set('desligamento', '');

   fetch('acolhido.do', {
     method: 'POST',
     body: formData
   }).then(function (response) {
       if (!response.ok) {
           throw new Error("HTTP error, status = " + response.status);
       }
       return response.json();
     })
     .then(function (json) {
       if (json.exception === undefined)
         window.location = 'visualizar-acolhidos.jsp?acolhidoDesligado';
       else {
         while (message_div.hasChildNodes()) {
             message_div.removeChild(message_div.firstChild);
         }
         message_span = document.createElement('span');
         message_span.className = 'exceptionMessage';
         message_span.textContent = json.exception;
         message_div.appendChild(message_span);
         setTimeout(clearMessage, 3000, message_span);
         setTimeout(removeMessage, 5000, message_span);
         window.scroll(0, 0);
         salvarAlteracoes_button.disabled = false;
         info_span.textContent = '';
       }
     })
     .catch(function (error) {
       console.log(error.message);
     });
 }

 function clearMessage(e) {
   window.requestAnimationFrame(function (time) {
     window.requestAnimationFrame(function (time) {
       e.style.opacity = 0; });
   });
 }

 function removeMessage(e) {
   e.style.display = 'none';
 }

</script>
<%@include file="template-p2.jsp"%>
  </body>
</html>
<!-- vi: set sts=2 ts=2 sw=2 et fenc=utf-8: -->