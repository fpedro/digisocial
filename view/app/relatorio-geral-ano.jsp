<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Relatório Geral do Ano</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <script>

      var dataAtual = new Date();

      let params = (new URL(document.location)).searchParams;

      mes = params.get('mes');
      ano = params.get('ano');

      if (mes === null)
        mes = dataAtual.getMonth();
      else
        mes = Number.parseInt(mes) - 1;

      if (ano === null)
        ano = dataAtual.getFullYear();

    </script>
    <style>

      * {
        font-size: 14pt;
        font-family: sans-serif;
      }

      body {
        margin: 0;
        background-color: skyblue;
      }

      h1 {
        font-size: 18pt;
      }

      span {
        width: 80px;
      }

      table {
        width: 310px;
        border-collapse: collapse;
      }

      td {
        border: 2px solid skyblue;
        padding: 3px;
        text-align: center;
      }

      .result {
        padding: 2px;
        color: blue;
        border: 2px solid skyblue;
      }

      .itemRelatorio {
        display: block;  
        margin: 5px;
        padding: 1px;
      }

      #relatorio {
        display: inline-block;
        margin: 30px;
        padding: 5px;
        background-color: white;
      }

    </style>
  </head>
  <body>
    <div id="relatorio" >
      <div style="display: block" >
        <h1>Relatório Geral do Ano</h1>
        <h2 id="mesH2" ></h2>
      </div>
      <div style="float: left" >
        <div class="itemRelatorio" >
          <span>Acolhidos atendidas: </span>
          <span id="acolhidosAtendidasSPAN" class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>Desligamentos: </span>
          <span id="desligamentosSPAN" class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>Desligamentos com menos de 1 mês: </span>
          <span class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>IDADES</span>
          <table>
            <tr>
              <td>Menos de 8 anos</td>
              <td id="menosDe8AnosTD" style="color: blue" >0</td>
              <td id="menosDe8AnosTDpc" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Com 8 anos até menos de 10 anos</td>
              <td id="com8AnosAteMenosDe10AnosTD" style="color: blue" >0</td>
              <td id="com8AnosAteMenosDe10AnosTDpc" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Com 10 anos ou mais</td>
              <td id="com10AnosOuMaisTD" style="color: blue" >0</td>
              <td id="com10AnosOuMaisTDpc" style="color: blue" >0%</td>
            </tr>
          </table>
        </div>
        <div class="itemRelatorio" >
          <span>Famí­lias atendidas: </span>
          <span class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>TEMPO DE PERMANÃNCIA NO PROJETO</span>
          <table>
            <tr>
              <td>3 anos ou mais</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>2 anos até menos de 3 anos</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>1 ano até menos de 2 anos </td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>1 semestre até menos de 1 ano</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>1 mês até menos de 1 semestre</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Menos de 1 mês</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
          </table>
        </div>
        <div class="itemRelatorio" style="display: none" >
          <span>Atividades Realizadas</span>
          <table>
            <tr>
              <td>Socio Educativa</td>
              <td style="color: blue" >0</td>
            </tr>
            <tr>
              <td>Oficina de violão</td>
              <td style="color: blue" >0</td>
            </tr>
            <tr>
              <td>Artesanato com papel</td>
              <td style="color: blue" >0</td>
            </tr>
            <tr>
              <td>Futsal</td>
              <td style="color: blue" >0</td>
            </tr>
          </table>
        </div>
      </div>
      <div style="float: left; margin-left: 5px" >
        <div class="itemRelatorio" >
          <span>Meninos: </span>
          <span class="result" >0</span><span class="result"
                                              style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" style="margin-top: 10px" >
          <span>Meninas: </span>
          <span class="result" >0</span><span class="result"
                                              style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" >
          <span>PERÍODO</span>
        </div>
        <div class="itemRelatorio" >
          <span>Manhã: </span>
          <span class="result" >0</span><span class="result"
                                              style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" style="margin-top: 10px" >
          <span>Tarde: </span>
          <span class="result" >0</span><span class="result"
                                              style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" >
          <span>BAIRROS</span>
          <table>
            <tr>
              <td>Centro</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Ipiranga</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Nogueira</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Itapema</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Freguesia</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Itaoca</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
          </table>
        </div>
        <div class="itemRelatorio" >
          <span>ESCOLAS</span>
          <table>
            <tr>
              <td>E. E. Ivan Brasil</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>E. E. Getúlio Vargas</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Célia Leonor</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Outras</td>
              <td style="color: blue" >0</td>
              <td style="color: blue" >0%</td>
            </tr>
          </table>
        </div>
      </div>
      <div style="clear: both" ></div>
    </div>
    <script>

      mesH2 = document.getElementById('mesH2');
      acolhidosAtendidasSPAN = document.getElementById('acolhidosAtendidasSPAN');
      desligamentosSPAN = document.getElementById('desligamentosSPAN');
      menosDe8AnosTD = document.getElementById('menosDe8AnosTD');
      menosDe8AnosTDpc = document.getElementById('menosDe8AnosTDpc');
      com8AnosAteMenosDe10AnosTD =
        document.getElementById('com8AnosAteMenosDe10AnosTD');
      com8AnosAteMenosDe10AnosTDpc =
        document.getElementById('com8AnosAteMenosDe10AnosTDpc');
      com10AnosOuMaisTD = document.getElementById('com10AnosOuMaisTD');
      com10AnosOuMaisTDpc = document.getElementById('com10AnosOuMaisTDpc');

      mesH2.textContent = ano;

      fetch('visualizar-dados-mes.do?mes=' + mes + '&ano=' + ano)
              .then(function (response) {
                if (!response.ok) {
                  throw new Error('Http error: ' + response.status);
                }
        return response.json();        
      }).then(function (json) {
        /*acolhidosAtendidasSPAN.textContent = json.acolhidosAtendidas;
        desligamentosSPAN.textContent = json.desligamentos;
        menosDe8AnosTD.textContent = json.acolhidosComMenosDe8Anos;
        menosDe8AnosTDpc.textContent = formatPC(
                                         json.acolhidosComMenosDe8Anos /
                                         json.acolhidosAtendidas);
        com8AnosAteMenosDe10AnosTD.textContent =
                json.acolhidosCom8AnosAteMenosDe10Anos;
        com8AnosAteMenosDe10AnosTDpc.textContent = formatPC(
                                         json.acolhidosCom8AnosAteMenosDe10Anos /
                                         json.acolhidosAtendidas);
        com10AnosOuMaisTD.textContent = json.acolhidosCom10AnosOuMais;
        com10AnosOuMaisTDpc.textContent = formatPC(
                                          json.acolhidosCom10AnosOuMais /
                                          json.acolhidosAtendidas);

   
         */
      }).catch(function (error) {
        console.log(error.message);
      });  

      function formatPC(n) {
        var parteInteira;
        var parteFracionada;
        n *= 10000;
        n = Number.parseInt(n);
        if (n % 10 !== 0) {
          parteInteira = Number.parseInt(n / 100);
          parteFracionada = n % 100;
          return parteInteira + "," + parteFracionada + "%";
        } else if (n % 100 !== 0) {
          parteInteira = Number.parseInt(n / 100);
          parteFracionada = n % 100;
          parteFracionada /= 10;
          return parteInteira + "," + parteFracionada + "%";
        } else {
          parteInteira = Number.parseInt(n / 100);
          return parteInteira + "%";
        }
      }

    </script>  
  </body>
</html>
