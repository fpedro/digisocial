<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Reinserir acolhido</title>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../style.css" />
    <script>
        let params = (new URL(document.location)).searchParams;
        let id = params.get('id');
    </script>
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<script src="script.js" ></script>
<form id="acolhido_form" style="width: 790px" >
  <h1>Reinserir acolhido</h1>
  <div id="message_div" >
    <span class="infoMessage" >Processando...</span>
  </div>
  <div>
    <canvas id="photo_canvas" width="100" height="115"
            style="float: left; margin: 5px;
                   border: 3px solid skyblue;" ></canvas>
    <div style="margin-top: 10px; float: left" >
      <span>Nome: </span>
      <input id="nomeAcolhido_input"
             name="nomeAcolhido" maxlength="60"
             type="text" style="width: 545px" required />
    </div>
    <div style="display: block; clear: both" >
      <label for="foto_input" class="optionsButton" >
        Pegar foto...</label>
      <input id="foto_input" type="file" style="opacity: 0"/>
  </div>  
  </div>
  <div>
    <span>Data de nascimento: </span>
    <input id="diaNascimento_input" type="text"
           oninput="validateNumber(this)"
           style="width: 20px" required /> / 
    <input id="mesNascimento_input" type="text"
           oninput="validateNumber(this)"
           style="width: 20px" required /> /
    <input id="anoNascimento_input" type="text"
           oninput="validateNumber(this)"
           style="width: 40px" required />
    <span>Sexo:</span>
    <select id="sexo_select"
            name="sexo" required >
      <option>FEMININO</option>
      <option>MASCULINO</option>
    </select>
  </div>
  <div style="margin-top: 10px" >
    DADOS DA MÃE
  </div>
  <div>
    <span>Nome: </span>
    <input id="nomeMae_input" name="nomeMae" maxlength="60"
           type="text" style="width: 545px" required />
  </div>  
  <div>
    <span>RG: </span>
    <input id="RGmae_input" name="RGmae" maxlength="20"
           type="text" style="width: 165px" required />
    <span>CPF: </span>
    <input id="CPFmae_p1_input" oninput="validateNumber(this)"
           type="text" style="width: 30px" required /> . 
    <input id="CPFmae_p2_input" oninput="validateNumber(this)"
           type="text" style="width: 30px" required /> . 
    <input id="CPFmae_p3_input" oninput="validateNumber(this)"
           type="text" style="width: 30px" required /> - 
    <input id="CPFmae_p4_input" oninput="validateNumber(this)"
           type="text" style="width: 20px" required />
  </div>
  <div>
    <span>Local de trabalho: </span>
    <input id="localTrabalhoMae_input" name="localTrabalhoMae"
           maxlength="60" type="text"
           style="width: 455px" required />
  </div>
  <div style="margin-top: 10px" >
    DADOS DO PAI
  </div>
  <div>
    <span>Nome: </span>
    <input id="nomePai_input" name="nomePai" maxlength="60"
           type="text" style="width: 545px" required />
  </div>  
  <div>
    <span>RG: </span>
    <input id="RGpai_input" name="RGpai" maxlength="20"
           type="text" style="width: 165px" required />
    <span>CPF: </span>
    <input id="CPFpai_p1_input" oninput="validateNumber(this)"
           type="text" style="width: 30px" required /> . 
    <input id="CPFpai_p2_input" oninput="validateNumber(this)"
           type="text" style="width: 30px" required /> . 
    <input id="CPFpai_p3_input" oninput="validateNumber(this)"
           type="text" style="width: 30px" required /> - 
    <input id="CPFpai_p4_input" oninput="validateNumber(this)"
           type="text" style="width: 20px" required />
  </div>
  <div>
    <span>Local de trabalho: </span>
    <input id="localTrabalhoPai_input" name="localTrabalhoPai"
           maxlength="60" type="text"
           style="width: 455px" required />
  </div>
  <div>ENDEREÇO</div>
  <div>
    <span>Rua: </span>
    <input id="ruaEndereco_input" name="endereco.rua" type="text"
           style="width: 455px" maxlength="60" required />
    <span>N°: </span>
    <input id="numeroEndereco_input"
           oninput="validateNumber(this)"
           type="text" style="width: 55px" required />
    <div style="margin: 5px 0 0; padding: 5px 0 0" >
      <span>Complemento: </span>
      <input id="complementoEndereco_input" name="endereco.complemento"
             maxlength="30" type="text" style="width: 220px"/>
    </div>
  </div>
  <div>
    <span>Bairro: </span>
    <select id="bairro_select" 
            onchange="bairro_changed()" required >
      <option>Centro</option>
      <option>Ipiranga</option>
      <option>Nogueira</option>
      <option>Itapema</option>
      <option>Freguesia</option>
      <option>Maracatu</option>
      <option>Itaoca</option>
      <option>Outro</option>
    </select>
    <span class="disabledLabel" 
          id="qualBairro_label" >Qual?</span>
    <input type="text" maxlength="30"
           id="qualBairro_input" style="width: 290px"/>
  </div>  
  <div>                  
    <span>Cidade: </span>
    <input name="endereco.cidade" type="text" maxlength="30"
           style="width: 300px" value="Guararema" required />
  </div>
  <div>
    <span>Volta para casa sozinho:</span>
    <select id="voltaSozinho_select" 
            onchange="voltaSozinho_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <div style="margin: 5px 0 0; padding: 5px 0 0" >
        <span class="disabledLabel" 
              id="comQuem_label" >Com quem?</span>
        <input type="text"  maxlength="50"
               id="comQuem_input" style="width: 390px"/>
    </div>
  </div>
  <div>TELEFONE</div>
  <div>
    <span>Residencial: </span>
    <input id="telefoneResidencial_input"
           name="telefoneResidencial" maxlength="16"
           type="text" style="width: 140px" required />
    <span>Celular: </span>
    <input id="telefoneCelular_input"
           name="telefoneCelular" maxlength="16"
           type="text" style="width: 140px" required />
  </div>
  <div>DADOS DO PROJETO</div>
  <div>
    <span>Perí­odo:</span>
    <select id="periodoPS_select" name="periodoPS" required >
      <option value="MANHA" >MANHÃ</option>
      <option>TARDE</option>
    </select>
  </div>
  <div>
    <span>Já esteve no projeto?</span>
    <select id="esteveNoProjeto_select"
            onchange="esteveNoProjeto_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <span class="disabledLabel"
          id="quantasVezes_label" >Quantas vezes? </span>
    <select id="quantasVezes_select" >
      <option>1</option>
      <option>2</option>
      <option>3</option>
      <option>4</option>
      <option>5</option>
      <option>6</option>
      <option>7</option>
      <option>8</option>
      <option>9</option>
      <option>10</option>
    </select>
  </div>
  <div>
    <span>Atendimento Prioritário: </span>
    <select id="atendimentoPrioritario_select"
            name="atendimentoPrioritario" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
  </div>
  <div>DADOS DA ESCOLA</div>
  <div>
    <span>Escola: </span>
    <select id="escola_select"
            onchange="escola_changed()" required >
      <option value="IB" >E. E. Ivan Brasil</option>
      <option value="GV" >E. E. Getúlio Vargas</option>
      <option value="CL" >E. M. Célia Leonor</option>
      <option value="RF" >E. E. Roberto Feijó</option>
      <option>Outra</option>
    </select>
    <span class="disabledLabel" 
          id="qualEscola_label" >Qual?</span>
    <input type="text" maxlength="30"
           id="qualEscola_input" style="width: 220px" required />
  </div>
  <div>
    <span>Perí­odo: </span>
    <select id="periodoEscolar_select"
            name="escola.periodo" required >
      <option value="MANHA" >MANHÃ</option>
      <option>TARDE</option>
    </select>
    <span>Ano: </span>
    <select id="anoEscolar_select"
            name="escola.ano" required >                  
      <option value="1" >1°</option>
      <option value="2" >2°</option>
      <option value="3" >3°</option>
      <option value="4" >4°</option>
      <option value="5" >5°</option>
      <option value="6" >6°</option>
      <option value="7" >7°</option>
      <option value="8" >8°</option>
    </select>  
  </div>
  <div>DADOS PESSOAIS E DA FAMÍLIA</div>
  <div>
    <span>Possui alguma deficiência?</span>
    <select id="deficiencia_select"
            onchange="deficiencia_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <span class="disabledLabel" 
          id="qualDeficiencia_label" >Qual? </span>
    <input id="qualDeficiencia_input" maxlength="60"
           type="text" style="width: 380px" required />
  </div>
  <div>
    <span>Tem alergia?</span>
    <select id="alergia_select"
            onchange="alergia_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <span class="disabledLabel"
          id="qualAlergia_label" >Qual? </span>
    <input id="qualAlergia_input" maxlength="60"
           type="text" style="width: 490px" required />
  </div>
  <div>
    <span>Toma medicamento?</span>
    <select id="medicamento_select"
            onchange="medicamento_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <span class="disabledLabel"
          id="qualMedicamento_label" >Qual? </span>
    <input id="qualMedicamento_input" maxlength="60"
           type="text" style="width: 430px" required />
  </div>
  <div>
    <span>Recebe bolsa famí­lia?</span>
    <select id="bolsaFamilia_select"
            name="bolsaFamilia" required >
      <option value="true" >SIM</option>
      <option value="false" >NÃO</option>
    </select>
  </div>
  <div>FAMÍLIARES QUE MORAM COM O ACOLHIDO</div>
  <div id="familiares" >
    <div id="familiarTemplate" style="display: none" >
      <div>
        <span>Nome: </span>
        <input id="nomeFamiliar_input" type="text"
               maxlength="60" style="width: 570px" required />
        <span id="removerFamiliar" class="optionsButton" >Remover</span>
      </div>
      <div>
        <span>Idade: </span>
        <input id="idadeFamiliar_input"
               oninput="validateNumber(this)" type="text"
               style="width: 30px" required />
        <span>Parentesco: </span>
        <input id="parentescoFamiliar_input" type="text"
               maxlength="20" style="width: 160px" required />
        <span>Ocupação: </span>
        <input id="ocupacaoFamiliar_input" type="text"
               maxlength="30" style="width: 240px" required />
      </div>
    </div>
  </div>
  <script>

    function adicionarFamiliar() {
        var novoFamiliar = familiarTemplate.cloneNode(true);
        novoFamiliar.setAttribute('id', 'familiar');
        nome = novoFamiliar.querySelector('#nomeFamiliar');
        removerOptions = novoFamiliar
                           .querySelector('#removerFamiliar');
        removerOptions.onclick = function removerFamiliar() {
            familiares.removeChild(novoFamiliar);
        };
        novoFamiliar.style.display = 'block';
        familiares.appendChild(novoFamiliar);
    }

  </script>
  <span class="optionsButton" 
        style="clear: both" onclick="adicionarFamiliar()" >
    Adicionar familiar...</span>
  <div>
    <span>O acolhido já teve alguma doença grave quando pequeno?</span>
    <div style="display: block" ></div>
    <textarea id="doencaGrave_descricao" name="doencaGrave"
              style="width: 600px; height: 80px; clear: both"
              maxlength="200" ></textarea>
  </div> 
  <div>
    <span>Já fez algum tratamento? Quanto tempo?</span>
    <div style="clear: both" >
      <div>
        <input id="neurologista_checkbox" type="checkbox"
               onchange="neurologista_changed()"/>
        <label for="neurologista_checkbox" >Neurologista</label>
        <input id="neurologista_input" maxlength="60"
               style="margin-left: 5px; width: 400px"
               type="text"/>
      </div>
      <div>
        <input id="psiquiatra_checkbox" type="checkbox"
               onchange="psiquiatra_changed()"/>
        <label for="psiquiatra" >Psiquiatra</label>
        <input id="psiquiatra_input" maxlength="60"
               style="margin-left: 5px; width: 418px" 
               type="text"/>
      </div>
      <div>
        <input id="psicologo_checkbox" type="checkbox"
               onchange="psicologo_changed()"/>
        <label for="psicologo" >Psicólogo</label>
        <input id="psicologo_input" maxlength="60"
               style="margin-left: 5px; width: 425px"
               type="text"/>
      </div>
      <div>
        <input id="psicopedagogo_checkbox" type="checkbox"
               onchange="psicopedagogo_changed()"/>
        <label for="psicopedagogo_checkbox" >Psicopedagogo
        </label>
        <input id="psicopedagogo_input" maxlength="60"
               style="margin-left: 5px; width: 382px"
               type="text"/>
      </div>
      <div>
        <input id="fonoaudiologo_checkbox" type="checkbox"
               onchange="fonoaudiologo_changed()"/>
        <label for="fonoaudiologo_checkbox" >Fonoaudiólogo
        </label>
        <input id="fonoaudiologo_input" maxlength="60"
               style="margin-left: 5px; width: 382px"
               type="text"/>
      </div>
      <div>
        <input id="outrosTratamentos_checkbox" type="checkbox"
               onchange="outrosTratamentos_changed()"/>
        <label for="outrosTratamentos" >Outros
        </label>
        <input id="outrosTratamentos_input" maxlength="60"
               style="margin-left: 5px; width: 442px"
               type="text"/>
      </div>
    </div>
  </div> 
  <div>
    <span style="display: block" >Na famí­lia tem algum caso de diabetes,
    </span>
    <span style="display: block" >
      &nbsp;&nbsp;problemas do coração, transtorno mental,
    </span>
    <span style="display: block" >&nbsp;&nbsp;alcoolismo, drogas, etc. ?
    </span>
    <textarea id="problemaNaFamilia_descricao"
              name="problemaNaFamilia"
              style="width: 600px; height: 80px; clear: both"
              maxlength="200" ></textarea>
  </div>
  <div>
    <span style="display: block" >
      O que o acolhido gosta de fazer em casa?</span>
    <textarea id="gostaDeFazer_descricao" name="gostaDeFazer"
              style="width: 600px; height: 80px; clear: both"
              maxlength="200" required ></textarea>
  </div>
  <div>
    <span style="display: block" >
      Como o acolhido se relaciona com a famí­lia?</span>
    <textarea id="relacionamentoComFamilia_descricao"
              name="relacionamentoComFamilia"
              style="width: 600px; height: 80px; clear: both"
              maxlength="200" required ></textarea>
  </div>
  <div style="float: right" >
    <div><span id="info_span" class="infoMessage" ></span></div>
    <input id="reinsercao_button" type="button"
           class="operationControl" style="align-self: flex-end"
           value="Reinserir no projeto" onclick="reinsercao_executer()"/>
  </div>
  <div style="clear: both" ></div>
  <input type="hidden" name="operation" value="SAVE" />
</form>
<script>

  nomeAcolhido_input.focus();

  acolhido_form.reset();

  sexo_select.value = '';
  voltaSozinho_select.value = '';
  bairro_select.value = '';
  periodoPS_select.value = '';
  esteveNoProjeto_select.value = '';
  quantasVezes_select.value = '';
  atendimentoPrioritario_select.value = '';
  escola_select.value = '';
  periodoEscolar_select.value = '';
  anoEscolar_select.value = '';
  deficiencia_select.value = '';
  alergia_select.value = '';
  medicamento_select.value = '';
  bolsaFamilia_select.value = '';

  qualBairro_input.disabled = true;
  comQuem_input.disabled = true;
  quantasVezes_select.disabled = true;
  qualEscola_input.disabled = true;
  qualDeficiencia_input.disabled = true;  
  qualAlergia_input.disabled = true;  
  qualMedicamento_input.disabled = true;  
  neurologista_input.disabled = true;  
  psiquiatra_input.disabled = true;
  psicologo_input.disabled = true;
  psicopedagogo_input.disabled = true;
  fonoaudiologo_input.disabled = true;
  outrosTratamentos_input.disabled = true;

  var foto_img = document.createElement('img');
  var photoSelected = false;

  var ctx = photo_canvas.getContext('2d');
  var currentPhoto = null;

  foto_img.src = 'sem-foto.png';
  foto_input.addEventListener('change', updateFoto);

  foto_img.addEventListener('load', e => {
    ctx.drawImage(foto_img, 0, 0, 100, 115);
  });

  function updateFoto() {
    var curFoto = foto_input.files[0];
    foto_img.src = window.URL.createObjectURL(curFoto);
    photoSelected = true;
  }

  function bairro_changed() {
    if (bairro_select.value === 'Outro')
      enable(qualBairro_label, qualBairro_input);
    else
      disable(qualBairro_label, qualBairro_input);
  }

  function voltaSozinho_changed() {
    if (voltaSozinho_select.value === 'NÃO')
      enable(comQuem_label, comQuem_input);
    else
      disable(comQuem_label, comQuem_input);
  }

  function esteveNoProjeto_changed() {
    if (esteveNoProjeto_select.value === 'SIM')
      enable(quantasVezes_label, quantasVezes_select);
    else
      disable(quantasVezes_label, quantasVezes_select);
  }

  function escola_changed() {
    if (escola_select.value === 'Outra')
      enable(qualEscola_label, qualEscola_input);
    else
      disable(qualEscola_label, qualEscola_input);
  }  

  function deficiencia_changed() {
    if (deficiencia_select.value === 'SIM')
      enable(qualDeficiencia_label, qualDeficiencia_input);
    else
      disable(qualDeficiencia_label, qualDeficiencia_input);
  }  

  function alergia_changed() {
    if (alergia_select.value === 'SIM')
      enable(qualAlergia_label, qualAlergia_input);
    else
      disable(qualAlergia_label, qualAlergia_input);
  }  

  function medicamento_changed() {
    if (medicamento_select.value === 'SIM')
      enable(qualMedicamento_label, qualMedicamento_input);
    else
      disable(qualMedicamento_label, qualMedicamento_input);
  }  

  function neurologista_changed() {
    if (neurologista_checkbox.checked) 
      tempoDeTratamento_enable(neurologista_input);
    else
      tempoDeTratamento_disable(neurologista_input);
  }  

  function psiquiatra_changed() {
    if (psiquiatra_checkbox.checked) 
      tempoDeTratamento_enable(psiquiatra_input);
    else
      tempoDeTratamento_disable(psiquiatra_input);
  }  

  function psicologo_changed() {
    if (psicologo_checkbox.checked) 
      tempoDeTratamento_enable(psicologo_input);
    else
      tempoDeTratamento_disable(psicologo_input);
  }  

  function psicopedagogo_changed() {
    if (psicopedagogo_checkbox.checked) 
      tempoDeTratamento_enable(psicopedagogo_input);
    else
      tempoDeTratamento_disable(psicopedagogo_input);
  }  

  function fonoaudiologo_changed() {
    if (fonoaudiologo_checkbox.checked) 
      tempoDeTratamento_enable(fonoaudiologo_input);
    else
      tempoDeTratamento_disable(fonoaudiologo_input);
  }  

  function outrosTratamentos_changed() {
    if (outrosTratamentos_checkbox.checked) 
      tempoDeTratamento_enable(outrosTratamentos_input);
    else
      tempoDeTratamento_disable(outrosTratamentos_input);
  }  

  function tempoDeTratamento_enable(input) {
    input.disabled = false;
    input.required = true;
  }

  function tempoDeTratamento_disable(input) {
    input.value = '';  
    input.disabled = true;
    input.required = false;
  }

  function enable(label, input) {
    label.className = '';
    input.disabled = false;
    input.required = true;
  }

  function disable(label, input) {
    label.className = 'disabledLabel';
    input.value = '';  
    input.disabled = true;
    input.required = false;
  }

fetch('acolhido.do?id=' + id, {
  method: 'GET'
}).then(function (response) {
    if (!response.ok)
        throw new Error("HTTP error, status = " + response.status);
    return response.json();
  })
  .then(function (json) {
    nomeAcolhido_input.value = json.nomeAcolhido;
    if (json.foto  !== undefined) {
       foto_img.src = json.foto;
       currentPhoto = json.foto;
    }
    dataNascimento = new Date(json.dataNascimento);
    diaNascimento_input.value = dataNascimento.getDate();
    mesNascimento_input.value = dataNascimento.getMonth() + 1;
    anoNascimento_input.value = dataNascimento.getFullYear();
    sexo_select.value = json.sexo;
    nomeMae_input.value = json.nomeMae;
    RGmae_input.value = json.RGmae;
    if (json.CPFmae !== undefined) {
       CPF_p1 = parseInt(json.CPFmae / 100000000);
       CPFmae_p1_input.value = CPF_p1;
       CPF_p2 = parseInt((json.CPFmae - CPF_p1 * 100000000) / 100000);
       CPFmae_p2_input.value = CPF_p2;
       CPF_p3 = parseInt((json.CPFmae - CPF_p1 * 100000000 -
                            CPF_p2 * 100000) / 100);
       CPFmae_p3_input.value = CPF_p3;
       CPFmae_p4_input.value = json.CPFmae - CPF_p1 * 100000000 -
                                 CPF_p2 * 100000 - CPF_p3 * 100;
    }
    localTrabalhoMae_input.value = json.localTrabalhoMae;
    nomePai_input.value = json.nomePai;
    RGpai_input.value = json.RGpai;
    if (json.CPFpai !== undefined) {
       CPF_p1 = parseInt(json.CPFpai / 100000000);
       CPFpai_p1_input.value = CPF_p1;
       CPF_p2 = parseInt((json.CPFpai - CPF_p1 * 100000000) / 100000);
       CPFpai_p2_input.value = CPF_p2;
       CPF_p3 = parseInt((json.CPFpai - CPF_p1 * 100000000 -
                            CPF_p2 * 100000) / 100);
       CPFpai_p3_input.value = CPF_p3;
       CPFpai_p4_input.value = json.CPFpai - CPF_p1 * 100000000 -
                                 CPF_p2 * 100000 - CPF_p3 * 100;
    }
    localTrabalhoPai_input.value = json.localTrabalhoPai;
    ruaEndereco_input.value = json.ruaEndereco;
    numeroEndereco_input.value = json.numeroEndereco;
    complementoEndereco_input.value = json.complementoEndereco;
    switch (json.bairroEndereco) {
      case 'Centro':
      case 'Ipiranga':
      case 'Nogueira':
      case 'Itapema':
      case 'Freguesia':
      case 'Maracatu': bairro_select.value = json.bairroEndereco; break;
      default: 
        bairro_select.value = 'Outro'; 
        qualBairro_input.value = json.bairroEndereco;
        bairro_changed();
    }
    if (json.comQuemVoltaParaCasa !== undefined) {
      voltaSozinho_select.value = 'NÃO';
      comQuem_input.value = json.comQuemVoltaParaCasa;
      voltaSozinho_changed();
    }
    else voltaSozinho_select.value = 'SIM';
    telefoneResidencial_input.value = json.telefoneResidencial;
    telefoneCelular_input.value = json.telefoneCelular;
    periodoPS_select.value = json.periodoPS;
    if (json.quantasVezesNoProjeto !== 0) {
      esteveNoProjeto_select.value = 'SIM';
      quantasVezes_select.value = json.quantasVezesNoProjeto;
    } else esteveNoProjeto_select.value = 'NÃO';
    if (json.atendimentoPrioritario)
      atendimentoPrioritario_select.value = 'SIM';
    else atendimentoPrioritario_select.value = 'NÃO';
    switch (json.nomeEscola) {
      case 'IB':
      case 'GV':
      case 'CL':
      case 'RF': escola_select.value = json.nomeEscola; break;
      default: 
        escola_select.value = 'Outra'; 
        qualEscola_input.value = json.nomeEscola;
        escola_changed();
    }
    periodoEscolar_select.value = json.periodoEscola;
    anoEscolar_select.value = json.anoEscola;
    if (json.deficiencia !== undefined) {
      deficiencia_select.value = 'SIM';
      qualDeficiencia_input.value = json.deficiencia;
      deficiencia_changed();
    }
    else deficiencia_select.value = 'NÃO';
    if (json.alergia !== undefined) {
      alergia_select.value = 'SIM';
      qualAlergia_input.value = json.alergia;
      alergia_changed();
    }
    else alergia_select.value = 'NÃO';
    if (json.medicamento !== undefined) {
      medicamento_select.value = 'SIM';
      qualMedicamento_input.value = json.medicamento;
      medicamento_changed();
    }
    else medicamento_select.value = 'NÃO';
    if (json.bolsaFamilia)
      bolsaFamilia_select.value = 'true';
    else bolsaFamilia_select.value = 'false';
    for (i = 0; i < json.familiares.length; i++) {
      n = familiarTemplate.cloneNode(true);
      f = n.querySelector('#nomeFamiliar_input');
      f.value = json.familiares[i].nome;
      f = n.querySelector('#idadeFamiliar_input');
      f.value = json.familiares[i].idade;
      f = n.querySelector('#parentescoFamiliar_input');
      f.value = json.familiares[i].parentesco;
      f = n.querySelector('#ocupacaoFamiliar_input');
      f.value = json.familiares[i].ocupacao;
      n.style.display = 'block';
      familiares.appendChild(n);
    }
    doencaGrave_descricao.value = json.doencaGrave;
    if (json.tratamentoNeurologista !== undefined) {
      neurologista_checkbox.checked = true;
      neurologista_input.value = json.tratamentoNeurologista;
      neurologista_changed();
    }
    if (json.tratamentoPsiquiatra !== undefined) {
      psiquiatra_checkbox.checked = true;
      psiquiatra_input.value = json.tratamentoPsiquiatra;
      psiquiatra_changed();
    }
    if (json.tratamentoPsicologo !== undefined) {
      psicologo_checkbox.checked = true;
      psicologo_input.value = json.tratamentoPsicologo;
      psicologo_changed();
    }
    if (json.tratamentoPsicopedagogo !== undefined) {
      psicopedagogo_checkbox.checked = true;
      psicopedagogo_input.value = json.tratamentoPsicopedagogo;
      psicologo_changed();
    }
    if (json.tratamentoFonoaudiologo !== undefined) {
      fonoaudiologo_checkbox.checked = true;
      fonoaudiologo_input.value = json.tratamentoFonoaudiologo;
      fonoaudiologo_changed();
    }
    if (json.tratamentoOutros !== undefined) {
      outrosTratamentos_checkbox.checked = true;
      outrosTratamentos_input.value = json.tratamentoOutros;
      outrosTratamentos_changed();
    }
    problemaNaFamilia_descricao
      .value = json.problemaNaFamilia;
    gostaDeFazer_descricao.value = json.gostaDeFazer;
    relacionamentoComFamilia_descricao
      .value = json.relacionamentoComFamilia;
    while (message_div.hasChildNodes())
      message_div.removeChild(message_div.firstChild);
  })      
  .catch(function (error) {
    console.log(error.message);
});
  
  function reinsercao_executer() {

    reinsercao_button.disabled = true;
    info_span.textContent = 'processando...';

    formData = new FormData(acolhido_form);

    formData.set('id', id);

    if (photoSelected)
      formData.set('foto', photo_canvas.toDataURL());
   else if (currentPhoto !== null)
      formData.set('foto', currentPhoto);

    if (anoNascimento_input.value !== '' &&
          mesNascimento_input.value !== '' &&
          diaNascimento_input.value !== '') {
      dataNascimento = new Date(anoNascimento_input.value,
                                  mesNascimento_input.value - 1,
                                  diaNascimento_input.value);
      formData.set('dataNascimento', dataNascimento.getTime());
    }

    nomePai_input.required = true;
    RGpai_input.required = true;
    CPFpai_p1_input.required = true;
    CPFpai_p2_input.required = true;
    CPFpai_p3_input.required = true;
    CPFpai_p4_input.required = true;
    localTrabalhoPai_input.required = true;

    nomeMae_input.required = true;
    RGmae_input.required = true;
    CPFmae_p1_input.required = true;
    CPFmae_p2_input.required = true;
    CPFmae_p3_input.required = true;
    CPFmae_p4_input.required = true;
    localTrabalhoMae_input.required = true;

    if (nomeMae_input.value !== '') {
      nomePai_input.required = false;
      RGpai_input.required = false;
      CPFpai_p1_input.required = false;
      CPFpai_p2_input.required = false;
      CPFpai_p3_input.required = false;
      CPFpai_p4_input.required = false;
      localTrabalhoPai_input.required = false;
    } else if (nomePai_input.value !== '') {
      nomeMae_input.required = false;
      RGmae_input.required = false;
      CPFmae_p1_input.required = false;
      CPFmae_p2_input.required = false;
      CPFmae_p3_input.required = false;
      CPFmae_p4_input.required = false;
      localTrabalhoMae_input.required = false;
    }

    if (CPFmae_p4_input.value !== '' &&
          CPFmae_p3_input.value !== '' &&
          CPFmae_p2_input.value !== '' &&
          CPFmae_p1_input.value !== '') {
      CPFmae = Number.parseInt(CPFmae_p4_input.value);
      CPFmae += Number.parseInt(CPFmae_p3_input.value) * 100;
      CPFmae += Number.parseInt(CPFmae_p2_input.value) * 100000;
      CPFmae += Number.parseInt(CPFmae_p1_input.value) * 100000000;
      formData.set('CPFmae', CPFmae);
    }

    if (CPFpai_p4_input.value !== '' &&
          CPFpai_p3_input.value !== '' &&
          CPFpai_p2_input.value !== '' &&
          CPFpai_p1_input.value !== '') {
      CPFpai = Number.parseInt(CPFpai_p4_input.value);
      CPFpai += Number.parseInt(CPFpai_p3_input.value) * 100;
      CPFpai += Number.parseInt(CPFpai_p2_input.value) * 100000;
      CPFpai += Number.parseInt(CPFpai_p1_input.value) * 100000000;
      formData.set('CPFpai', CPFpai);
    }

    numeroEndereco = numeroEndereco_input.value;
    if (numeroEndereco !== '')
      formData.set('endereco.numero', numeroEndereco);

    bairro = bairro_select.value;
    if (bairro === 'Outro')
      bairro = qualBairro_input.value;
    formData.set('endereco.bairro', bairro);

    if (voltaSozinho_select.value === 'NÃO') {
      comQuem = comQuem_input.value;
      formData.set('comQuemVoltaParaCasa', comQuem);
    }

    telefoneResidencial_input.required = true;
    telefoneCelular_input.required = true;

    if (telefoneResidencial_input.value !== '')
      telefoneCelular_input.required = false;

    if (telefoneCelular_input.value !== '')
      telefoneResidencial_input.required = false;

    quantasVezes = quantasVezes_select.value;
    if (quantasVezes !== '')
      formData.set('quantasVezesNoProjeto', quantasVezes);

    escola = escola_select.value;
    if (escola === 'Outra')
      escola = qualEscola_input.value;
    formData.set('escola.nome', escola);

    if (deficiencia_select.value === 'SIM') {
      qualDeficiencia = qualDeficiencia_input.value;
      formData.set('deficiencia', qualDeficiencia);
    }

    if (alergia_select.value === 'SIM') {
      qualAlergia = qualAlergia_input.value;
      formData.set('alergia', qualAlergia);
    }

    if (medicamento_select.value === 'SIM') {
      qualMedicamento = qualMedicamento_input.value;
      formData.set('medicamento', qualMedicamento);
    }

    familiares_list = familiares.querySelectorAll('#familiar');
    for (i = 0; i < familiares_list.length; i++) {
      nomeFamiliar = familiares_list[i]
                           .querySelector('#nomeFamiliar_input').value;
      idadeFamiliar = familiares_list[i]
                           .querySelector('#idadeFamiliar_input').value;
      parentescoFamiliar = familiares_list[i]
                           .querySelector('#parentescoFamiliar_input').value;
      ocupacaoFamiliar = familiares_list[i]
                           .querySelector('#ocupacaoFamiliar_input').value;
      formData.append('familiares.nome', nomeFamiliar);
      formData.append('familiares.idade', idadeFamiliar);
      formData.append('familiares.parentesco', parentescoFamiliar);
      formData.append('familiares.ocupacao', ocupacaoFamiliar);
    }

    if (neurologista_checkbox.checked)
      formData.set('tratamentoNeurologista', neurologista_input.value);

    if (psiquiatra_checkbox.checked)
      formData.set('tratamentoPsiquiatra', psiquiatra_input.value);

    if (psicologo_checkbox.checked)
      formData.set('tratamentoPsicologo', psicologo_input.value);

    if (psicopedagogo_checkbox.checked)
      formData.set('tratamentoPsicopedagogo', psicopedagogo_input.value);

    if (fonoaudiologo_checkbox.checked)
      formData.set('tratamentoFonoaudiologo', fonoaudiologo_input.value);

    if (outrosTratamentos_checkbox.checked)
      formData.set('tratamentoOutros', outrosTratamentos_input.value);

    for (i = 0; i < acolhido_form.elements.length; i++) {
      if (acolhido_form.elements[i].validity.valueMissing)
        acolhido_form.elements[i].classList.add('exceptionField');
      else
        acolhido_form.elements[i].classList.remove('exceptionField');
    }

    fetch('acolhido.do', {
      method: 'POST',
      body: formData
    }).then(function (response) {
        if (!response.ok) {
            throw new Error("HTTP error, status = " + response.status);
        }
        return response.json();
      })
      .then(function (json) {
	console.log(json);
        if (json.exception === undefined)
          window.location = 'visualizar-acolhidos.jsp?acolhidoReinserido=' +
                              json.id;
        else {
          while (message_div.hasChildNodes())
            message_div.removeChild(message_div.firstChild);
          message_span = document.createElement('span');
          message_span.className = 'exceptionMessage';
          message_span.textContent = json.exception;
          message_div.appendChild(message_span);
          setTimeout(clearMessage, 3000, message_span);
          setTimeout(removeMessage, 5000, message_span);
          console.log(json);
          window.scroll(0, 0);
          reinsercao_button.disabled = false;
          info_span.textContent = '';
        }
      })
      .catch(function (error) {
        console.log(error.message);
      });
  }

  function clearMessage(e) {
    window.requestAnimationFrame(function (time) {
      window.requestAnimationFrame(function (time) {
        e.style.opacity = 0; });
    });
  }

  function removeMessage(e) {
    e.style.display = 'none';
  }

</script>
<%@include file="template-p2.jsp"%>
  </body>
</html>
