<!DOCTYPE html>
<html>
  <head>
    <title>Agenda de atividades</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <script>

      let params = (new URL(document.location)).searchParams;
      var semestre;
      var ano;
      var dataAtual = new Date();

      var novaatividade;
      var atividaderemovida;

      semestre = params.get('semestre');
      ano = params.get('ano');
      
      if (ano === null || semestre === null) {
        semestre = dataAtual.getMonth() < 6 ? 1 : 2;
        ano = dataAtual.getFullYear();
      }
      
      ano = Number.parseInt(ano);
      semestre = Number.parseInt(semestre);

      novaatividade = params.get('novaatividade');
      atividaderemovida = params.get('atividaderemovida');
  
    </script>
    <style>
      * {
        font-size: 14pt;
        font-family: sans-serif;
      }
      
      #up {
        position: fixed;
        bottom: 10px;
        right: 250px;
        width: 40px;
        height: 40px;
        cursor: pointer;
      }
      
      
      a, a:visited {
        color: blue;
        text-decoration: underline;
        cursor: pointer;
      }
      
      .semestreLink {
        color: blue;
        text-decoration: none;
        cursor: pointer;
      }
      
      .enter {
        width: 18px;
        height: 18px;
        cursor: pointer;
      }
      
      .optionsButton {
        margin: 5px;
        color: blue;
        text-decoration: underline;
        cursor: pointer;
      }
      
      table {
        border-collapse: collapse;
        border: 2px solid rgb(200,200,200);
        letter-spacing: 1px;
        font-size: 0.8rem;
      }

      td, th {
        border: 1px solid rgb(190,190,190);
        padding: 10px 20px;
      }

      th {
        /*font-style: oblique;*/
        background-color: rgb(235,235,235);
      }

      td {
        text-align: center;
      }

      tr:nth-child(even) td {
        background-color: rgb(250,250,250);
      }

      tr:nth-child(odd) td {
        background-color: rgb(245,245,245);
      }

    </style>
  </head>
  <body style="margin: 30px" >
    <span id="novaatividadeSpan" style="display: none; color: #005aff" >
      Nova atividade adicionada
    </span>
    <span id="atividaderemovidaSpan" style="display: none; color: #005aff" >
      Atividade removida
    </span>
    <a id="semestreAnterior" href="" class="semestreLink" >&lt;--</a>&nbsp;&nbsp;
    <h1 id="semestreHeading" style="display: inline-block" ></h1>
    &nbsp;&nbsp;<a id="semestreSeguinte" href="" class="semestreLink" >--&gt;</a>
    <span class="optionsButton" style="margin-left: 450px" >
      <a href="agendar-atividade.html" >Agendar atividade</a></span>
    <table id="atividadesTable" >
      <tbody id="atividadesTableBody" >
      <tr>
        <th colspan="2" >DIA</th>
        <th>ATIVIDADE</th>
        <th>ORIENTADOR</th>
        <th>HOR�RIO</th>
      </tr>
      <tr id="linhaTemplate" style="display: none" >
        <td id="diaTemplate" rowspan="1" ></td>
        <td id="diadasemanaTemplate" rowspan="1" ></td>
        <td id="atividadeTemplate" ></td>
        <td id="orientadorTemplate" ></td>
        <td id="horarioTemplate" ></td>
        <td><a id="atividadelinkTemplate"
            ><img class="enter" src="enter.png"
                 alt="Entrar" onmouseover="highLightEnter(this)"
                 onmouseout="outEnter(this)"/></a></td>
      </tr>
      </tbody>
    </table>
    <script>

      var novaatividadeSpan = document.getElementById('novaatividadeSpan');
      var atividaderemovidaSpan = document
                                    .getElementById('atividaderemovidaSpan');

      if (novaatividade !== null) {
        novaatividadeSpan.style.display = 'block';
      }
      
      if (atividaderemovida !== null) {
        atividaderemovidaSpan.style.display = 'block';
      }

      var semestreHeading = document.getElementById('semestreHeading');
      var semestreSeguinteLink = document.getElementById('semestreSeguinte');
      var semestreAnteriorLink = document.getElementById('semestreAnterior');
      
      var anoSeguinte = ano;
      var anoAnterior = ano;
      
      
      semestreSeguinte = semestre === 1 ? 2 : 1;
      semestreAnterior = semestre === 1 ? 2 : 1;
      
      if (semestre === 2)
        anoSeguinte = ano + 1;
      
      if (semestre === 1)
        anoAnterior = ano - 1;
      
      semestreSeguinteLink.href = '?semestre=' +
                                    semestreSeguinte + '&ano=' +
                                    anoSeguinte;
      
      semestreAnteriorLink.href = '?semestre=' +
                                    semestreAnterior + '&ano=' +
                                    anoAnterior;
            
      var d = new Date();
      var semestreText = '';
      
      semestreText += semestre;
      semestreText += '� semestre de ';
      semestreText += ano;
      
      semestreHeading.textContent = semestreText;
      
      var tabela = document.getElementById('atividadesTableBody');
      var linha = document.getElementById('linhaTemplate');
      var linhaComDestaque = Number.parseInt(params.get('destacar'));
  
      function highLightEnter(e){
        e.src = "enter-selected.png";
      }
      function outEnter(e){
        e.src = "enter.png";
      }
     
      fetch('visualizar-agenda.do?semestre=' + semestre + '&ano=' + ano)
      .then(function(response) {
        if (!response.ok) {
          throw new Error("HTTP error, status = " + response.status);
        }
        return response.json();
      })
      .then (function(json) {
        diaanterior = document.getElementById("diaTemplate");
        diadasemanaanterior = document.getElementById("diadasemanaTemplate"); 
        for (var i = 0; i < json.atividadesagendadas.length; i++) {
          var novadata = new Date();
          var novodia = '';
          var novaatividade;
          var novoorientador;
          var novohorario;
          var horario = '';
          var horainicio;
          var minutoinicio;
          var horatermino;
          var minutotermino;
          var idLink;
          var novoid;
          novoid = json.atividadesagendadas[i].id;
          novadata.setTime(json.atividadesagendadas[i].inicio);
          novodia += novadata.getDate();
          novodia += '-';
          novodia += novadata.getMonth() + 1;
          novalinha = linha.cloneNode(true);
          novalinha.style.display = '';
          dia = novalinha.querySelector('#diaTemplate');
          diadasemana = novalinha.querySelector('#diadasemanaTemplate');
          novaatividade = novalinha.querySelector('#atividadeTemplate');
          novoorientador = novalinha.querySelector('#orientadorTemplate');
          novohorario = novalinha.querySelector('#horarioTemplate');
          idLink = novalinha.querySelector('#atividadelinkTemplate');
          idLink.href = 'visualizar-atividade.html?id=' + novoid;
          dia.textContent = novodia;
          switch (novadata.getDay()) {
            case 0:
              diadasemana.textContent = 'domingo';
              break;
            case 1:
              diadasemana.textContent = 'segunda-feira';
              break;
            case 2:
              diadasemana.textContent = 'ter�a-feira';
              break;
            case 3:
              diadasemana.textContent = 'quarta-feira';
              break;
            case 4:
              diadasemana.textContent = 'quinta-feira';
              break;
            case 5:
              diadasemana.textContent = 'sexta-feira';
              break;
            case 6:
              diadasemana.textContent = 'sabado';
          }
          novaatividade.textContent = json.atividadesagendadas[i].atividade;
          novoorientador.textContent = json.atividadesagendadas[i].orientador;
          d = new Date();
          d.setTime(json.atividadesagendadas[i].inicio);
          horainicio = d.getHours();
          minutoinicio = d.getMinutes();
          d.setTime(json.atividadesagendadas[i].termino);
          horatermino = d.getHours();
          minutotermino = d.getMinutes();
          horario += horainicio + ':';
          if (minutoinicio < 10) {
            horario += '0';            
          }
          horario += minutoinicio;
          horario += ' at� ';
          horario += horatermino + ':';
          if (minutotermino < 10) {
            horario += '0';            
          }
          horario += minutotermino;
          novohorario.textContent = horario;
          // para mesclar as c�lulas
          if (dia.textContent === diaanterior.textContent) {
            diaanterior.setAttribute('rowspan', 
                          Number.parseInt(
                            diaanterior.getAttribute('rowspan')) + 1);
            diadasemanaanterior.setAttribute('rowspan', 
                          Number.parseInt(
                            diadasemanaanterior.getAttribute('rowspan')) + 1);
                    
            dia.style.display = 'none';
            diadasemana.style.display = 'none';
            diadasemana = diadasemanaanterior;
            dia = diaanterior;
            
          }
          diaanterior = dia;
          diadasemanaanterior = diadasemana;
          if (json.atividadesagendadas[i].id === linhaComDestaque) {
            novalinha.style.border = '3px solid skyblue';            
          }
          tabela.appendChild(novalinha);
        }
      })
      .catch (function(error) {
        console.log(error.message);
      });
      
    </script>
    <img id="up" src="up.png" 
         onmouseover="highLightUp(this)"
         onmouseout="outUp(this)"
         onclick="toTop()"/>
    <script>
      
      function highLightUp(e){
        e.src = "up-selected.png";
      }
      
      function outUp(e){
        e.src = "up.png";
      }
      
      function toTop() {
        window.scrollTo(0, 0);
      }
      
    </script>
  </body>
</html>
