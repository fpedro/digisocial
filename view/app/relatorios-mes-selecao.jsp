<%@page import="java.util.GregorianCalendar" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTY:PE html>
<html>
  <head>
    <title>Relatórios</title>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../style.css" />
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<script src="script.js" ></script>
  <h1>Relatório Geral do Mês</h1>
  <label for="mes_select" >Mês: </label>
  <select id="mes_select" >
    <option value="1" >Janeiro</option>
    <option value="2" >Fevereiro</option>
    <option value="3" >Março</option>
    <option value="4" >Abril</option>
    <option value="5" >Maio</option>
    <option value="6" >Junho</option>
    <option value="7" >Julho</option>
    <option value="8" >Agosto</option>
    <option value="9" >Setembro</option>
    <option value="10" >Outubro</option>
    <option value="11" >Novembro</option>
    <option value="12" >Dezembro</option>
  </select>
  <label for="ano_input" >Ano: </label>
  <input id="ano_input" type="text" style="width: 51px"/>
  <input type="button" class="operationInput" value="Visualizar" onclick="visualizarRelatorio()"/>
  <script>

    <% GregorianCalendar dataAtual = new GregorianCalendar(); %>

      mes_select.value = <%= dataAtual.get(GregorianCalendar.MONTH) + 1 %>;
      ano_input.value = <%= dataAtual.get(GregorianCalendar.YEAR) %>;

      function visualizarRelatorio() {
        var mes;
        var ano;
        mes = mes_select.value;
        ano = ano_input.value;
        document.location = 'relatorio-geral-mes.jsp?mes=' +
                                mes + '&ano=' + ano;
      }
   </script>
 <%@include file="template-p2.jsp"%>
  </body>
</html>
<!-- vi: set sts=2 ts=2 sw=2 et fenc=utf-8 ff=dos: -->
