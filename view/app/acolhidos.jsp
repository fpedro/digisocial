<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Acolhidos</title>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../style.css" />
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<div class="paneOption" >
  ● <a style="text-decoration: none"
       href="<%=contextPath%>/app/cadastrar-acolhido.jsp" >
    <span class="optionsButton" >Cadastrar acolhido</span></a>
</div>
<div class="paneOption" >
  ● <a style="text-decoration: none"
       href="<%=contextPath%>/app/visualizar-acolhidos.jsp" >
    <span class="optionsButton" >
      Visualizar acolhidos atendidos</span></a>
</div>
<div class="paneOption" >
  ● <a style="text-decoration: none"
       href="<%=contextPath%>/app/reinsercao.jsp" >
    <span class="optionsButton" >Reinserir acolhido</span></a>
</div>
<div class="paneOption" >
  ● <a style="text-decoration: none"
       href="<%=contextPath%>/app/desligamento.jsp" >
    <span class="optionsButton" >Saída de acolhido</span></a>
</div>
<%@include file="template-p2.jsp"%>
    <script src="script.js" ></script>
  </body>
</html>
