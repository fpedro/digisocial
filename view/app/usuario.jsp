<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Usuário</title>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../style.css" />
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<div class="paneOption" >
  ● <span class="optionsButton" 
          style="clear: both" onclick="encerrarSessao()" >
    Encerrar sessão</span>
</div>
<script>
   
   function encerrarSessao() {
      fetch('sessao.do?operation=MODIFY&logout')
        .then(function (response) {
           if (!response.ok) {
              throw new Error("HTTP error, status = " + response.status);
           }
        return response.json();
      })
      .then(function (json) {
         window.location = '/'; 
      });
   }
   
</script>
<%@include file="template-p2.jsp"%>
    <script src="script.js" ></script>
  </body>
</html>
<!-- vi: set sts=2 ts=2 sw=2 et fenc=utf-8 ff=dos: -->