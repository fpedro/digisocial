<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <title>CrianÃ§as</title>
        <meta charset="UTF-8" >
        <meta name="viewport" content="width=device-width, initial-scale=1.0" >
        <link rel="stylesheet" href="style.css" />
        <script>

            var dataAtual = new Date();
            var anoAtual;
            var mesAtual;

            anoAtual = dataAtual.getFullYear();
            mesAtual = dataAtual.getMonth();

        </script>
    </head>
    <body>
        <div id="flexContainer" >
            <div style="background-color: skyblue; padding: 0; margin: 0;
                 margin-right: 5px" >
                <img id="logo" src="logo-min.png" alt="Luminosa" />
                <div id="menu" style="padding: 0; margin: 0;
                     background-color: skyblue;" >
                    <a class="menuLink" href="acolhidos.html" >
                        <div class="menuItem" >
                            <span style="margin: auto; font-weight: bolder" >CRIANÃAS</span>
                        </div></a>
                    <a class="menuLink" href="orientadores.html" >
                        <div class="menuItem" >
                            <span style="margin: auto; font-weight: bolder" >
                                ORIENTADORES</span>
                        </div></a>
                    <a class="menuLink" href="atividades.html" >
                        <div class="menuItem" >
                            <span style="margin: auto; font-weight: bolder" >ATIVIDADES</span>
                        </div></a>
                    <a class="menuLink" href="agenda.html" >
                        <div class="menuItem" >
                            <span style="margin: auto; font-weight: bolder" >AGENDA</span>
                        </div></a>
                    <a class="menuLink" href="objetivos-resultados.html" >
                        <div class="menuItem" style="height: 50px;" >
                            <span style="margin: auto; font-weight: bolder" >OBJETIVOS E<br />
                                &nbsp;&nbsp;RESULTADOS
                            </span>
                        </div></a>
                    <a class="menuLink" href="relatorios.html" >
                        <div class="menuItem" >
                            <span style="margin: auto; font-weight: bolder" >
                                RELATÃRIOS</span>
                        </div></a>
                    <a class="menuLink" href="analise.html" >
                        <div class="menuItem" style="height: 50px;" >
                            <span style="margin: auto; font-weight: bolder" >ANÃLISE<br />
                                &nbsp;&nbsp;DE DADOS
                            </span>
                        </div></a>
                </div>
            </div>
            <div style="background-color: skyblue; padding: 0; width: 100%" >
                <div style="margin: 0; margin-bottom: 10px; background-color: skyblue;" >
                    <input id="queryInput"
                           type="text" 
                           style="width: 565px"    
                           placeholder="Nome da crianÃ§a ou CPF do responsÃ¡vel"
                           oninput="query()"
                           >
                    <span class="optionsButton" style="margin-right: 5px; float: right" >
                        Pedro de Jesus</span>
                </div>
                <div style="padding: 0px;
                     background-color: white; margin: 0;
                     width: 100%;" > 
                    <div style="padding: 10px;
                         background-color: white; margin: 0px;" >
                        <div id="contentPane" style="display: block; min-height: 800px;" >
                          <h1>RelatÃ³rio Geral do Ano</h1>
                          <label for="anoINPUT" >Ano: </label>
                          <input id="anoINPUT" type="text" style="width: 51px"/>
                          <input type="button" class="operationInput" value="Visualizar"
                                 onclick="visualizarRelatorio()"/>
                        </div>
                        <div id="resultPane" style="display: none; min-height: 800px" >
                            <div>40 resultados<div>
                                    <div class="queryResult" 
                                         style="display: inline-block;
                                         border: 3px solid yellow" >
                                        <div style="display: flex;
                                             background-color: transparent;
                                             justify-content: center" >
                                            <img class="queryImage"
                                                 src="foto-a.png" alt="foto"/></div>
                                        <div style="background-color: transparent" >Maria Aparecida</div>
                                        <div style="background-color: transparent" >456.654.454-44</div>
                                        <div style="background-color: transparent" >654.456.654-55</div>
                                    </div>  
                                    <div class="queryResult"
                                         style="display: inline-block; border: 3px solid skyblue" >
                                        <div style="display: flex;
                                             background-color: transparent;
                                             justify-content: center" >
                                            <img class="queryImage"
                                                 src="foto-b.png" alt="foto"/></div>
                                        <div style="background-color: transparent" >Mariana Paz</div>
                                        <div style="background-color: transparent" >123.321.121-11</div>
                                        <div style="background-color: transparent" >321.123.121-22</div>
                                    </div>
                                    <div class="queryResult"
                                         style="display: inline-block; border: 3px solid blue" >
                                        <div style="display: flex;
                                             background-color: transparent;
                                             justify-content: center" >
                                            <img class="queryImage"
                                                 src="foto-b.png" alt="foto"/></div>
                                        <div style="background-color: transparent" >
                                            Marcos dos Santos</div>
                                        <div style="background-color: transparent" >789.987.789-77</div>
                                        <div style="background-color: transparent" >987.789.987-88</div>
                                    </div>
                                    <div class="queryResult"
                                         style="display: inline-block; border: 3px solid gainsboro" >
                                        <div style="display: flex;
                                             background-color: transparent;
                                             justify-content: center" >
                                            <img class="queryImage"
                                                 src="foto-a.png" alt="foto"/></div>
                                        <div style="background-color: transparent" >
                                            Marcos Paz</div>
                                        <div style="background-color: transparent" >123.321.121-11</div>
                                        <div style="background-color: transparent" >321.123.121-22</div>
                                    </div>
                                </div>            
                            </div>
                        </div>  
                    </div>
                </div>
            </div>
        </div>
      <script>
      anoINPUT = document.getElementById('anoINPUT');

      anoINPUT.value = anoAtual;

      function visualizarRelatorio() {
        var ano;
        ano = anoINPUT.value;
        document.location = 'relatorio-geral-ano.jsp?ano=' + ano;
      }
    </script>
    <script src="script.js" ></script>
  </body>
</html>
