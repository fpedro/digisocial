<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Análise de idades</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <script>

      var mesInicio, anoInicio;
      var mesTermino, anoTermino;
      var mesAtual, anoAtual;

      var data = new Date();

      mesAtual = data.getMonth();
      anoAtual = data.getFullYear();

    </script>
    <style>

      * {
        font-size: 14pt;
        font-family: sans-serif;
      }


      body {
        margin: 0;
        background-color: skyblue;
      }

      #analise {
        margin: 10px;
        padding: 5px;
        background-color: white;
      }

      #atendimentoChart {
        display: block;
      }

      .operationInput {
        font-weight: bold;
        height: 40px;
      }

    </style>
  </head>
  <body>
    <div id="analise"  style="display: inline-block; margin: 30px" >
      <div style="margin: 10px" >
        <div style="margin: 5px; margin-left: 0px" >
          <span>INÍCIO</span>
        </div>
        <label for="mesInicio" >Mês: </label>
        <select id="mesInicioSELECT" name="mesInicio" >
          <option value="12" >Janeiro</option>
          <option value="1" >Fevereiro</option>
          <option value="2" >Março</option>
          <option value="3" >Abril</option>
          <option value="4" >Maio</option>
          <option value="5" >Junho</option>
          <option value="6" >Julho</option>
          <option value="7" >Agosto</option>
          <option value="8" >Setembro</option>
          <option value="9" >Outubro</option>
          <option value="10" >Novembro</option>
          <option value="11" >Dezembro</option>
        </select>
        <label for="anoInicio" >Ano: </label>
        <input id="anoInicioINPUT" name="anoInicio" type="text"
               style="width: 51px" value="2019"/>
        <div style="margin: 5px; margin-left: 0px" >
          <span>TÉRMINO</span>
        </div>
        <label for="mesTermino" >Mês: </label>
        <select id="mesTerminoSELECT" name="mesInicio" >
          <option value="12" >Janeiro</option>
          <option value="1" >Fevereiro</option>
          <option value="2" >Março</option>
          <option value="3" >Abril</option>
          <option value="4" >Maio</option>
          <option value="5" >Junho</option>
          <option value="6" >Julho</option>
          <option value="7" >Agosto</option>
          <option value="8" >Setembro</option>
          <option value="9" >Outubro</option>
          <option value="10" >Novembro</option>
          <option value="11" >Dezembro</option>
        </select>
        <label for="anoTermino" >Ano: </label>
        <input id="anoTerminoINPUT" name="anoTermino" type="text"
               style="width: 51px" value="2019"/>

        <input type="button" class="operationInput" value="Visualizar" 
               onclick="visualizarDados()" autofocus/>
      </div>

      <div style="padding: 10px; min-width: 100px; min-height: 100px;
           max-width: 1100px; max-height: 500px" >
        <canvas id="idadesChart" width="1000" height="400" ></canvas>
      </div>
    </div>
  </body>
  <script src="Chart.bundle.min.js" ></script>
  <script>

    var dados;
    var ctx = document.getElementById('idadesChart').getContext('2d');
    var lineChart;
    var apresentarDadosDoAno;

    mesInicioSELECT = document.getElementById('mesInicioSELECT');
    anoInicioINPUT = document.getElementById('anoInicioINPUT');
    mesTerminoSELECT = document.getElementById('mesTerminoSELECT');
    anoTerminoINPUT = document.getElementById('anoTerminoINPUT');
    optionsFORM = document.getElementById('optionsFORM');

    mesInicioSELECT.value = mesAtual === 1? 12: mesAtual ;
    anoInicioINPUT.value = anoAtual - 1;

    mesTerminoSELECT.value = mesAtual === 1? 12: mesAtual;
    anoTerminoINPUT.value = anoAtual;

    dados = {
     "labels": [],
     "datasets": [{
         "label": "Menos de 8 anos",
         "data": [],

         "fill": false,
         "borderColor": "blue",
         "lineTension": 0.1
       },
       {
         "label": "Com 8 anos até menos de 10 anos",
         "data": [],
         "fill": false,
         "borderColor": "yellow",
         "lineTension": 0.1
       },

       {
         "label": "Com 10 anos ou mais",
         "data": [],
         "fill": false,
         "borderColor": "rgb(100, 100, 100)",
         "lineTension": 0.1
       }
     ]
    };

    var options = {
     "scales": {
       "yAxes": [{
           "ticks": {
             "beginAtZero": true
           }
         }]
     }
    };

    function visualizarDados() {
      var anoLimite;
      var mesLimite;
      mesInicio = mesInicioSELECT.value;
      anoInicio = anoInicioINPUT.value;
      mesTermino = mesTerminoSELECT.value;
      anoTermino = anoTerminoINPUT.value;

      if (mesInicio === '12')
        anoInicio--;
      if (mesTermino === '12')
        anoTermino--;

      if (mesAtual === 1) {
        mesLimite = 12;
        anoLimite = anoAtual -1;
      } else {
        mesLimite = mesAtual -1;
        anoLimite = anoAtual;
      }

      mesInicio = Number.parseInt(mesInicio);
      anoInicio = Number.parseInt(anoInicio);
      mesTermino = Number.parseInt(mesTermino);
      anoTermino = Number.parseInt(anoTermino);
      
      if (anoTermino > anoLimite ||
           anoTermino === anoLimite && mesTermino > mesLimite) {
        mesTermino = mesLimite;
        anoTermino = anoLimite;
      }
      formData = new FormData();
      formData.append('mesInicio', mesInicio);
      formData.append('anoInicio', anoInicio);
      formData.append('mesTermino', mesTermino);
      formData.append('anoTermino', anoTermino);
      fetch('dados-periodo.do', {
        method: 'POST',
        body: formData
      }).then(function (response) {
       if (!response.ok)
         throw new Error("Http error: " + response.status);
       return response.json();
      }).then(function (json) {
         apresentarDadosDoAno = false;
         dados.labels = [];
         dados.datasets[0].data = [];
         dados.datasets[1].data = [];
         dados.datasets[2].data = [];
         if (json.dadosDoPeriodo.length > 15)
           apresentarDadosDoAno = true;
         for (var i = 0; i < json.dadosDoPeriodo.length; i++) {
           switch (json.dadosDoPeriodo[i].mes) {
             case 2:
               if (apresentarDadosDoAno)
                 dados.labels.push(["Fevereiro", String(
                           json.dadosDoPeriodo[i].ano)]);
               else
                 dados.labels.push("Fevereiro");
               break;
             case 3:
               if (apresentarDadosDoAno)
                 dados.labels.push(["Março",
                        String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Março");
            break;
          case 4:
            if (apresentarDadosDoAno)
              dados.labels.push(["Abril",
                String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Abril");
            break;
          case 5:
            if (apresentarDadosDoAno)
              dados.labels.push(["Maio", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Maio");
            break;
          case 6:
            if (apresentarDadosDoAno)
              dados.labels.push(["Junho", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Junho");
            break;
          case 7:
            if (apresentarDadosDoAno)
              dados.labels.push(["Julho", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Julho");
            break;
          case 8:
            if (apresentarDadosDoAno)
              dados.labels.push(["Agosto", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Agosto");
            break;
          case 9:
            if (apresentarDadosDoAno)
              dados.labels.push(["Setembro", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Setembro");
            break;
          case 10:
            if (apresentarDadosDoAno)
              dados.labels.push(["Outubro", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Outubro");
            break;
          case 11:
            if (apresentarDadosDoAno)
              dados.labels.push(["Novembro", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Novembro");
            break;
          case 12:
            if (apresentarDadosDoAno)
              dados.labels.push(["Dezembro", String(json.dadosDoPeriodo[i].ano)]);
            else
              dados.labels.push("Dezembro");
            break;
          case 1:
            if (apresentarDadosDoAno)
              dados.labels.push(["Janeiro",
                String(json.dadosDoPeriodo[i].ano + 1)]);
            else
              dados.labels.push(["Janeiro",
                String(json.dadosDoPeriodo[i].ano + 1) + " -->"]);
            break;
        }
        dados.datasets[0].data.push(json.dadosDoPeriodo[i]
                .acolhidosComMenosDe8Anos);
        dados.datasets[1].data.push(json.dadosDoPeriodo[i]
                .acolhidosCom8AnosAteMenosDe10Anos);
        dados.datasets[2].data.push(json.dadosDoPeriodo[i]
                .acolhidosCom10AnosOuMais);
      }
      lineChart = new Chart(ctx, {
        type: 'line',
        data: dados,
        options: options
      });
    }).catch(function (error) {
      console.log(error.message);
    });
    }
    visualizarDados();
  </script>
</html>
