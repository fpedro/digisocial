<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Visualizar dados do acolhido</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <link rel="stylesheet" href="../style.css" />
    <script>
        let params = (new URL(document.location)).searchParams;
        let id = params.get('id');
    </script>
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<script src="script.js" ></script>
<form id="acolhido_form" style="width: 790px" >
  <h1>Visualizar dados do acolhido</h1>
  <div id="message_div" ></div>
  <div>
    <canvas id="photo_canvas" width="100" height="115"
            style="float: left; margin: 5px;
                   border: 3px solid skyblue;" ></canvas>
    <div style="margin-top: 10px; float: left" >
      <span>Nome: </span>
      <span id="nomeAcolhido_span" ></span>
    <div id="reinserir_div" style="display: none; clear: both" >
      <span class="optionsButton" onclick="redirect_to_reinserir()" >
        Reinserir no projeto...</span>  
    </div>  
    </div>
    <div style="display: block; clear: both" >
      <label for="foto_input" class="optionsButton" >
        Pegar foto...</label>  
    </div>  
    <input id="foto_input" type="file" style="opacity: 0"/>
  </div>
  <div>
    <span>Data de nascimento: </span>
    <span id="diaNascimento_span" ></span> / 
    <span id="mesNascimento_span" ></span> /
    <span id="anoNascimento_span" ></span>&nbsp;&nbsp;
    <span>Sexo: </span><span id="sexo_span" ></span>
  </div>
  <div style="margin-top: 10px" >
    DADOS DA MÃE
  </div>
  <div>
    <span>Nome: </span>
    <span id="nomeMae_span" ></span>
  </div>  
  <div>
    <span>RG: </span>
    <span id="RGmae_span" ></span>&nbsp;&nbsp;
    <span>CPF: </span>
    <span id="CPFmae_p1_span" ></span> . 
    <span id="CPFmae_p2_span" ></span> . 
    <span id="CPFmae_p3_span" ></span> - 
    <span id="CPFmae_p4_span" ></span>
  </div>
  <div>
    <span>Local de trabalho: </span>
    <input id="localTrabalhoMae_input" name="localTrabalhoMae"
           maxlength="60" type="text"
           style="width: 600px" required />
  </div>
  <div style="margin-top: 10px" >
    DADOS DO PAI
  </div>
  <div>
    <span>Nome: </span>
    <span id="nomePai_span" ></span>
  </div>  
  <div>
    <span>RG: </span>
    <span id="RGpai_span" ></span>&nbsp;&nbsp;
    <span>CPF: </span>
    <span id="CPFpai_p1_span" ></span> . 
    <span id="CPFpai_p2_span" ></span> . 
    <span id="CPFpai_p3_span" ></span> - 
    <span id="CPFpai_p4_span" ></span>
  </div>
  <div>
    <span>Local de trabalho: </span>
    <input id="localTrabalhoPai_input" name="localTrabalhoPai"
           maxlength="60" type="text"
           style="width: 600px" required />
  </div>
  <div>ENDEREÇO</div>
  <div>
    <span>Rua: </span>
    <input id="ruaEndereco_input" name="endereco.rua" type="text"
           maxlength="60" required />
    <span>N°: </span>
    <input id="numeroEndereco_input"
           oninput="validateNumber(this)"
           type="text" style="width: 55px" required />
    <div>
    <span>Complemento: </span>
      <input id="complementoEndereco_input"
             name="endereco.complemento" maxlength="30" 
             type="text" style="width: 170px"/>
    </div>
  </div>
  <div>
    <span>Bairro: </span>
    <select id="bairro_select" 
            onchange="bairro_changed()" required >
      <option>Centro</option>
      <option>Ipiranga</option>
      <option>Nogueira</option>
      <option>Itapema</option>
      <option>Freguesia</option>
      <option>Maracatu</option>
      <option>Itaoca</option>
      <option>Outro</option>
    </select>
    <span class="disabledLabel" 
          id="qualBairro_label" >Qual?</span>
    <input type="text"  maxlength="30"
           id="qualBairro_input" style="width: 335px"/>
  </div>  
  <div>                  
    <span>Cidade: </span>
    <input id="cidadeEndereco_input" name="endereco.cidade"
           type="text" maxlength="30" style="width: 300px"
           value="Guararema" required />
  </div>
  <div>
    <span>Volta para casa sozinho:</span>
    <select id="voltaSozinho_select" 
            onchange="voltaSozinho_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <div>
    <span class="disabledLabel" 
          id="comQuem_label" >Com quem?</span>
    <input type="text"  maxlength="50"
           id="comQuem_input" style="width: 390px"/>
    </div>
  </div>
  <div>TELEFONE</div>
  <div>
    <span>Residencial: </span>
    <input id="telefoneResidencial_input"
           name="telefoneResidencial" maxlength="16"
           type="text" style="width: 140px" required />
    <span>Celular: </span>
    <input id="telefoneCelular_input"
           name="telefoneCelular" maxlength="16"
           type="text" style="width: 140px" required />
  </div>
  <div>DADOS DO PROJETO</div>
  <div>
    <span>Perí­odo:</span>
    <select id="periodoPS_select" name="periodoPS" required >
      <option value="MANHA" >MANHÃ</option>
      <option>TARDE</option>
    </select>
  </div>
  <div>
    <span>Já esteve no projeto?</span>
    <span id="esteveNoProjeto_span" ></span>&nbsp;&nbsp;
    <span id="quantasVezes_label" >Quantas vezes? </span>
    <span id="quantasVezes_span" ></span>
  </div>
  <div>
    <span>Atendimento Prioritário: </span>
    <select id="atendimentoPrioritario_select"
            name="atendimentoPrioritario" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
  </div>
  <div>DADOS DA ESCOLA</div>
  <div>
    <span>Escola: </span>
    <select id="escola_select"
            onchange="escola_changed()" required >
      <option value="IB" >E. E. Ivan Brasil</option>
      <option value="GV" >E. E. Getúlio Vargas</option>
      <option value="CL" >E. M. Célia Leonor</option>
      <option value="RF" >E. E. Roberto Feijó</option>
      <option>Outra</option>
    </select>
    <span class="disabledLabel" 
          id="qualEscola_label" >Qual?</span>
    <input type="text" maxlength="30"
           id="qualEscola_input" style="width: 335px" required />
  </div>
  <div>
    <span>Perí­odo: </span>
    <select id="periodoEscolar_select"
            name="escola.periodo" required >
      <option value="MANHA" >MANHÃ</option>
      <option>TARDE</option>
    </select>
    <span>Ano: </span>
    <select id="anoEscolar_select"
            name="escola.ano" required >                  
      <option value="1" >1°</option>
      <option value="2" >2°</option>
      <option value="3" >3°</option>
      <option value="4" >4°</option>
      <option value="5" >5°</option>
      <option value="6" >6°</option>
      <option value="7" >7°</option>
      <option value="8" >8°</option>
    </select>  
  </div>
  <div>DADOS PESSOAIS E DA FAMÍLIA</div>
  <div>
    <span>Possui alguma deficiência?</span>
    <select id="deficiencia_select"
            onchange="deficiencia_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <span class="disabledLabel" 
          id="qualDeficiencia_label" >Qual? </span>
    <input id="qualDeficiencia_input" maxlength="60"
           type="text" style="width: 380px" required />
  </div>
  <div>
    <span>Tem alergia?</span>
    <select id="alergia_select"
            onchange="alergia_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <span class="disabledLabel"
          id="qualAlergia_label" >Qual? </span>
    <input id="qualAlergia_input" maxlength="60"
           type="text" style="width: 490px" required />
  </div>
  <div>
    <span>Toma medicamento?</span>
    <select id="medicamento_select"
            onchange="medicamento_changed()" required >
      <option>SIM</option>
      <option>NÃO</option>
    </select>
    <span class="disabledLabel"
          id="qualMedicamento_label" >Qual? </span>
    <input id="qualMedicamento_input" maxlength="60"
           type="text" style="width: 430px" required />
  </div>
  <div>
    <span>Recebe bolsa famí­lia?</span>
    <select id="bolsaFamilia_select"
            name="bolsaFamilia" required >
      <option value="true" >SIM</option>
      <option value="false" >NÃO</option>
    </select>
  </div>
  <div>FAMÍLIARES QUE MORAM COM O ACOLHIDO</div>
  <div id="familiares" >
    <div id="familiarTemplate" style="display: none" >
      <div>
        ● <span>Nome: </span>
        <span id="nomeFamiliar_span" ></span>
      </div>
      <div>
        &nbsp;&nbsp;&nbsp;&nbsp;<span>Idade: </span>
        <span id="idadeFamiliar_span" ></span>&nbsp;&nbsp;
        <span>Parentesco: </span>
        <span id="parentescoFamiliar_span" ></span>&nbsp;&nbsp;
        <span>Ocupação: </span>
        <span id="ocupacaoFamiliar_span" ></span>&nbsp;&nbsp;
      </div>
    </div>
  </div>
  <div>
    <span>O acolhido já teve alguma doença grave quando pequeno?
      </span>
    <span id="doencaGrave_span"
          style="width: 600px; height: 80px; clear: both" ></span>
  </div> 
  <div>
    <span>Já fez algum tratamento? Quanto tempo?</span>
    <div style="clear: both" >
      <div>
        <input id="neurologista_checkbox" type="checkbox"
               onchange="neurologista_changed()" disabled />
        <label for="neurologista_checkbox" >Neurologista
                                             &nbsp;&nbsp;</label>
        <span id="neurologista_span" ></span>
      </div>
      <div>
        <input id="psiquiatra_checkbox" type="checkbox"
               onchange="psiquiatra_changed()" disabled />
        <label for="psiquiatra" >Psiquiatra&nbsp;&nbsp;</label>
        <span id="psiquiatra_span" ></span>
      </div>
      <div>
        <input id="psicologo_checkbox" type="checkbox"
               onchange="psicologo_changed()" disabled />
        <label for="psicologo" >Psicólogo&nbsp;&nbsp;</label>
        <span id="psicologo_span" ></span>
      </div>
      <div>
        <input id="psicopedagogo_checkbox" type="checkbox"
               onchange="psicopedagogo_changed()" disabled />
        <label for="psicopedagogo_checkbox" >Psicopedagogo
                                              &nbsp;&nbsp;
        </label>
        <span id="psicopedagogo_span" ></span>
      </div>
      <div>
        <input id="fonoaudiologo_checkbox" type="checkbox"
               onchange="fonoaudiologo_changed()" disabled />
        <label for="fonoaudiologo_checkbox" >Fonoaudiólogo
                                              &nbsp;&nbsp;
        </label>
        <span id="fonoaudiologo_span" ></span>
      </div>
      <div>
        <input id="outrosTratamentos_checkbox" type="checkbox"
               onchange="outrosTratamentos_changed()" disabled />
        <label for="outrosTratamentos" >Outros&nbsp;&nbsp;
        </label>
        <span id="outrosTratamentos_span" ></span>
      </div>
    </div>
  </div> 
  <div>
    <span style="display: block" >
      Na famí­lia tem algum caso de diabetes,</span>
    <span style="display: block" >
      &nbsp;&nbsp;problemas do coração, transtorno mental,
    </span>
    <span style="display: block" >
      &nbsp;&nbsp;alcoolismo, drogas, etc. ?
    </span>
    <span id="problemaNaFamilia_span"
          style="width: 600px; height: 80px; clear: both" ></span>
  </div>
  <div style="min-height: 10px" ></div>
  <div>
    <span style="display: block" >
      O que o acolhido gosta de fazer em casa?</span>
    <span id="gostaDeFazer_span"
          style="width: 600px; height: 80px; clear: both" ></span>
  </div>
  <div style="min-height: 10px" ></div>
  <div>
    <span style="display: block" >
      Como o acolhido se relaciona com a família?</span>
    <span id="relacionamentoComFamilia_span"
          style="width: 600px; height: 80px; clear: both" ></span>
  </div>
  <div>
    <span>
      Cadastro realizado por: </span>
    <span id="usuarioNoCadastro_span"
          style="width: 600px; height: 80px;" ></span>
  </div>
  <div>
    <span>
      Data e hora do Cadastro: </span>
    <span id="dataCadastro_span"
          style="width: 600px; height: 80px;" ></span>
  </div>
  <div>
    <span>
      Id do usuário que realizou o cadastro: </span>
    <span id="idUsuarioNoCadastro_span"
          style="width: 600px; height: 80px; clear: both" ></span>
  </div>
  <div style="float: right" >
    <div><span id="info_span" class="infoMessage" ></span></div>
    <input id="salvarAlteracoes_button" type="button"
           class="operationControl" style="align-self: flex-end"
           value="Salvar alterações"
           onclick="alteracao_executer()"/>
  </div>
  <div style="clear: both" ></div>
  <input type="hidden" name="operation" value="MODIFY" />
</form>
<script>

  acolhido_form.reset();

  qualBairro_input.disabled = true;
  comQuem_input.disabled = true;
  qualEscola_input.disabled = true;
  qualDeficiencia_input.disabled = true;  
  qualAlergia_input.disabled = true;  
  qualMedicamento_input.disabled = true;

  var foto_img = document.createElement('img');
  var photoSelected = false;
  var operacaoReinserir = false;
  var currentPhoto = null;

  var ctx = photo_canvas.getContext('2d');

  foto_img.src = 'sem-foto.png';
  foto_input.addEventListener('change', updateFoto);

  foto_img.addEventListener('load', e => {
     ctx.drawImage(foto_img, 0, 0, 100, 115);
  });

  function updateFoto() {
     var curFoto = foto_input.files[0];
     foto_img.src = window.URL.createObjectURL(curFoto);
     photoSelected = true;
  }


  fetch('acolhido.do?id=' + id, {
          method: 'GET'
  }).then(function (response) {
      if (!response.ok)
         throw new Error("HTTP error, status = " + response.status);
      return response.json();
   })
   .then(function (json) {
     if (json.desligamento === undefined &&
        new Date(json.dataCadastro).getFullYear() < new Date().getFullYear()) {
        reinserir_div.style.display = 'block';
        operacaoReinserir = true;
     }
     nomeAcolhido_span.textContent = json.nomeAcolhido;
     if (json.foto  !== undefined) {
        foto_img.src = json.foto;
        currentPhoto = json.foto;
     }
     dataNascimento = new Date(json.dataNascimento);
     diaNascimento_span.textContent = dataNascimento.getDate();
     mesNascimento_span.textContent = dataNascimento.getMonth() + 1;
     anoNascimento_span.textContent = dataNascimento.getFullYear();
     sexo_span.textContent = json.sexo;
     if (json.nomeMae !== undefined)
        nomeMae_span.textContent = json.nomeMae;
     RGmae_span.textContent = json.RGmae;
     if (json.CPFmae !== undefined) {
        CPF_p1 = parseInt(json.CPFmae / 100000000);
        CPFmae_p1_span.textContent = CPF_p1;
        CPF_p2 = parseInt((json.CPFmae - CPF_p1 * 100000000) / 100000);
        CPFmae_p2_span.textContent = CPF_p2;
        CPF_p3 = parseInt((json.CPFmae - CPF_p1 * 100000000 -
                             CPF_p2 * 100000) / 100);
        CPFmae_p3_span.textContent = CPF_p3;
        CPFmae_p4_span.textContent = json.CPFmae - CPF_p1 * 100000000 -
                                       CPF_p2 * 100000 - CPF_p3 * 100;
     }
     localTrabalhoMae_input.value = json.localTrabalhoMae;
     if (json.nomePai !== undefined)
        nomePai_span.textContent = json.nomePai;
     RGpai_span.textContent = json.RGpai;
     if (json.CPFpai !== undefined) {
        CPF_p1 = parseInt(json.CPFpai / 100000000);
        CPFpai_p1_span.textContent = CPF_p1;
        CPF_p2 = parseInt((json.CPFpai - CPF_p1 * 100000000) / 100000);
        CPFpai_p2_span.textContent = CPF_p2;
        CPF_p3 = parseInt((json.CPFpai - CPF_p1 * 100000000 -
                             CPF_p2 * 100000) / 100);
        CPFpai_p3_span.textContent = CPF_p3;
        CPFpai_p4_span.textContent = json.CPFpai - CPF_p1 * 100000000 -
                                       CPF_p2 * 100000 - CPF_p3 * 100;
     }
     localTrabalhoPai_input.value = json.localTrabalhoPai;
     ruaEndereco_input.value = json.ruaEndereco;
     numeroEndereco_input.value = json.numeroEndereco;
     complementoEndereco_input.value = json.complementoEndereco;
     switch (json.bairroEndereco) {
       case 'Centro':
       case 'Ipiranga':
       case 'Nogueira':
       case 'Itapema':
       case 'Freguesia':
       case 'Maracatu': bairro_select.value = json.bairroEndereco; break;
       default: 
         bairro_select.value = 'Outro'; 
         qualBairro_input.value = json.bairroEndereco;
         bairro_changed();
     }
     if (json.comQuemVoltaParaCasa !== undefined) {
       voltaSozinho_select.value = 'NÃO';
       comQuem_input.value = json.comQuemVoltaParaCasa;
       voltaSozinho_changed();
     }
     else voltaSozinho_select.value = 'SIM';
     telefoneResidencial_input.value = json.telefoneResidencial;
     telefoneCelular_input.value = json.telefoneCelular;
     periodoPS_select.value = json.periodoPS;
     if (json.quantasVezesNoProjeto !== 0) {
       esteveNoProjeto_span.textContent = 'SIM';
       quantasVezes_span.textContent = json.quantasVezesNoProjeto;
     } else esteveNoProjeto_span.textContent = 'NÃO';
     if (json.atendimentoPrioritario)
       atendimentoPrioritario_select.value = 'SIM';
     else atendimentoPrioritario_select.value = 'NÃO';
     switch (json.nomeEscola) {
       case 'IB':
       case 'GV':
       case 'CL':
       case 'RF': escola_select.value = json.nomeEscola; break;
       default: 
         escola_select.value = 'Outra'; 
         qualEscola_input.value = json.nomeEscola;
         escola_changed();
     }
     periodoEscolar_select.value = json.periodoEscola;
     anoEscolar_select.value = json.anoEscola;
     if (json.deficiencia !== undefined) {
       deficiencia_select.value = 'SIM';
       qualDeficiencia_input.value = json.deficiencia;
       deficiencia_changed();
     }
     else deficiencia_select.value = 'NÃO';
     if (json.alergia !== undefined) {
       alergia_select.value = 'SIM';
       qualAlergia_input.value = json.alergia;
       alergia_changed();
     }
     else alergia_select.value = 'NÃO';
     if (json.medicamento !== undefined) {
       medicamento_select.value = 'SIM';
       qualMedicamento_input.value = json.medicamento;
       medicamento_changed();
     }
     else medicamento_select.value = 'NÃO';
     if (json.bolsaFamilia)
       bolsaFamilia_select.value = 'true';
     else bolsaFamilia_select.value = 'false';
     for (i = 0; i < json.familiares.length; i++) {
       n = familiarTemplate.cloneNode(true);
       f = n.querySelector('#nomeFamiliar_span');
       f.textContent = json.familiares[i].nome;
       f = n.querySelector('#idadeFamiliar_span');
       f.textContent = json.familiares[i].idade;
       f = n.querySelector('#parentescoFamiliar_span');
       f.textContent = json.familiares[i].parentesco;
       f = n.querySelector('#ocupacaoFamiliar_span');
       f.textContent = json.familiares[i].ocupacao;
       n.style.display = 'block';
       familiares.appendChild(n);
     }
     doencaGrave_span.textContent = json.doencaGrave;
     if (json.tratamentoNeurologista !== undefined) {
       neurologista_checkbox.checked = true;
       neurologista_span
         .textContent = json.tratamentoNeurologista;
     }
     if (json.tratamentoPsiquiatra !== undefined) {
       psiquiatra_checkbox.checked = true;
       psiquiatra_span
         .textContent = json.tratamentoPsiquiatra;
     }
     if (json.tratamentoPsicologo !== undefined) {
       psicologo_checkbox.checked = true;
       psicologo_span
         .textContent = json.tratamentoPsicologo;
     }
     if (json.tratamentoPsicopedagogo !== undefined) {
       psicopedagogo_checkbox.checked = true;
       psicopedagogo_span
         .textContent = json.tratamentoPsicopedagogo;
     }
     if (json.tratamentoFonoaudiologo !== undefined) {
       fonoaudiologo_checkbox.checked = true;
       fonoaudiologo_span
         .textContent = json.tratamentoFonoaudiologo;
     }
     if (json.tratamentoOutros !== undefined) {
       outrosTratamentos_checkbox.checked = true;
       outrosTratamentos_span
         .textContent = json.tratamentoOutros;
     }
     problemaNaFamilia_span.textContent = json.problemaNaFamilia;
     gostaDeFazer_span.textContent = json.gostaDeFazer;
     relacionamentoComFamilia_span
       .textContent = json.relacionamentoComFamilia;
     usuarioNoCadastro_span
       .textContent = json.userOnRegistry.nome;
     idUsuarioNoCadastro_span
       .textContent = json.userOnRegistry.id;
     d = new Date(json.dataCadastro);
     h = d.getHours();
     if (h < 10)
        h = '0' + h;
     m = d.getMinutes();
     if (m < 10)
        m = '0' + m;
     dataCadastro_span
       .textContent = '' + d.getDate() +
                      '-' + (d.getMonth() + 1) +
                      '-' + d.getFullYear() + '  ' +
                      h + ':' +  m;
   })      
   .catch(function (error) {
     console.log(error.message);
 });

 function bairro_changed() {
   if (bairro_select.value === 'Outro')
     enable(qualBairro_label, qualBairro_input);
   else
     disable(qualBairro_label, qualBairro_input);
 }

 function voltaSozinho_changed() {
   if (voltaSozinho_select.value === 'NÃO')
     enable(comQuem_label, comQuem_input);
   else
     disable(comQuem_label, comQuem_input);
 }

 function escola_changed() {
   if (escola_select.value === 'Outra')
     enable(qualEscola_label, qualEscola_input);
   else
     disable(qualEscola_label, qualEscola_input);
 }  

 function deficiencia_changed() {
   if (deficiencia_select.value === 'SIM')
     enable(qualDeficiencia_label, qualDeficiencia_input);
   else
     disable(qualDeficiencia_label, qualDeficiencia_input);
 }  

 function alergia_changed() {
   if (alergia_select.value === 'SIM')
     enable(qualAlergia_label, qualAlergia_input);
   else
     disable(qualAlergia_label, qualAlergia_input);
 }  

 function medicamento_changed() {
   if (medicamento_select.value === 'SIM')
     enable(qualMedicamento_label, qualMedicamento_input);
   else
     disable(qualMedicamento_label, qualMedicamento_input);
 }  

 function enable(label, input) {
   label.className = '';
   input.disabled = false;
   input.required = true;
 }

 function disable(label, input) {
   label.className = 'disabledLabel';
   input.value = '';  
   input.disabled = true;
   input.required = false;
 }

 function redirect_to_reinserir() {
   if (operacaoReinserir)
     window.location = 'reinserir-acolhido.jsp?id=' + id;
 }

 function alteracao_executer() {

   salvarAlteracoes_button.disabled = true;
   info_span.textContent = 'processando...';

   formData = new FormData(acolhido_form);

   formData.set('id', id);

   if (photoSelected)
      formData.set('foto', photo_canvas.toDataURL());
   else if (currentPhoto !== null)
      formData.set('foto', currentPhoto);
    
   localTrabalhoPai_input.required = true;
   localTrabalhoMae_input.required = true;

   if (localTrabalhoMae_input.value !== '')
     localTrabalhoPai_input.required = false;
   else if (localTrabalhoPai_input.value !== '')
     localTrabalhoMae_input.required = false;

   numeroEndereco = numeroEndereco_input.value;
   if (numeroEndereco !== '')
     formData.set('endereco.numero', numeroEndereco);

   bairro = bairro_select.value;
   if (bairro === 'Outro')
     bairro = qualBairro_input.value;
   formData.set('endereco.bairro', bairro);

   if (voltaSozinho_select.value === 'NÃO') {
     comQuem = comQuem_input.value;
     formData.set('comQuemVoltaParaCasa', comQuem);
   }

   telefoneResidencial_input.required = true;
   telefoneCelular_input.required = true;

   if (telefoneResidencial_input.value !== '')
     telefoneCelular_input.required = false;

   if (telefoneCelular_input.value !== '')
     telefoneResidencial_input.required = false;

   escola = escola_select.value;
   if (escola === 'Outra')
     escola = qualEscola_input.value;
   formData.set('escola.nome', escola);

   if (deficiencia_select.value === 'SIM') {
     qualDeficiencia = qualDeficiencia_input.value;
     formData.set('deficiencia', qualDeficiencia);
   }

   if (alergia_select.value === 'SIM') {
     qualAlergia = qualAlergia_input.value;
     formData.set('alergia', qualAlergia);
   }

   if (medicamento_select.value === 'SIM') {
     qualMedicamento = qualMedicamento_input.value;
     formData.set('medicamento', qualMedicamento);
   }

   for (i = 0; i < acolhido_form.elements.length; i++) {
     if (acolhido_form.elements[i].validity.valueMissing)
       acolhido_form.elements[i].classList.add('exceptionField');
     else
       acolhido_form.elements[i].classList.remove('exceptionField');
   }

   fetch('acolhido.do', {
     method: 'POST',
     body: formData
   }).then(function (response) {
       if (!response.ok) {
           throw new Error("HTTP error, status = " + response.status);
       }
       return response.json();
     })
     .then(function (json) {
       if (json.exception === undefined)
         window.location = 'visualizar-acolhidos.jsp?dadosAlterados=' +
                             json.id;
       else {
         while (message_div.hasChildNodes()) {
             message_div.removeChild(message_div.firstChild);
         }
         message_span = document.createElement('span');
         message_span.className = 'exceptionMessage';
         message_span.textContent = json.exception;
         message_div.appendChild(message_span);
         setTimeout(clearMessage, 3000, message_span);
         setTimeout(removeMessage, 5000, message_span);
         window.scroll(0, 0);
         salvarAlteracoes_button.disabled = false;
         info_span.textContent = '';
       }
     })
     .catch(function (error) {
       console.log(error.message);
     });
 }

 function clearMessage(e) {
   window.requestAnimationFrame(function (time) {
     window.requestAnimationFrame(function (time) {
       e.style.opacity = 0; });
   });
 }

 function removeMessage(e) {
   e.style.display = 'none';
 }

</script>
<%@include file="template-p2.jsp"%>
  </body>
</html>
<!-- vi: set sts=2 ts=2 sw=2 et fenc=utf-8: -->
