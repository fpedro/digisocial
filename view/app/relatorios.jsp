<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Relatórios</title>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../style.css" />
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<script src="script.js" ></script>
<div class="paneOption" >
  ● <a style="text-decoration: none"
       href="<%=contextPath%>/app/relatorios-mes-selecao.jsp" >
    <span class="optionsButton" >Relatório mensal</span></a>
</div>
<div style="display: none" class="paneOption" >
  ● <a style="text-decoration: none"
       href="<%=contextPath%>/app/relatorios-ano-selecao.jsp" >
    <span class="optionsButton" >
      Relatório semanal</span></a>
</div>
<div class="paneOption" >
  ● <a style="text-decoration: none"
       href="<%=contextPath%>/app/logs.jsp" >
    <span class="optionsButton" >Logs do sistema</span></a>
</div>
<%@include file="template-p2.jsp"%>
  </body>
</html>
<!-- vi: set sts=2 ts=2 sw=2 et fenc=utf-8 ff=dos: -->
