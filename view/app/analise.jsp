<!DOCTYPE html>
<html>
  <head>
    <title>Luminosa</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <style>
      * {
        font-size: 18pt;
      }

      body {
        margin: 0;
        background-color: skyblue;
      }

      a {
        color: blue;
      }
      
      #flexContainer {
        display: flex;
      }

      #logo {
        margin: 30px;
        width: 180px;
        height: 30px;
      }

    </style>    
  </head>
  <body>
    <div id="flexContainer" >
      <img id="logo" src="logo-min.png" alt="Luminosa" />
      <div style="margin: 30px; padding-top: 10px;
           padding-bottom: 30px; background-color: white" >
        <div style="margin: 30px" >
          <br />
          <div style="display: block; margin-top: 10px" >
            ● <a href="relatorios.html" >Relatórios</a> </div>
          <div style="display: block" >
            ● <a href="analise-idades.html" >Análise de idades</a>
          </div>
          <div style="display: block" >
            ● <a href="analise-atendimento.html" >Análise de atendimento</a>
          </div>
        </div>
      </div>  
    </div>
  </body>
</html>
