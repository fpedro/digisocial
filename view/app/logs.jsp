<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Logs</title>
    <meta charset="UTF-8" name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="style.css" />
  </head>
  <script>

  var mes;
  var ano;
  var dataAtual = new Date();
  const meses = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho',
                   'Julho', 'Agosto', 'Setembro', 'Outubro',
                   'Novembro', 'Dezembro'];
                 
  mes = meses[dataAtual.getMonth()];
  ano = dataAtual.getFullYear();

  ano = Number.parseInt(ano);

  </script>
<%@include file="template-p1.jsp"%>
<a id="mesAnterior" href=""  style="text-decoration: none" class="menuOptions" >
&lt;--</a>
&nbsp;&nbsp;<h1 id="mes_heading" class="menuOptions"
                style="display: inline-block" ></h1>&nbsp;&nbsp;
<a id="mesSeguinte" href=""  style="text-decoration: none" class="menuOptions" >
--&gt;</a>
<table>
  <tbody id="logs_tbody" >
    <tr>
      <th>DATA</th>
      <th>HORÁRIO</th>
      <th>ID DO <br />USUÁRIO</th>
      <th>USUÁRIO</th>
      <th>OPERAÇÃO</th>
    </tr>
    <tr id="row_template" style="display: none" >
      <td id="data_log" ></td>
      <td id="horario_log" ></td>
      <td id="userId_log" ></td>
      <td id="userName_log" ></td>
      <td id="operationDescription_log" ></td>
    </tr>
  </tbody>
</table>
<img id="upButton" src="up.png" alt="Para cima"
     onmouseover="highLightUp(this)"
     onmouseout="outUp(this)"
     onclick="toTop()"/>
<script>

mes_heading.textContent = mes + ' de ' + ano;

  fetch('logs.do')
    .then(function (response) {
      if (!response.ok) {
          throw new Error("HTTP error, status = " + response.status);
      }
      return response.json();
    })
    .then(function (json) {
      console.log(json);
      d = new Date();
      for (i = 0; i < json.logs.length; i++) {
        log_row = row_template.cloneNode(true);
        log_row.querySelector('#userId_log')
          .textContent = json.logs[i].userId;
        d.setTime(json.logs[i].operationDate);
        log_row.querySelector('#data_log')
          .textContent = d.getDate() + '/' + (d.getMonth() + 1) + '/' +
                           d.getFullYear();
        log_row.querySelector('#horario_log')
          .textContent = d.getHours() + ':' + d.getMinutes();
        log_row.querySelector('#userName_log')
          .textContent = json.logs[i].userName;
        log_row.querySelector('#operationDescription_log')
          .textContent = json.logs[i].operationDescription;
        log_row.style.display = 'table-row';
        logs_tbody.appendChild(log_row);
      }
    })
    .catch(function (error) {
      console.log(error.message);
    });

function highLightUp(e){
  e.src = "up-selected.png";
}

function outUp(e){
  e.src = "up.png";
}

function toTop() {
  window.scrollTo(0, 0);
}

</script>
<%@include file="template-p2.jsp"%>