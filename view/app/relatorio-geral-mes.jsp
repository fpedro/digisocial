<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Relatório Geral do Mês</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <script>
      var dataAtual = new Date();
      let params = (new URL(document.location)).searchParams;
      mes = params.get('mes');
      ano = params.get('ano');
      if (mes === null)
        mes = dataAtual.getMonth() + 1;
      if (ano === null)
        ano = dataAtual.getFullYear();
    </script>
    <style>

   @media print {
     
   }

      * {
        font-size: 14pt;
        font-family: sans-serif;
      }

      body {
        margin: 0;
        background-color: skyblue;
      }

      h1 {
        font-size: 18pt;
      }

      span {
        width: 80px;
      }

      table {
        width: 310px;
        border-collapse: collapse;
      }

      td {
        border: 2px solid skyblue;
        padding: 3px;
        text-align: center;
      }

      .result {
        padding: 2px;
        color: blue;
        border: 2px solid skyblue;
      }

      .itemRelatorio {
        display: block;  
        margin: 5px;
        padding: 1px;
      }

      #relatorio {
        display: inline-block;
        margin: 30px;
        padding: 5px;
        background-color: white;
      }

    </style>
  </head>
  <body>
    <div id="relatorio" >
      <div style="display: block" >
        <h1>Relatório Geral do Mês</h1>
        <h3>NOME DO PROJETO SOCIAL</h3>
        <h2 id="mes_h2" ></h2>
      </div>
      <div style="float: left" >
        <div class="itemRelatorio" >
          <span>Total de acolhidos: </span>
          <span id="totalAcolhidos_span" class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>Desligamentos: </span>
          <span id="desligamentos_span" class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>Desligamentos com menos de 1 mês: </span>
          <span id="desligamentosMenosDe1mes_span" class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>IDADES</span>
          <table>
            <tr>
              <td>Menos de 8 anos</td>
              <td id="menosDe8Anos_td" style="color: blue" >0</td>
              <td id="menosDe8AnosPC_td" style="color: blue" >%</td>
            </tr>
            <tr>
              <td>Com 8 anos até menos de 10 anos</td>
              <td id="com8AnosAteMenosDe10Anos_td" style="color: blue" >0</td>
              <td id="com8AnosAteMenosDe10AnosPC_td" style="color: blue" >%</td>
            </tr>
            <tr>
              <td>Com 10 anos até menos de 12 anos</td>
              <td id="com10AnosAteMenosDe12Anos_td" style="color: blue" >0</td>
              <td id="com10AnosAteMenosDe12AnosPC_td" style="color: blue" >%</td>
            </tr>
            <tr>
              <td>Com 12 anos até menos de 14 anos</td>
              <td id="com12AnosAteMenosDe14Anos_td" style="color: blue" >0</td>
              <td id="com12AnosAteMenosDe14AnosPC_td" style="color: blue" >%</td>
            </tr>
            <tr>
              <td>Com 14 anos ou mais</td>
              <td id="com14AnosOuMais_td" style="color: blue" >0</td>
              <td id="com14AnosOuMaisPC_td" style="color: blue" >%</td>
            </tr>
          </table>
        </div>
        <div class="itemRelatorio" >
          <span>Famí­lias atendidas: </span>
          <span id="familiasAtendidas_span" class="result" >0</span>
        </div>
        <div class="itemRelatorio" >
          <span>TEMPO DE PERMANÊNCIA NO PROJETO</span>
          <table>
            <tr>
              <td>3 anos ou mais</td>
              <td id="permanencia3anos_td" style="color: blue" >0</td>
              <td id="permanencia3anosPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>2 anos até menos de 3 anos</td>
              <td id="permanencia2anos_td" style="color: blue" >0</td>
              <td id="permanencia2anosPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>1 ano até menos de 2 anos </td>
              <td id="permanencia1ano_td" style="color: blue" >0</td>
              <td id="permanencia1anoPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>1 semestre até menos de 1 ano</td>
              <td id="permanencia1semestre_td" style="color: blue" >0</td>
              <td id="permanencia1semestrePC_td"
                  style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>1 mês até menos de 1 semestre</td>
              <td id="permanencia1mes_td" style="color: blue" >0</td>
              <td id="permanencia1mesPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Menos de 1 mês</td>
              <td id="permanenciaMenosDe1mes_td" style="color: blue" >0</td>
              <td id="permanenciaMenosDe1mesPC_td"
                  style="color: blue" >0%</td>
            </tr>
          </table>
        </div>
        <div class="itemRelatorio" style="display: none" >
          <span>Atividades Realizadas</span>
          <table>
            <tr>
              <td>nome da atividade</td>
              <td style="color: blue" >0</td>
            </tr>
          </table>
        </div>
      </div>
      <div style="float: left; margin-left: 5px" >
        <div class="itemRelatorio" >
          <span>Meninas: </span>
          <span id="meninas_span" class="result" >
            0</span><span id="meninasPC_span" class="result"
                          style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" style="margin-top: 10px" >
          <span>Meninos: </span>
          <span id="meninos_span" class="result" >
            0</span><span id="meninosPC_span" class="result"
                          style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" >
          <span>PERÍODO</span>
        </div>
        <div class="itemRelatorio" >
          <span>Manhã: </span>
          <span id="periodoPSManha_span" class="result" >
            0</span><span id="periodoPSManhaPC_span" class="result"
                          style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" style="margin-top: 10px" >
          <span>Tarde: </span>
          <span id="periodoPSTarde_span" class="result" >
            0</span><span id="periodoPSTardePC_span" class="result"
                          style="border-left: 0" >0%</span>
        </div>
        <div class="itemRelatorio" >
          <span>BAIRROS</span>
          <table>
            <tr>
              <td>Centro</td>
              <td id="centro_td" style="color: blue" >0</td>
              <td id="centroPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Ipiranga</td>
              <td id="ipiranga_td" style="color: blue" >0</td>
              <td id="ipirangaPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Nogueira</td>
              <td id="nogueira_td" style="color: blue" >0</td>
              <td id="nogueiraPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Itapema</td>
              <td id="itapema_td" style="color: blue" >0</td>
              <td id="itapemaPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Freguesia</td>
              <td id="freguesia_td" style="color: blue" >0</td>
              <td id="freguesiaPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Maracatu</td>
              <td id="maracatu_td" style="color: blue" >0</td>
              <td id="maracatuPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Itaoca</td>
              <td id="itaoca_td" style="color: blue" >0</td>
              <td id="itaocaPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Outros</td>
              <td id="outrosBairros_td" style="color: blue" >0</td>
              <td id="outrosBairrosPC_td" style="color: blue" >0%</td>
            </tr>
          </table>
        </div>
        <div class="itemRelatorio" >
          <span>ESCOLAS</span>
          <table>
            <tr>
              <td>E. E. Ivan Brasil</td>
              <td id="IB_td" style="color: blue" >0</td>
              <td id="IBPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>E. E. Getúlio Vargas</td>
              <td id="GV_td"  style="color: blue" >0</td>
              <td id="GVPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>E. M. Célia Leonor</td>
              <td id="CL_td" style="color: blue" >0</td>
              <td id="CLPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>E. E. Roberto Feijó</td>
              <td id="RF_td"  style="color: blue" >0</td>
              <td id="RFPC_td" style="color: blue" >0%</td>
            </tr>
            <tr>
              <td>Outras</td>
              <td id="outrasEscolas_td"  style="color: blue" >0</td>
              <td id="outrasEscolasPC_td" style="color: blue" >0%</td>
            </tr>
          </table>
        </div>
      </div>
      <div style="clear: both" ></div>
    </div>
    <script>

    switch (Number.parseInt(mes)) {
      case 1:
        mes_h2.textContent = 'Janeiro';
        break;
      case 2:
        mes_h2.textContent = 'Fevereiro';
        break;
      case 3:
        mes_h2.textContent = 'Março';
        break;
      case 4:
        mes_h2.textContent = 'Abril';
        break;
      case 5:
        mes_h2.textContent = 'Maio';
        break;
      case 6:
        mes_h2.textContent = 'Junho';
        break;
      case 7:
        mes_h2.textContent = 'Julho';
        break;
      case 8:
        mes_h2.textContent = 'Agosto';
        break;
      case 9:
        mes_h2.textContent = 'Setembro';
        break;
      case 10:
        mes_h2.textContent = 'Outubro';
        break;
      case 11:
        mes_h2.textContent = 'Novembro';
        break;
      case 12:
        mes_h2.textContent = 'Dezembro';
        break;
    }

    mes_h2.textContent += ' de ' + ano;

    fetch('dados-mes.do?mes=' + mes + '&ano=' + ano)
            .then(function (response) {
              if (!response.ok) {
                throw new Error('Http error: ' + response.status);
              }
      return response.json();        
    }).then(function (json) {
      totalAcolhidos_span.textContent = json.totalAcolhidos;
      desligamentos_span.textContent = json.desligamentos;
      desligamentosMenosDe1mes_span.
        textContent = json.desligamentosMenosDe1mes;
      menosDe8Anos_td.textContent = json.acolhidosComMenosDe8Anos;
      menosDe8AnosPC_td.textContent =
        formatPercent(json.acolhidosComMenosDe8Anos / json.totalAcolhidos);
      com8AnosAteMenosDe10Anos_td.textContent =
        json.acolhidosCom8AnosAteMenosDe10Anos;
      com8AnosAteMenosDe10AnosPC_td.textContent =
        formatPercent(json.acolhidosCom8AnosAteMenosDe10Anos /
                        json.totalAcolhidos);
      com10AnosAteMenosDe12Anos_td.textContent =
        json.acolhidosCom10AnosAteMenosDe12Anos;
      com10AnosAteMenosDe12AnosPC_td.textContent =
        formatPercent(json.acolhidosCom10AnosAteMenosDe12Anos /
                        json.totalAcolhidos);
      com12AnosAteMenosDe14Anos_td.textContent =
        json.acolhidosCom12AnosAteMenosDe14Anos;
      com12AnosAteMenosDe14AnosPC_td.textContent =
        formatPercent(json.acolhidosCom12AnosAteMenosDe14Anos /
                        json.totalAcolhidos);
      com14AnosOuMais_td.textContent =
        json.acolhidosCom14AnosOuMais;
      com14AnosOuMaisPC_td.textContent =
        formatPercent(json.acolhidosCom14AnosOuMais /
                        json.totalAcolhidos);
      familiasAtendidas_span.textContent = json.familiasAtendidas;
      desligamentos_span.textContent = json.desligamentos;
      meninas_span.textContent = json.meninas;
      meninasPC_span.textContent = formatPercent(json.meninas /
                                                   json.totalAcolhidos);
      meninos = json.totalAcolhidos - json.meninas;
      meninos_span.textContent = meninos;
      meninosPC_span.textContent = formatPercent(meninos /
                                                   json.totalAcolhidos);
      periodoPSManha_span.textContent = json.periodoPSManha;
      periodoPSManhaPC_span.textContent =
        formatPercent(json.periodoPSManha / json.totalAcolhidos);
      periodoPSTarde = json.totalAcolhidos - json.periodoPSManha;
      periodoPSTarde_span.textContent = periodoPSTarde;
      periodoPSTardePC_span.textContent =
        formatPercent(periodoPSTarde / json.totalAcolhidos);
      centro_td.textContent = json.bairros.centro;
      centroPC_td.textContent = formatPercent(json.bairros.centro /
                                                json.totalAcolhidos);
      ipiranga_td.textContent = json.bairros.ipiranga;
      ipirangaPC_td.textContent = formatPercent(json.bairros.ipiranga /
                                                  json.totalAcolhidos);
      nogueira_td.textContent = json.bairros.nogueira;
      nogueiraPC_td.textContent = formatPercent(json.bairros.nogueira /
                                                  json.totalAcolhidos);
      itapema_td.textContent = json.bairros.itapema;
      itapemaPC_td.textContent = formatPercent(json.bairros.itapema /
                                                 json.totalAcolhidos);
      freguesia_td.textContent = json.bairros.freguesia;
      freguesiaPC_td.textContent = formatPercent(json.bairros.freguesia /
                                                   json.totalAcolhidos);
      maracatu_td.textContent = json.bairros.maracatu;
      maracatuPC_td.textContent = formatPercent(json.bairros.maracatu /
                                                  json.totalAcolhidos);
      itaoca_td.textContent = json.bairros.itaoca;
      itaocaPC_td.textContent = formatPercent(json.bairros.itaoca /
                                                  json.totalAcolhidos);
      outrosBairros = json.totalAcolhidos -
                        (json.bairros.centro + json.bairros.ipiranga +
                          json.bairros.nogueira + json.bairros.itapema +
                          json.bairros.freguesia + json.bairros.maracatu +
                          json.bairros.itaoca);
      outrosBairros_td.textContent = outrosBairros;
      outrosBairrosPC_td.textContent = formatPercent(outrosBairros /
                                                       json.totalAcolhidos);
      IB_td.textContent = json.escolas.IB;
      IBPC_td.textContent = formatPercent(json.escolas.IB /
                                            json.totalAcolhidos);
      GV_td.textContent = json.escolas.GV;
      GVPC_td.textContent = formatPercent(json.escolas.GV /
                                            json.totalAcolhidos);
      CL_td.textContent = json.escolas.CL;
      CLPC_td.textContent = formatPercent(json.escolas.CL /
                                            json.totalAcolhidos);
      RF_td.textContent = json.escolas.RF;
      RFPC_td.textContent = formatPercent(json.escolas.RF /
                                            json.totalAcolhidos);
      outrasEscolas = json.totalAcolhidos -
                        (json.escolas.IB + json.escolas.GV +
                          json.escolas.CL + json.escolas.RF);
      outrasEscolas_td.textContent = outrasEscolas;
      outrasEscolasPC_td.textContent = formatPercent(outrasEscolas /
                                            json.totalAcolhidos);
      permanencia3anos_td.textContent = json.permanencia.com3anos;
      permanencia3anosPC_td.textContent = formatPercent(
                                            json.permanencia.com3anos /
                                            json.totalAcolhidos);
      permanencia2anos_td.textContent = json.permanencia.com2anos;
      permanencia2anosPC_td.textContent = formatPercent(
                                            json.permanencia.com2anos /
                                            json.totalAcolhidos);
      permanencia1ano_td.textContent = json.permanencia.com1ano;
      permanencia1anoPC_td.textContent = formatPercent(
                                            json.permanencia.com1ano /
                                            json.totalAcolhidos);
      permanencia1semestre_td.textContent = json.permanencia.com1semestre;
      permanencia1semestrePC_td.textContent = formatPercent(
        json.permanencia.com1semestre /
        json.totalAcolhidos);
      permanencia1mes_td.textContent = json.permanencia.com1mes;
      permanencia1mesPC_td.textContent = formatPercent(
                                           json.permanencia.com1mes /
                                           json.totalAcolhidos);
      permanenciaMenosDe1mes_td.textContent = json.permanencia.menosDe1mes;
      permanenciaMenosDe1mesPC_td.textContent = formatPercent(
        json.permanencia.menosDe1mes /
        json.totalAcolhidos);
 
    }).catch(function (error) {
      console.log(error.message);
    });  

    function formatPercent(n) {
      var intPart;
      var fractionPart;
      n *= 10000;
      n = Number.parseInt(n);
      if (n % 10 !== 0) {
        intPart = Number.parseInt(n / 100);
        fractionPart = n % 100;
        return intPart + "," + fractionPart + "%";
      } else if (n % 100 !== 0) {
        intPart = Number.parseInt(n / 100);
        fractionPart = n % 100;
        fractionPart /= 10;
        return intPart + "," + fractionPart + "%";
      } else {
        intPart = Number.parseInt(n / 100);
        return intPart + "%";
      }
    }

    </script>  
  </body>
</html>
<!-- vi: set sts=2 ts=2 sw=2 et fenc=utf-8 ff=dos: -->
