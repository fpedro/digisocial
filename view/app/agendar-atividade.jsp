<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <title>Agendar atividade</title>
    <script></script>
    <style>

      * {
        font-size: 14pt;
        font-family: sans-serif;
      }

      body {
        margin: 0;
        background-color: skyblue; 
      }

      form {
        padding: 30px;
        background-color: white;
      }

      h1 {
        font-size: 18pt;
      }

      h3 {
        font-weight: normal;
      }

      form {
        margin: 30px;
        padding: 30px;
        background-color: white;
      }

      table {
        border-collapse: collapse;
        letter-spacing: 1px;
        font-size: 0.8rem;
      }

      td, th {
        padding: 10px 20px;
      }

      td {
        text-align: center;
      }

      tr:nth-child(even) td {
        background-color: rgb(245,245,245);
      }

      tr:nth-child(odd) td {
        background-color: #95c0fd;
      }

      .operationInput {
        font-weight: bold;
        height: 40px;
      }

      .optionsButton {
        color: blue;
        text-decoration: underline;
        cursor: pointer;
      }

    </style>
  </head>
  <body>
    <div id="flexContainer" >
      <form id="atividadeForm" action="agendar-atividade.do" method="POST"
            enctype="multipart/form-data" >
          <h1>Agendar atividade</h1>
          <div id="leftSection" style="float: left" >
            <div style="display: block; margin-bottom: 5px" >
              <label for="atividade" >Atividade: </label>
              <select id="atividade" name="atividade" >
                <option style="display:none" value="" ></option>
              </select>
            </div>
            <div style="display: block; margin-bottom: 5px" >
              <span class="optionsButton" >Cadastrar nova atividade</span>
            </div>
            <div style="display: block; margin-bottom: 5px" >
              <label for="orientador" >Orientador: </label>
              <select id="orientador" name="orientador" >
              </select>
            </div>
            <div style="display: block; margin-bottom: 5px" >
              <span>DIAS E HOR�RIOS DISPON�VEIS</span>
              <table id="DiasHorariosDisponiveis" >
                <tbody>
                  <tr>
                    <td>sem restri��es</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div  id="rightSection" style="margin-left: 10px; float: left" >  
            <div style="display: block; margin-bottom: 5px" >
              <label for="diaAtividadeLabel" >Dia da atividade: </label>
              <label id="diaAtividadeLabel" for="diaAtividadeInput"
                     class="optionsButton" >
                DD-MM-AAAA</label>
              <input type="date" id="diaAtividadeInput" name="data"
                     style="position: relative; left: -140px; visibility: hidden" >
              <span id="diadasemana"
                    style="position: relative; left: -140px" ></span>
            </div>
            <div style="display: block; margin-bottom: 5px" >
              <label>Hor�rio: </label>
              <input id="horarioInicioAtividade" name="inicio"
                     type="text" placeholder="00:00" style="width: 58px"/>
              <span> at� </span>
              <input id="horarioTerminoAtividade" name="termino"
                     type="text" placeholder="00:00" style="width: 58px"/>
            </div>
            <div id="message"
                  style="display: block; margin-bottom: 5px; margin-top: 30px;
                   min-height: 30px" >
              <span id="mensagemTemplate"
                    style="display: none; color: #fd4747;
                           transition: opacity 2s" ></span>
            </div>
            <div style="display: block; margin-bottom: 5px; margin-top: 30px;
                 margin-left: 130px" >
              <input type="hidden" name="operacao" value="SALVAR" />
              <input id="agendarInput" class="operationInput" type="button"
                     onclick="agendarAtividade()" value="Agendar" />
            </div>
          </div>
          <div style="clear: both" ></div>  
        </form>

      </div>
      <script>

        class Atividade {
          constructor() {
            this.orientadores = new Array();
          }
        }

        class Orientador {
          constructor() {
            this.horariosDisponiveis = new Array();
          }
        }

        class HorarioDisponivel {
        }

        var atividadeSelect = document.getElementById('atividade');
        var orientadorSelect = document.getElementById('orientador');
        var diaAtividadeInput = document.getElementById('diaAtividadeInput');
        var diaAtividadeLabel = document.getElementById('diaAtividadeLabel');
        var diadasemanaSpan = document.getElementById('diadasemana');
        var diashorariosTabela = document
                .getElementById('DiasHorariosDisponiveis');
        var mensagemTemplate = document.getElementById('mensagemTemplate');
        var message = document.getElementById('message');

        var atividadeForm = document.getElementById('atividadeForm');

        var atividadeEscolhida = new Atividade();
        var orientadorEscolhido = new Orientador();

        var atividades = new Array();

        diaAtividadeInput.value = '';

        diaAtividadeInput.onchange = function f() {
          var dia, mes, ano;
          var data;
          var formatoAtual = new String();
          var formatoInvertido = new String();

          if (diaAtividadeInput.value.length === 0) {
            diaAtividadeLabel.textContent = 'DD-MM-AAAA';
            diadasemanaSpan.textContent = '';
          } else {
            diaAtividadeLabel.textContent = diaAtividadeInput.value;
            formatoAtual = diaAtividadeLabel.textContent;
            // modificar formato da data
            if (formatoAtual.indexOf('-') === 4) {
              // pegar dia
              formatoInvertido += formatoAtual.charAt(8);
              formatoInvertido += formatoAtual.charAt(9);
              formatoInvertido += '-';
              // pegar mês
              formatoInvertido += formatoAtual.charAt(5);
              formatoInvertido += formatoAtual.charAt(6);
              formatoInvertido += '-';
              // pegar ano
              formatoInvertido += formatoAtual.charAt(0);
              formatoInvertido += formatoAtual.charAt(1);
              formatoInvertido += formatoAtual.charAt(2);
              formatoInvertido += formatoAtual.charAt(3);
              diaAtividadeLabel.textContent = formatoInvertido;
              formatoAtual = formatoInvertido;
            }
            dia = formatoAtual.charAt(0);
            dia += formatoAtual.charAt(1);
            mes = formatoAtual.charAt(3);
            mes += formatoAtual.charAt(4);
            ano = formatoAtual.charAt(6);
            ano += formatoAtual.charAt(7);
            ano += formatoAtual.charAt(8);
            ano += formatoAtual.charAt(9);
            data = new Date();
            data.setDate(Number.parseInt(dia));
            data.setMonth(Number.parseInt(mes) - 1);
            data.setFullYear(Number.parseInt(ano));
            switch (data.getDay()) {
              case 0:
                diadasemanaSpan.textContent = 'domingo';
                break;
              case 1:
                diadasemanaSpan.textContent = 'segunda-feira';
                break;
              case 2:
                diadasemanaSpan.textContent = 'ter�a-feira';
                break;
              case 3:
                diadasemanaSpan.textContent = 'quarta-feira';
                break;
              case 4:
                diadasemanaSpan.textContent = 'quinta-feira';
                break;
              case 5:
                diadasemanaSpan.textContent = 'sexta-feira';
                break;
              case 6:
                diadasemanaSpan.textContent = 'sábado';
                break;
            }
          }
        };

        fetch('visualizar-atividade.do')
                .then(function (response) {
                  if (!response.ok) {
                    throw new Error("HTTP error, status = " + response.status);
                  }
                  return response.json();
                })
                .then(function (json) {
                  for (var i = 0; i < json.atividades.length; i++) {
                    optionElement = document.createElement('option');
                    optionElement.textContent = json.atividades[i].nome;
                    atividades[i] = new Atividade();
                    atividades[i].id = json.atividades[i].id;
                    atividades[i].nome = json.atividades[i].nome;
                    for (var j = 0;
                            j < json.atividades[i].orientadores.length; j++) {
                      atividades[i].orientadores[j] = new Orientador();
                      atividades[i].orientadores[j].id = json.atividades[i]
                              .orientadores[j].id;
                      ;
                    }
                    optionElement.value = atividades[i].id;
                    atividadeSelect.appendChild(optionElement);
                  }
                  atividadeSelect.onchange = definirOrientadores;
                })
                .catch(function (error) {
                  console.log(error.message);
                });

        function definirOrientadores() {
          atividadeId = Number.parseInt(atividadeSelect.value);
          while (orientadorSelect.hasChildNodes()) {
            orientadorSelect.removeChild(orientadorSelect.firstChild);
          }
          // encontrar atividade no array
          for (var i = 0; i < atividades.length; i++) {
            if (atividades[i].id === atividadeId) {
              atividadeEscolhida = atividades[i];
              break;
            }
          }
          for (var i = 0; i < atividadeEscolhida.orientadores.length; i++) {
            // pegar orientadores pelo id
            fetch('visualizar-orientador.do?id='
                    + atividadeEscolhida.orientadores[i].id)
                    .then(function (response) {
                      if (!response.ok) {
                        throw new Error("HTTP error, status = " + response.status);
                      }

                      return response.json();
                    })
                    .then(function (json) {
                      var o = new Orientador();
                      o.id = json.orientadores[0].id;
                      // encontrar orientador na escolha
                      for (var j = 0;
                              j < atividadeEscolhida.orientadores.length; j++) {
                        if (atividadeEscolhida.orientadores[j].id === o.id) {
                          o = atividadeEscolhida.orientadores[j];
                          break;
                        }
                      }
                      optionElement = document.createElement('option');
                      o.nome = json.orientadores[0].nome;
                      for (var j = 0;
                              j < json.orientadores[0].horariosdisponiveis.length; j++) {
                        o.horariosDisponiveis[j] = new HorarioDisponivel();

                        d = json.orientadores[0]
                                .horariosdisponiveis[j].diadasemana;

                        switch (d) {
                          case 'DOMINGO':
                            o.horariosDisponiveis[j].diadasemana = 'domingo';
                            break;
                          case 'SEGUNDA':
                            o.horariosDisponiveis[j].diadasemana = 'segunda-feira';
                            break;
                          case 'TERCA':
                            o.horariosDisponiveis[j].diadasemana = 'terça-feira';
                            break;
                          case 'QUARTA':
                            o.horariosDisponiveis[j].diadasemana = 'quarta-feira';
                            break;
                          case 'QUINTA':
                            o.horariosDisponiveis[j].diadasemana = 'quinta-feira';
                            break;
                          case 'SEXTA':
                            o.horariosDisponiveis[j].diadasemana = 'sexta-feira';
                            break;
                          case 'SABADO':
                            o.horariosDisponiveis[j].diadasemana = 'sábado';
                            break;
                        }

                        o.horariosDisponiveis[j]
                                .horainicio = json.orientadores[0]
                                .horariosdisponiveis[j].horainicio;
                        o.horariosDisponiveis[j]
                                .minutoinicio = json.orientadores[0]
                                .horariosdisponiveis[j].minutoinicio;
                        o.horariosDisponiveis[j]
                                .horatermino = json.orientadores[0]
                                .horariosdisponiveis[j].horatermino;
                        o.horariosDisponiveis[j]
                                .minutotermino = json.orientadores[0]
                                .horariosdisponiveis[j].minutotermino;
                      }
                      optionElement.value = o.id;
                      optionElement.textContent = json.orientadores[0].nome;
                      orientadorSelect.appendChild(optionElement);

                      if (o.id === atividadeEscolhida.orientadores[0].id) {
                        apresentarDiasHorarios();
                      }
                    })
                    .catch(function (error) {
                      console.log(error.message);
                    });
          }
        }

        function apresentarDiasHorarios() {
          conteudoTabela = diashorariosTabela.querySelector('tbody');
          orientadorId = Number.parseInt(orientadorSelect.value);
          while (conteudoTabela.hasChildNodes()) {
            conteudoTabela.removeChild(conteudoTabela.firstChild);
          }
          // encontrar orientador no array
          for (var i = 0; i < atividadeEscolhida.orientadores.length; i++) {
            if (atividadeEscolhida.orientadores[i].id === orientadorId) {
              orientadorEscolhido = atividadeEscolhida.orientadores[i];
              break;
            }
          }
          if (orientadorEscolhido.horariosDisponiveis.length === 0) {
            r = document.createElement('tr');
            d = document.createElement('td');
            r.appendChild(d);
            d.textContent = 'sem restrições';
            conteudoTabela.appendChild(r);
          }
          for (var i = 0;
                  i < orientadorEscolhido.horariosDisponiveis.length; i++) {
            r = document.createElement('tr');
            d1 = document.createElement('td');
            d2 = document.createElement('td');
            textd2 = '';
            r.appendChild(d1);
            d1.textContent = orientadorEscolhido
                    .horariosDisponiveis[i].diadasemana;
            textd2 += orientadorEscolhido.horariosDisponiveis[i].horainicio;
            textd2 += ':';
            if (orientadorEscolhido.horariosDisponiveis[i].minutoinicio < 10) {
              textd2 += '0';
            }
            textd2 += orientadorEscolhido.horariosDisponiveis[i].minutoinicio;
            textd2 += ' at� ';
            textd2 += orientadorEscolhido.horariosDisponiveis[i].horatermino;
            textd2 += ':';
            if (orientadorEscolhido.horariosDisponiveis[i].minutotermino < 10) {
              textd2 += '0';
            }
            textd2 += orientadorEscolhido.horariosDisponiveis[i].minutotermino;
            d2.textContent = textd2;
            r.appendChild(d2);
            conteudoTabela.appendChild(r);
          }
        }

        function agendarAtividade() {

          formData = new FormData(atividadeForm);

          fetch('agendar-atividade.do', {
            method: 'POST',
            body: formData
          })
          .then(function (response) {
            if (!response.ok) {
              throw new Error("HTTP error, status = " + response.status);
            }
            return response.json();
          })
          .then ( function (json) {
            if (json.erro === undefined) {
              novadata = new Date();
              novadata.setTime(Number.parseInt((json.
                                        atividadesagendadas[0].inicio)));
              semestre = novadata.getMonth() < 6 ? 1 : 2;
              ano = novadata.getFullYear();
              document.location = 'agenda.html?novaatividade&destacar=' + 
                                      json.atividadesagendadas[0].id +
                                      '&semestre=' + semestre + 
                                      '&ano=' + ano;
            }

            while (message.hasChildNodes()) {
              message.removeChild(message.firstChild);
            }


            novamensagem = mensagemTemplate.cloneNode();
            novamensagem.style.display = 'block';
            novamensagem.textContent = json.erro;
            message.appendChild(novamensagem);
            setTimeout(clearMessage, 3000, novamensagem);
          })
          .catch(function (error) {
            console.log(error.message);
          });

        }

        function clearMessage(e) {
          window.requestAnimationFrame(function (time) {
            window.requestAnimationFrame(function (time) {
              e.style.opacity = 0; });
            });
        }

        orientadorSelect.onchange = apresentarDiasHorarios;
      </script>
  </body>


</html>
