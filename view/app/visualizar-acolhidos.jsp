<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
  <head>
    <title>Acolhidos sendo atendidos</title>
    <meta charset="UTF-8" />
    <meta name="viewport"
          content="width=device-width, initial-scale=1.0" />
    <link rel="stylesheet" href="../style.css" />
    <script>
        let params = (new URL(document.location)).searchParams;
        let novoAcolhido = params.get('novoAcolhido');
        let dadosAlterados = params.get('dadosAlterados');
        let acolhidoReinserido = params.get('acolhidoReinserido');
        let acolhidoDesligado = params.get('acolhidoDesligado');
        if (novoAcolhido !== null)
          novoAcolhido = true;
        dadosAlterados = dadosAlterados !== null? true : false;
        acolhidoReinserido = acolhidoReinserido !== null? true : false;
        acolhidoDesligado = acolhidoDesligado !== null? true : false;
        let novoAcolhidoId = parseInt(params.get('novoAcolhido'));
        let dadosAlteradosId = parseInt(params.get('dadosAlterados'));
        let acolhidoReinseridoId = parseInt(params.get('acolhidoReinserido'));
    </script>
  </head>
  <body>
<%@include file="template-p1.jsp"%>
<script src="script.js" ></script>
<span id="info_span" class="infoMessage" ></span>
<div style="clear: both" ></div>
<div id="periodoManha_div" >
  PERÍODO DA MANHÃ
  <br /><span id="manhaCounter_span" ></span> acolhidos
  <div style="clear: both" ></div>
</div>
<div style="clear: both" ></div>
<div id="periodoTarde_div" >
  PERÍODO DA TARDE
  <br /><span id="tardeCounter_span" ></span> acolhidos
  <div style="clear: both" ></div>
</div>
<div style="clear: both" ></div>
<script>

  info_span.textContent = 'processando...';

  fetch('acolhido-query.do')
    .then(function (response) {
      if (!response.ok) {
          throw new Error("HTTP error, status = " + response.status);
      }
      return response.json();
    })
    .then(function (json) {
      manhaCounter = 0;
      tardeCounter = 0;
      currentTime = new Date(json.currentTime);
      for (i = 0; i < json.acolhidos.length; i++) {
         acolhido_div = acolhido_builder(json.acolhidos[i]
                                         .id,
                                         json.acolhidos[i]
                                         .dataCadastro,
                                         json.acolhidos[i]
                                         .nomeAcolhido,
                                         json.acolhidos[i]
                                         .foto,
                                         json.acolhidos[i]
                                         .dataNascimento,
                                         json.acolhidos[i]
                                         .sexo,
                                         json.acolhidos[i]
                                         .nomeMae,
                                         json.acolhidos[i]
                                         .nomePai,
                                         json.acolhidos[i]
                                         .telefoneResidencial,
                                         json.acolhidos[i]
                                         .telefoneCelular,
                                         json.acolhidos[i]
                                         .atendimentoPrioritario,
                                         currentTime);
      if (novoAcolhido) {
        if (novoAcolhidoId === json.acolhidos[i].id)
           acolhido_highLight(acolhido_div);
      }
      if (dadosAlterados) {
        if (dadosAlteradosId === json.acolhidos[i].id)
           acolhido_highLight(acolhido_div);
      }
      if (acolhidoReinserido) {
        if (acolhidoReinseridoId === json.acolhidos[i].id)
           acolhido_highLight(acolhido_div);
      }
      view_link = document.createElement('a');
      view_link.href = 'visualizar-dados-acolhido.jsp?id=' +
                         json.acolhidos[i].id;
      view_link.style.color = 'black';
      view_link.style.textDecoration = 'none';
      view_link.appendChild(acolhido_div);
      if (json.acolhidos[i].periodoPS === 'MANHA') {
          periodoManha_div.appendChild(view_link);
          manhaCounter++;
        }
        if (json.acolhidos[i].periodoPS === 'TARDE') {
          periodoTarde_div.appendChild(view_link);
          tardeCounter++;
        }
      }
      manhaCounter_span.textContent = manhaCounter;
      tardeCounter_span.textContent = tardeCounter;
      if (novoAcolhido)
        info_span.textContent = 'Novo acolhido adicionado';
      else if (acolhidoReinserido)
        info_span.textContent = 'Acolhido reinserido';
      else if (dadosAlterados)
        info_span.textContent = 'Dados alterados';
      else if (acolhidoDesligado)
        info_span.textContent = 'Acolhido desligado';
      else info_span.textContent = '';
    })
    .catch(function (error) {
      console.log(error.message);
    });

</script>
<%@include file="template-p2.jsp"%>
  </body>
</html>
<!-- vi: set sts=3 ts=3 sw=3 et: -->
