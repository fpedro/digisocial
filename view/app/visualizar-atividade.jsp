<!DOCTYPE html>
<html>
  <head>
    <title>Visualizar Encontro</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <script>

        let params = (new URL(document.location)).searchParams;

        id = params.get('id');

        class Resultado {
        }

    </script>
    <style>

      * {
          font-size: 14pt;
          font-family: sans-serif;
      }

      body {
          margin: 0;
          background-color: skyblue; 
      }

      h1 > span {
          font-size: 18pt;
      }

      h3 {
          font-weight: normal;
      }

      #atividadeDiv {
          margin-top: 30px;
          margin-left: 30px;
          padding: 30px;
          background-color: white;
          width: 650px;
      }

      .operationInput {
          font-weight: bold;
          height: 40px;
      }

      .optionsButton {
          color: blue;
          text-decoration: underline;
          cursor: pointer;
      }


    </style>
  </head>
  <body>
    <div id="atividadeDiv" >  
      <h1>
        <span id="dataSpan" ></span>
        <span id="nomeAtividadeSpan" ></span>
      </h1>
      <span id="nomeOrientadorSpan" ></span>
      <span id="horarioSpan" style="margin-left: 30px" ></span>
      <span onclick="repetirAtividade()"
            class="optionsButton" style="float: right" >
        Repetir na próxima semana</span>
      <div>
        <span class="optionsButton" >
          Modificar dia ou horário</span>
      </div>
      <div>
        <span class="optionsButton"
              onclick="removerAtividade()" >
          Remover atividade</span>
      </div>  
      <div id="message"
           style="display: block; margin-bottom: 5px; margin-top: 30px;" >
        <span id="mensagemTemplate"
              style="display: none; color: #fd4747;
              transition: opacity 2s" ></span>
      </div>
      <h3>DESCRIÇÃO DA ATIVIDADE</h3>
      <p style="width: 450px" ><span id="descricaoAtividadeSpan" ></span></p> 
      <h2 style="margin-bottom: 0" >Objetivos e resultados</h2>
      <div id="ObjetivosEResultados" >
        <div id="templateResultadoPresentation"
             style="display: none; margin-top: 10px; margin-bottom: 10px;
             padding: 5px; width: 628px; border: 3px solid #005aff" >
          <div style="display: block" >
            <span>Nome: </span><span id="nomeObjetivoSpan" ></span> 
          </div>
          <div style="display: block" >
            <span>Descrição: </span><span id="descricaoObjetivoSpan" ></span>
          </div>
          <div style="display: block" >
            <span>Crianças que participaram: </span>
            <span id="participacaoSpan" ></span>
          </div>
          <div style="display: block" >
            <span>Resultados esperados: </span>
            <span id="resultadosEsperadosSpan" ></span>
          </div>
          <div style="display: block" >
            <span>Os resultados foram alcançados? </span>
            <span id="resultadosRespostaSpan" ></span>
          </div>
          <div style="display: block" >
            <span>Resultados alcançados: </span>
            <span id="resultadosAlcancadosSpan" ></span>
          </div>
        </div>
        <div id="templateResultadoForm"
             style="display: none;
             margin-top: 10px; padding: 5px; padding-top: 10px;
             width: 628px; border: 3px solid gainsboro" >
          <div style="display: block; margin-top: 5px" >
            <label for="resultadosEsperadosText" style="float: left" >
              Resultados esperados:&nbsp;</label>
            <textarea id="resultadosEsperadosText" style="width: 400px" >
            </textarea>
          </div>        
          <div style="display: block; margin-top: 5px" >
            <label for="resultadosRespostaSelect" >
              Os resultados foram alcançados? 
            </label>
            <select id="resultadosRespostaSelect" >
              <option style="display:none" value="" ></option>
              <option>SIM</option>
              <option>NÃO</option>
            </select>
          </div>
          <div style="display: block; margin-top: 5px" >
            <label for="resultadosAlcancadosText" style="float: left" >
              Resultados alcançados:&nbsp;</label>
            <textarea id="resultadosAlcancadosText" style="width: 400px" >
            </textarea>
          </div>
          <div style="display: block; margin-top: 10px; float: right" >
            <input id="salvar" type="button" class="operationInput"
                   value="Salvar"/>            
          </div>
        </div>
        <div id="templateObjetivo"
             style="display: none;
             margin-top: 10px; padding: 5px; padding-top: 10px;
             width: 628px; border: 3px solid gainsboro" >
          <div style="float: left" >
            <label for="nomeObjetivoInput" >Objetivo:</label>
            <input id="nomeObjetivoInput" type="text" style="width: 400px" />
          </div>
          <div>
            <span id="removerObjetivo" class="optionsButton" 
                  style="width: 80px; float: right; margin-right: 10px"
                  onclick="removerObjetivo(this)" >
              Remover objetivo
            </span>
          </div>
          <div style="display: block; margin-top: 5px; clear: both" >
            <label for="descricaoObjetivoText" style="float: left" >
              Descrição:&nbsp;</label>
            <textarea id="descricaoObjetivoText" style="width: 400px" >
            </textarea>
          </div>
          <div style="display: block; margin-top: 5px" >
            <label>
              Crianças para o objetivo:&nbsp;</label>
            <span id="participacaoOptions" class="optionsButton" 
                  style="display: block; clear: both"
                  onclick="showacolhidosPane()" >
              Adicionar crianças
            </span>
          </div>
          <div style="display: block; margin-top: 10px; float: right" >
            <input id="salvarObjetivo" type="button" class="operationInput"
                   value="Salvar"/>            
          </div>
        </div>
      </div>  
      <div>
        <span class="optionsButton"
              onclick="adicionarObjetivo()" >
          Adicionar objetivo</span>
      </div>  
      <script>

        var contForms = 0;

        ObjetivosEResultados = document
                .getElementById("ObjetivosEResultados");

        function adicionarObjetivo() {

          contForms++;
          e = document.getElementById("templateObjetivo");
          novoObjetivo = e.cloneNode(true);
          novoObjetivo.id = contForms;
          novoObjetivo.style.display = 'inline-block';
          salvarObjetivoInput = novoObjetivo
                  .querySelector('#salvarObjetivo');
          salvarObjetivoInput.setAttribute('onclick',
                  'salvarObjetivo(' + contForms + ')');
          ObjetivosEResultados.appendChild(novoObjetivo);

        }

        function removerObjetivo(e) {
          ObjetivosEResultados.removeChild(e.parentNode.parentNode);
        }

        function salvarObjetivo(f) {
          form = document.getElementById(f);
          nomeObjetivoInput = form.querySelector('#nomeObjetivoInput');
          descricaoObjetivoText = form.querySelector(
                                          '#descricaoObjetivoText');
          resultadosRespostaSelect = form.querySelector(
                                          '#resultadosRespostaSelect');
          r = new Resultado();
          r.nome = nomeObjetivoInput.value;
          r.descricao = descricaoObjetivoText.value;
          r.resultadosresposta = resultadosRespostaSelect.value;
          salvarResultado(r);
          
        }
        
        function salvarResultado(n) {
          console.log(n.nome);
          console.log(n.descricao);
          console.log(n.resultadosresposta);
        }

      </script>
    </div>
    <div id="adicionaracolhidosPane"
         style="display: none;
           margin: 10px; padding: 10px; width: 300px;
           background-color: white" >
        <h2>Adicionar Crianças</h2>
        <div id="paneButtons" >
          <span class="optionsButton" >Cancelar</span>
          <input type="button" value="ADICIONAR"/>
        </div>
    </div>
    <script>

      var data;
      var horarioInicio;
      var horarioTermino;
      var orientador;
      var atividade;

      dataSpan = document.getElementById('dataSpan');
      nomeAtividadeSpan = document.getElementById('nomeAtividadeSpan');
      nomeOrientadorSpan = document.getElementById('nomeOrientadorSpan');
      horarioSpan = document.getElementById('horarioSpan');
      descricaoAtividadeSpan = document
              .getElementById('descricaoAtividadeSpan');
      mensagemTemplate = document.getElementById('mensagemTemplate');


      fetch('visualizar-agenda.do?id=' + id)
              .then(function (response) {
                if (!response.ok) {
                  throw new Error("HTTP error, status = " + response.status);
                }
                return response.json();
              })
              .then(function (json) {

                data = new Date(json.atividadesagendadas[0].inicio);

                horarioInicio = new Date(json.atividadesagendadas[0].inicio);
                horarioTermino = new Date(json.atividadesagendadas[0].termino);

                dataSpan.textContent = data.getDate() + '-' + (data.getMonth() + 1) +
                        '-' + data.getFullYear();
                nomeAtividadeSpan.textContent = json.atividadesagendadas[0].atividade;
                atividade = json.atividadesagendadas[0].atividadeid;
                nomeOrientadorSpan.textContent = json.atividadesagendadas[0].orientador;
                orientador = json.atividadesagendadas[0].orientadorid;
                horarioSpan.textContent = horarioInicio.getHours();
                horarioSpan.textContent += ':';
                if (horarioInicio.getMinutes() < 10)
                  horarioSpan.textContent += '0';
                horarioSpan.textContent += horarioInicio.getMinutes();
                horarioSpan.textContent += ' até ';
                horarioSpan.textContent += horarioTermino.getHours();
                horarioSpan.textContent += ':';
                if (horarioTermino.getMinutes() < 10)
                  horarioSpan.textContent += '0';
                horarioSpan.textContent += horarioTermino.getMinutes();

                fetch('visualizar-atividade.do?id=' + json.atividadesagendadas[0]
                        .atividadeid)
                        .then(function (response) {
                          return response.json();
                        })
                        .then(function (json) {
                          descricaoAtividadeSpan.textContent = json.atividades[0].descricao;
                        });
              })
              .catch(function (error) {
                console.log(error.message);
              });

      function removerAtividade() {
        semestre = data.getMonth() < 6 ? 1 : 2;
        ano = data.getFullYear();
        fetch('remover-atividade.do?&operacao=REMOVER&id=' + id)
                .then(function (response) {
                  document.location = 'agenda.html?semestre=' + semestre +
                          '&ano=' + ano +
                          '&atividaderemovida';
                });
      }

      function repetirAtividade() {

        var d;
        var inicio;
        var termino;

        d = data.getDate() + '-' + (data.getMonth() + 1) +
                '-' + data.getFullYear();


        inicio = horarioInicio.getHours() + ':' + horarioInicio.getMinutes();
        termino = horarioTermino.getHours() + ':'
                + horarioTermino.getMinutes();

        formData = new FormData();

        formData.append('proximasemana', '');
        formData.append('atividade', atividade);
        formData.append('orientador', orientador);
        formData.append('inicio', inicio);
        formData.append('termino', termino);
        formData.append('data', d);
        formData.append('operacao', 'SALVAR');

        fetch('agendar-atividade.do', {
          method: 'POST',
          body: formData
        })
                .then(function (response) {
                  if (!response.ok) {
                    throw new Error("HTTP error, status = " + response.status);
                  }
                  return response.json();
                })
                .then(function (json) {
                  if (json.erro === undefined) {
                    novadata = new Date();
                    novadata.setTime(Number.parseInt((json.
                            atividadesagendadas[0].inicio)));
                    semestre = novadata.getMonth() < 6 ? 1 : 2;
                    ano = novadata.getFullYear();
                    document.location = 'agenda.html?novaatividade&destacar=' +
                            json.atividadesagendadas[0].id +
                            '&semestre=' + semestre +
                            '&ano=' + ano;
                  }

                  while (message.hasChildNodes()) {
                    message.removeChild(message.firstChild);
                  }

                  novamensagem = mensagemTemplate.cloneNode();
                  novamensagem.style.display = 'block';
                  novamensagem.textContent = json.erro;
                  message.appendChild(novamensagem);
                  setTimeout(clearMessage, 3000, novamensagem);
                  setTimeout(removeErrorContent, 5000, novamensagem);
                })
                .catch(function (error) {
                  console.log(error.message);
                });
      }

      function clearMessage(e) {
        window.requestAnimationFrame(function (time) {
          window.requestAnimationFrame(function (time) {
            e.style.opacity = 0; });
          });
      }
      
      function removeErrorContent(e) {
        e.textContent = '';
      }

      function pegarObjetivosResultados() {

      }

      function showacolhidosPane() {
        e = document.getElementById('atividadeDiv');
        e.style.display = 'none';
        e = document.getElementById('adicionaracolhidosPane');
        e.style.display = 'block';

      }

    </script>
  </body>
</html>
