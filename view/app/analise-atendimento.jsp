<!DOCTYPE html>
<html>
  <head>
    <title>An�lise de atendimento</title>
    <meta charset="UTF-8" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0" >
    <style>

       * {
        font-size: 14pt;
        font-family: sans-serif;
      }

      
      body {
        margin: 0;
        background-color: skyblue;
      }

      #analise {
        margin: 10px;
        padding: 5px;
        background-color: white;
      }

      #atendimentoChart {
        display: block;
      }
      
      .operationInput {
        font-weight: bold;
        height: 40px;
      }

    </style>
  </head>

  <body>
    <div id="analise" style="display: inline-block; margin: 30px" >
      <div style="margin: 10px" >
        <label for="semestre" >Semestre: </label>
        <select id="semestre" >
          <option value="1" >1�</option>
          <option value="2" >2�</option>
        </select>
        <label for="ano" >Ano: </label>
        <input type="text" style="width: 51px" value="2019"/>
        <input type="button" class="operationInput" value="Visualizar" />
      </div>

      <div style="padding: 10px; min-width: 100px; min-height: 100px;
                    max-width: 500px; max-height: 500px" >
        <canvas id="atendimentoChart" width="400" height="400" ></canvas>
      </div>
    </div>  
  </body>
  <script src="Chart.bundle.min.js" ></script>
  <script>
    var data;
    data = {
      "labels": [
        "Fevereiro",
        "Mar�o",
        "Abril",
        "Maio",
        "Junho"
      ],
      "datasets": [{
          "label": "Acolhidos atendidas",
          "data": [35, 38, 34, 36, 31],
          "fill": false,
          "backgroundColor": "blue",
          "lineTension": 0.1
        },
        {
          "label": "Desligamentos",
          "data": [3, 1, 4, 1, 5],
          "fill": false,
          "backgroundColor": "rgb(100, 100, 100)",
          "lineTension": 0.1
        }
      ]
    };
    var options = {
      "scales": {
        "yAxes": [{
            "ticks": {
              "beginAtZero": true
            }
          }]
      }

    };

    var ctx = document.getElementById('atendimentoChart').getContext('2d');

    var myLineChart = new Chart(ctx, {
      type: 'bar',
      data: data,
      options: options
    });
  </script>
</html>
